-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Databasestructuur van roaming_souls_c wordt geschreven
DROP DATABASE IF EXISTS `roaming_souls_c`;
CREATE DATABASE IF NOT EXISTS `roaming_souls_c` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `roaming_souls_c`;

-- Structuur van  tabel roaming_souls_c.answer_sets wordt geschreven
DROP TABLE IF EXISTS `answer_sets`;
CREATE TABLE IF NOT EXISTS `answer_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_id` int(11) NOT NULL,
  `set_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `context` text NOT NULL,
  `weight` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.answer_sets: ~6 rows (ongeveer)
DELETE FROM `answer_sets`;
/*!40000 ALTER TABLE `answer_sets` DISABLE KEYS */;
INSERT INTO `answer_sets` (`id`, `original_id`, `set_id`, `description`, `context`, `weight`, `counter`, `version`) VALUES
	(1, 1, 1, 'Nee, niet aanwezig', 'Het is er niet en/of er wordt niet naar gehandeld door de organisatie', 0, 1, 1),
	(2, 2, 1, 'Nee, maar in ontwikkeling', 'Het is er nog niet/er wordt niet naar gehandeld, maar het wordt ontwikkeld', 10, 1, 1),
	(3, 3, 1, 'Ja, maar kan nog beter', 'Het is er en/of we handelen zo, maar het kan nog wel worden verbeterd', 20, 1, 1),
	(4, 4, 1, 'Ja, werkt naar tevredenheid', 'Het is er en/of we handelen zo, en het gaat naar tevredenheid', 30, 1, 1),
	(5, 5, 1, 'Niet bekend', 'Ik zou niet weten of dit er is/ of er zo gehandeld wordt', 5, 1, 1),
	(6, 6, 1, 'Niet van toepassing', 'Dit is voor de organisatie niet van toepassing', 0, 0, 1);
/*!40000 ALTER TABLE `answer_sets` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.category wordt geschreven
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `excerpt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.category: ~6 rows (ongeveer)
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `module_id`, `name`, `excerpt`) VALUES
	(1, 10, 'Personenvervoer', NULL),
	(2, 10, 'Goederenvervoer', NULL),
	(3, 12, 'Elektriciteit', NULL),
	(4, 12, 'Brandstoffen warmteopwekking', NULL),
	(5, 12, 'Warmtelevering', NULL),
	(6, 12, 'Koel- en koudemiddelen', NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.category_answer wordt geschreven
DROP TABLE IF EXISTS `category_answer`;
CREATE TABLE IF NOT EXISTS `category_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `has_child_question` int(11) DEFAULT NULL,
  `child_question_id` int(11) DEFAULT NULL,
  `has_unit` int(11) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `answer_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.category_answer: ~191 rows (ongeveer)
DELETE FROM `category_answer`;
/*!40000 ALTER TABLE `category_answer` DISABLE KEYS */;
INSERT INTO `category_answer` (`id`, `answer`, `question_id`, `has_child_question`, `child_question_id`, `has_unit`, `unit`, `answer_type`) VALUES
	(1, 'Benzine (E95) (NL)', 1, 0, NULL, 1, 'liter', 'input'),
	(2, 'Benzine (E95) (EUR)', 1, 0, NULL, 1, 'liter', 'input'),
	(3, 'Benzine (puur)', 1, 0, NULL, 1, 'liter', 'input'),
	(4, 'Bio-ethanol (E85)', 1, 0, NULL, 1, 'liter', 'input'),
	(5, 'Bio-ethanol', 1, 0, NULL, 1, 'liter', 'input'),
	(6, 'Bio-ethanol (mais)', 1, 0, NULL, 1, 'liter', 'input'),
	(7, 'Bio-ethanol (tarwe)', 1, 0, NULL, 1, 'liter', 'input'),
	(8, 'Bio-ethanol (suikerriet)', 1, 0, NULL, 1, 'liter', 'input'),
	(9, 'Diesel (NL)', 1, 0, NULL, 1, 'liter', 'input'),
	(10, 'Diesel (EUR)', 1, 0, NULL, 1, 'liter', 'input'),
	(11, 'Diesel (puur)', 1, 0, NULL, 1, 'liter', 'input'),
	(12, 'Biodiesel (B100) (NL)', 1, 0, NULL, 1, 'liter', 'input'),
	(13, 'Biodiesel (B100) (EUR)', 1, 0, NULL, 1, 'liter', 'input'),
	(14, 'Biodiesel (B100) (afgewerkte olie)', 1, 0, NULL, 1, 'liter', 'input'),
	(15, 'Waterstof grijs', 1, 0, NULL, 1, 'kg', 'input'),
	(16, 'Waterstof groen', 1, 0, NULL, 1, 'kg', 'input'),
	(17, 'LPG (NL)', 1, 0, NULL, 1, 'liter', 'input'),
	(18, 'LPG (EU)', 1, 0, NULL, 1, 'liter', 'input'),
	(19, 'LNG', 3, 0, NULL, 1, 'kg', 'input'),
	(20, 'CNG (aardgas) (NL)', 3, 0, NULL, 1, 'kg', 'input'),
	(21, 'CNG (aardgas) (EUR)', 3, 0, NULL, 1, 'kg', 'input'),
	(22, 'Bio-CNG (groengas)', 3, 0, NULL, 1, 'kg', 'input'),
	(23, 'Marine Diesel Oil', 3, 0, NULL, 1, 'liter', 'input'),
	(24, 'Marine Gas Oil', 3, 0, NULL, 1, 'liter', 'input'),
	(25, 'Heavy Fuel Oil', 3, 0, NULL, 1, 'liter', 'input'),
	(26, 'Auto', 2, 1, 6, 0, NULL, NULL),
	(27, 'Fiets', 2, 1, 14, 0, NULL, NULL),
	(28, 'Scooter - Motor (geen officiële invoer)', 2, 1, 16, 0, NULL, NULL),
	(29, 'Minibus (ma 8 personen)', 2, 1, 18, 0, NULL, NULL),
	(30, 'Toeringcar', 2, 1, 19, 0, NULL, NULL),
	(31, 'Openbaar vervoer', 2, 1, 20, 0, NULL, NULL),
	(32, 'Vliegtuig', 2, 1, 21, 0, NULL, NULL),
	(33, 'Onbekend', 6, 0, NULL, 1, 'km', 'input'),
	(34, 'Benzine', 6, 1, 7, 0, NULL, NULL),
	(35, 'Diesel', 6, 1, 8, 0, NULL, NULL),
	(38, 'LPG', 6, 1, 9, 0, NULL, NULL),
	(39, 'Aardgas/CNG', 6, 1, 10, 0, NULL, NULL),
	(40, 'Bio', 6, 1, 11, 0, NULL, NULL),
	(41, 'Waterstof', 6, 1, 12, 0, NULL, NULL),
	(42, 'Elektrisch', 6, 1, 13, 0, NULL, NULL),
	(43, 'Klein (< 950 kg)', 7, 0, NULL, 1, 'km', 'input'),
	(44, 'Middel (> 950 kg < 1.350 kg)', 7, 0, NULL, 1, 'km', 'input'),
	(45, 'Groot (> 1.350 kg)', 7, 0, NULL, 1, 'km', 'input'),
	(46, 'Hybride', 7, 0, NULL, 1, 'km', 'input'),
	(47, 'Plug-in hybride', 7, 0, NULL, 1, 'km', 'input'),
	(48, 'Klein (< 1050 kg)', 8, 0, NULL, 1, 'km', 'input'),
	(49, 'Middel (> 1050 kg < 1.450 kg)', 8, 0, NULL, 1, 'km', 'input'),
	(50, 'Groot (> 1.450 kg)', 8, 0, NULL, 1, 'km', 'input'),
	(51, 'Hybride', 8, 0, NULL, 1, 'km', 'input'),
	(52, 'Klein (< 1000 kg)', 9, 0, NULL, 1, 'km', 'input'),
	(53, 'Middel (> 1050 kg < 1.400 kg)', 9, 0, NULL, 1, 'km', 'input'),
	(54, 'Groot (> 1.400 kg)', 9, 0, NULL, 1, 'km', 'input'),
	(55, 'Klein (< 1000 kg)', 10, 0, NULL, 1, 'km', 'input'),
	(56, 'Middel (> 1050 kg < 1.400 kg)', 10, 0, NULL, 1, 'km', 'input'),
	(57, 'Groot (> 1.400 kg)', 10, 0, NULL, 1, 'km', 'input'),
	(58, 'CNG', 11, 0, NULL, 1, 'km', 'input'),
	(59, 'Ethanol (E85)', 11, 0, NULL, 1, 'km', 'input'),
	(60, 'Diesel (Euro 05)', 11, 0, NULL, 1, 'km', 'input'),
	(61, 'Grijs', 12, 0, NULL, 1, 'km', 'input'),
	(62, 'Groen', 12, 0, NULL, 1, 'km', 'input'),
	(63, 'Grijze stroom', 13, 0, NULL, 1, 'km', 'input'),
	(64, 'Spierkracht', 14, 0, NULL, 1, 'km', 'input'),
	(65, 'Elektrisch', 14, 1, 15, 0, NULL, NULL),
	(66, 'Grijze stroom', 15, 0, NULL, 1, 'km', 'input'),
	(67, 'Benzine', 16, 0, NULL, 1, 'km', 'input'),
	(68, 'Elektrisch', 16, 1, 17, 0, NULL, NULL),
	(69, 'Grijze stroom', 17, 0, NULL, 1, 'km', 'input'),
	(70, 'Benzine', 18, 0, NULL, 1, 'km', 'input'),
	(71, 'Diesel', 18, 0, NULL, 1, 'km', 'input'),
	(72, 'LPG', 18, 0, NULL, 1, 'km', 'input'),
	(73, 'Voertuigkilometer', 19, 0, NULL, 1, 'km', 'input'),
	(74, 'Reizigerskilometer', 19, 0, NULL, 1, 'km', 'input'),
	(75, 'Algemeen', 20, 0, NULL, 1, 'km', 'input'),
	(76, 'Trein type onbekend', 20, 0, NULL, 1, 'km', 'input'),
	(77, 'Stoptrein', 20, 0, NULL, 1, 'km', 'input'),
	(78, 'Intercity ', 20, 0, NULL, 1, 'km', 'input'),
	(79, 'Trein internationaal', 20, 0, NULL, 1, 'km', 'input'),
	(80, 'Bus type onbekend', 20, 0, NULL, 1, 'km', 'input'),
	(81, 'Streekbus ', 20, 0, NULL, 1, 'km', 'input'),
	(82, 'Stadsbus', 20, 0, NULL, 1, 'km', 'input'),
	(83, 'Bus elektrisch ', 20, 0, NULL, 1, 'km', 'input'),
	(84, 'Metro elektrisch ', 20, 0, NULL, 1, 'km', 'input'),
	(85, 'Tram elektrisch ', 20, 0, NULL, 1, 'km', 'input'),
	(86, 'Regionaal (< 700 km)', 21, 0, NULL, 1, 'km', 'input'),
	(87, 'Europees (> 700 < 2500 km)', 21, 0, NULL, 1, 'km', 'input'),
	(88, 'Intercontinentaal (> 2500 km)', 21, 0, NULL, 1, 'km', 'input'),
	(89, 'Belauto', 4, 1, 22, 0, NULL, NULL),
	(90, 'Vrachtwagen', 4, 1, 23, 0, NULL, NULL),
	(91, 'Trein ', 4, 1, 24, 0, NULL, NULL),
	(92, 'Binnenvaart ', 4, 1, 25, 0, NULL, NULL),
	(93, 'Zeevaart ', 4, 1, 26, 0, NULL, NULL),
	(94, '> 2 ton', 22, 0, NULL, 1, 'tonkilometer', 'input'),
	(95, 'Klein (< 10 ton)', 23, 0, NULL, 1, 'tonkilometer', 'input'),
	(96, 'Gemiddeld (10-20 ton)', 23, 0, NULL, 1, 'tonkilometer', 'input'),
	(97, 'Groot (> 20 ton)', 23, 0, NULL, 1, 'tonkilometer', 'input'),
	(98, 'Trekker met oplegger zwaar ', 23, 0, NULL, 1, 'tonkilometer', 'input'),
	(99, 'LZV', 23, 0, NULL, 1, 'tonkilometer', 'input'),
	(100, 'Diesel ', 24, 0, NULL, 1, 'tonkilometer', 'input'),
	(101, 'Elektrisch ', 24, 0, NULL, 1, 'tonkilometer', 'input'),
	(102, 'Gemiddeld', 24, 0, NULL, 1, 'tonkilometer', 'input'),
	(103, 'Klein (300 - 600 ton)', 25, 0, NULL, 1, 'tonkilometer', 'input'),
	(104, 'Gemiddeld (1500 - 3000 ton)', 25, 0, NULL, 1, 'tonkilometer', 'input'),
	(105, 'Groot (5000 - 11000 ton)', 25, 0, NULL, 1, 'tonkilometer', 'input'),
	(106, 'Klein (0 - 5 dwkt)', 26, 0, NULL, 1, 'tonkilometer', 'input'),
	(107, 'Middel (5 - 10 dwkt)', 26, 0, NULL, 1, 'tonkilometer', 'input'),
	(108, 'Groot (10 - 20 dwkt)', 26, 0, NULL, 1, 'tonkilometer', 'input'),
	(109, 'Vrachtwagen ', 5, 1, 27, 0, NULL, NULL),
	(110, 'Trein', 5, 1, 28, 0, NULL, NULL),
	(111, 'Binnenvaart ', 5, 1, 29, 0, NULL, NULL),
	(112, 'Zeevaart ', 5, 1, 30, 0, NULL, NULL),
	(113, '> 20 ton', 27, 0, NULL, 1, 'tonkilometer', 'input'),
	(114, '> 20 ton met aanhanger', 27, 0, NULL, 1, 'tonkilometer', 'input'),
	(115, 'Trekker met oplegger zwaar ', 27, 0, NULL, 1, 'tonkilometer', 'input'),
	(116, 'LZV', 27, 0, NULL, 1, 'tonkilometer', 'input'),
	(117, 'Diesel ', 28, 0, NULL, 1, 'tonkilometer', 'input'),
	(118, 'Elektrisch', 28, 0, NULL, 1, 'tonkilometer', 'input'),
	(119, 'Gemiddeld', 28, 0, NULL, 1, 'tonkilometer', 'input'),
	(120, '40 TEU', 29, 0, NULL, 1, 'tonkilometer', 'input'),
	(121, '96 TEU', 29, 0, NULL, 1, 'tonkilometer', 'input'),
	(122, '208 TEU', 29, 0, NULL, 1, 'tonkilometer', 'input'),
	(123, '348 TEU', 29, 0, NULL, 1, 'tonkilometer', 'input'),
	(124, 'Gemiddeld', 29, 0, NULL, 1, 'tonkilometer', 'input'),
	(125, 'Klein (635 TEU, Feeder)', 30, 0, NULL, 1, 'tonkilometer', 'input'),
	(126, 'Gemiddeld (4080 TEU, Panamax)', 30, 0, NULL, 1, 'tonkilometer', 'input'),
	(127, 'Groot (8170 TEU, Suezmax) ', 30, 0, NULL, 1, 'tonkilometer', 'input'),
	(128, 'Stroometiket', 31, 0, NULL, 1, 'kWh', 'input'),
	(129, 'Grijze stroom ', 31, 0, NULL, 1, 'kWh', 'input'),
	(130, 'Stroom (onbekend)', 31, 0, NULL, 1, 'kWh', 'input'),
	(131, 'Windkracht ', 31, 0, NULL, 1, 'kWh', 'input'),
	(132, 'Waterkracht ', 31, 0, NULL, 1, 'kWh', 'input'),
	(133, 'Zonne-energie ', 31, 0, NULL, 1, 'kWh', 'input'),
	(134, 'Biomassa ', 31, 0, NULL, 1, 'kWh', 'input'),
	(135, 'Stookolie ', 32, 0, NULL, 1, 'liter', 'input'),
	(136, 'Ruwe aardolie ', 32, 0, NULL, 1, 'kg', 'input'),
	(137, 'Rimulsion ', 32, 0, NULL, 1, 'kg', 'input'),
	(138, 'Aardgascondensaat ', 32, 0, NULL, 1, 'kg', 'input'),
	(139, 'Petroleum ', 32, 0, NULL, 1, 'kg', 'input'),
	(140, 'Leisteenolie', 32, 0, NULL, 1, 'kg', 'input'),
	(141, 'Ethaan ', 32, 0, NULL, 1, 'kg', 'input'),
	(142, 'Nafta ', 32, 0, NULL, 1, 'kg', 'input'),
	(143, 'Bitumen ', 32, 0, NULL, 1, 'kg', 'input'),
	(144, 'Smeeroliën ', 32, 0, NULL, 1, 'kg', 'input'),
	(145, 'Petroleum cokes ', 32, 0, NULL, 1, 'kg', 'input'),
	(146, 'Raffinaderij ', 32, 0, NULL, 1, 'kg', 'input'),
	(147, 'Raffinaderij gas ', 32, 0, NULL, 1, 'kg', 'input'),
	(148, 'Chemisch restgas ', 32, 0, NULL, 1, 'kg', 'input'),
	(149, 'Overige oliën ', 32, 0, NULL, 1, 'kg', 'input'),
	(150, 'Antraciet ', 32, 0, NULL, 1, 'kg', 'input'),
	(151, 'Cokeskolen', 32, 0, NULL, 1, 'kg', 'input'),
	(152, 'Cokeskolen (cokeovens)', 32, 0, NULL, 1, 'kg', 'input'),
	(153, 'Cokeskolen (basismeta)', 32, 0, NULL, 1, 'kg', 'input'),
	(154, 'Steenkool ', 32, 0, NULL, 1, 'kg', 'input'),
	(155, 'Sub-biumeneuze kool ', 32, 0, NULL, 1, 'kg', 'input'),
	(156, 'Bruinkool ', 32, 0, NULL, 1, 'kg', 'input'),
	(157, 'Bitimuneuze leisteen', 32, 0, NULL, 1, 'kg', 'input'),
	(158, 'Turf ', 32, 0, NULL, 1, 'kg', 'input'),
	(159, 'Steenkool-bruinkool ', 32, 0, NULL, 1, 'kg', 'input'),
	(160, 'Aardgas ', 32, 0, NULL, 1, 'Nm3', 'input'),
	(161, 'Propaan ', 32, 0, NULL, 1, 'liter', 'input'),
	(162, 'Biogas (stortgas) ', 32, 0, NULL, 1, 'Nm3', 'input'),
	(163, 'Biogas (convergisting) ', 32, 0, NULL, 1, 'Nm3', 'input'),
	(164, 'Houtchips (NL)', 32, 0, NULL, 1, 'kg ds', 'input'),
	(165, 'Shreds (NL)', 32, 0, NULL, 1, 'kg ds', 'input'),
	(166, 'Pellets uit (droge) instustie restroom (NL)', 32, 0, NULL, 1, 'kg ds', 'input'),
	(167, 'Pellets uit (droge) instustie restroom (NL)', 32, 0, NULL, 1, 'kg ds', 'input'),
	(168, 'Houtblokken (NL) ', 32, 0, NULL, 1, 'kg ds', 'input'),
	(169, 'STEG-centrale ', 33, 0, NULL, 1, 'GJ', 'input'),
	(170, 'Afvalverbrandinginstallatie ', 33, 0, NULL, 1, 'GJ', 'input'),
	(171, 'Geometrie', 33, 0, NULL, 1, 'GJ', 'input'),
	(172, 'Biomassa (pelletes) ', 33, 0, NULL, 1, 'GJ', 'input'),
	(173, 'Restwarmte met bijstook ', 33, 0, NULL, 1, 'GJ', 'input'),
	(174, 'Restwarmte zonder bijstook ', 33, 0, NULL, 1, 'GJ', 'input'),
	(175, 'R22', 34, 0, NULL, 1, 'kg', 'input'),
	(176, 'R134a', 34, 0, NULL, 1, 'kg', 'input'),
	(177, 'R125', 34, 0, NULL, 1, 'kg', 'input'),
	(178, 'R143a', 34, 0, NULL, 1, 'kg', 'input'),
	(179, 'R32', 34, 0, NULL, 1, 'kg', 'input'),
	(180, 'R404a', 34, 0, NULL, 1, 'kg', 'input'),
	(181, 'R507', 34, 0, NULL, 1, 'kg', 'input'),
	(182, 'R407c', 34, 0, NULL, 1, 'kg', 'input'),
	(183, 'R410a', 34, 0, NULL, 1, 'kg', 'input'),
	(184, 'R417a', 34, 0, NULL, 1, 'kg', 'input'),
	(185, 'R422d', 34, 0, NULL, 1, 'kg', 'input'),
	(186, '1234yf', 34, 0, NULL, 1, 'kg', 'input'),
	(187, '1234ze', 34, 0, NULL, 1, 'kg', 'input'),
	(188, 'R744', 34, 0, NULL, 1, 'kg', 'input'),
	(189, 'R448A', 34, 0, NULL, 1, 'kg', 'input'),
	(190, 'R449A', 34, 0, NULL, 1, 'kg', 'input'),
	(191, 'R450A', 34, 0, NULL, 1, 'kg', 'input'),
	(192, 'R452B', 34, 0, NULL, 1, 'kg', 'input'),
	(193, 'R513A', 34, 0, NULL, 1, 'kg', 'input');
/*!40000 ALTER TABLE `category_answer` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.category_answer_equivalents wordt geschreven
DROP TABLE IF EXISTS `category_answer_equivalents`;
CREATE TABLE IF NOT EXISTS `category_answer_equivalents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) DEFAULT NULL,
  `equivalent` decimal(11,4) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.category_answer_equivalents: ~166 rows (ongeveer)
DELETE FROM `category_answer_equivalents`;
/*!40000 ALTER TABLE `category_answer_equivalents` DISABLE KEYS */;
INSERT INTO `category_answer_equivalents` (`id`, `answer_id`, `equivalent`, `version`) VALUES
	(1, 1, 2.7400, 1),
	(2, 2, 2.8000, 1),
	(3, 3, 2.8800, 1),
	(4, 4, 1.0830, 1),
	(5, 5, 1.2400, 1),
	(6, 6, 2.1860, 1),
	(7, 7, 1.3900, 1),
	(8, 8, 0.9140, 1),
	(9, 9, 3.2300, 1),
	(10, 10, 3.2000, 1),
	(11, 11, 3.2400, 1),
	(12, 12, 3.1540, 1),
	(13, 13, 1.9200, 1),
	(14, 14, 0.3450, 1),
	(15, 15, 12.0000, 1),
	(16, 16, 0.7600, 1),
	(17, 17, 1.8060, 1),
	(18, 18, 1.9000, 1),
	(19, 19, 3.3700, 1),
	(20, 20, 2.7280, 1),
	(21, 21, 3.0700, 1),
	(22, 22, 1.0390, 1),
	(23, 23, 3.5300, 1),
	(24, 24, 3.4900, 1),
	(25, 25, 3.3100, 1),
	(26, 33, 0.2200, 1),
	(27, 43, 0.1770, 1),
	(28, 44, 0.2240, 1),
	(29, 45, 0.2530, 1),
	(30, 46, 0.1710, 1),
	(31, 47, 0.1460, 1),
	(32, 48, 0.1680, 1),
	(33, 49, 0.2130, 1),
	(34, 50, 0.2410, 1),
	(35, 51, 0.1570, 1),
	(36, 52, 0.1550, 1),
	(37, 53, 0.1960, 1),
	(38, 54, 0.2210, 1),
	(39, 55, 0.1490, 1),
	(40, 56, 0.1890, 1),
	(41, 57, 0.2140, 1),
	(42, 58, 0.0750, 1),
	(43, 59, 0.1220, 1),
	(44, 60, 0.2070, 1),
	(45, 61, 0.1120, 1),
	(46, 62, 0.0070, 1),
	(47, 63, 0.1070, 1),
	(48, 64, 0.0000, 1),
	(49, 66, 0.0060, 1),
	(50, 67, 0.0780, 1),
	(51, 69, 0.0430, 1),
	(52, 70, 0.2980, 1),
	(53, 71, 0.3120, 1),
	(54, 72, 0.2740, 1),
	(55, 73, 0.0330, 1),
	(56, 74, 1.0430, 1),
	(57, 75, 0.0360, 1),
	(58, 76, 0.0060, 1),
	(59, 77, 0.0240, 1),
	(60, 78, 0.0000, 1),
	(61, 79, 0.0260, 1),
	(62, 80, 0.1400, 1),
	(63, 81, 0.1350, 1),
	(64, 82, 0.1460, 1),
	(65, 83, 0.1340, 1),
	(66, 84, 0.0740, 1),
	(67, 85, 0.0660, 1),
	(68, 86, 0.2970, 1),
	(69, 87, 0.2000, 1),
	(70, 88, 0.1470, 1),
	(71, 94, 1.1530, 1),
	(72, 95, 0.4320, 1),
	(73, 96, 0.2590, 1),
	(74, 97, 0.1100, 1),
	(75, 98, 0.0820, 1),
	(76, 99, 0.0790, 1),
	(77, 100, 0.0180, 1),
	(78, 101, 0.0100, 1),
	(79, 102, 0.0120, 1),
	(80, 103, 0.0410, 1),
	(81, 104, 0.0300, 1),
	(82, 105, 0.0210, 1),
	(83, 106, 0.0270, 1),
	(84, 107, 0.0210, 1),
	(85, 108, 0.0150, 1),
	(86, 113, 0.2000, 1),
	(87, 114, 0.1170, 1),
	(88, 115, 0.1020, 1),
	(89, 116, 0.0930, 1),
	(90, 117, 0.0300, 1),
	(91, 118, 0.0160, 1),
	(92, 119, 0.0190, 1),
	(93, 120, 0.0450, 1),
	(94, 121, 0.0440, 1),
	(95, 122, 0.0240, 1),
	(96, 123, 0.0170, 1),
	(97, 124, 0.0190, 1),
	(98, 125, 0.0350, 1),
	(99, 126, 0.0210, 1),
	(100, 127, 0.0150, 1),
	(101, 128, NULL, 1),
	(102, 129, 0.6490, 1),
	(103, 130, 0.4130, 1),
	(104, 131, 0.0000, 1),
	(105, 132, 0.0000, 1),
	(106, 133, 0.0000, 1),
	(107, 134, 0.0750, 1),
	(108, 135, 3.1850, 1),
	(109, 136, 3.1300, 1),
	(110, 137, 2.1180, 1),
	(111, 138, 2.8250, 1),
	(112, 139, 3.0990, 1),
	(113, 140, 2.7930, 1),
	(114, 141, 2.7840, 1),
	(115, 142, 3.2250, 1),
	(116, 143, 3.3810, 1),
	(117, 144, 3.0350, 1),
	(118, 145, 3.4320, 1),
	(119, 146, 3.1520, 1),
	(120, 147, 3.0280, 1),
	(121, 148, 2.8200, 1),
	(122, 149, 2.9470, 1),
	(123, 150, 2.8800, 1),
	(124, 151, 2.6880, 1),
	(125, 152, 2.7280, 1),
	(126, 153, 2.5680, 1),
	(127, 154, 2.3680, 1),
	(128, 155, 1.8160, 1),
	(129, 156, 2.0200, 1),
	(130, 157, 0.9520, 1),
	(131, 158, 1.0350, 1),
	(132, 159, 2.0180, 1),
	(133, 160, 1.8900, 1),
	(134, 161, 1.7250, 1),
	(135, 162, 0.3980, 1),
	(136, 163, 1.2600, 1),
	(137, 164, 0.0620, 1),
	(138, 165, 0.0540, 1),
	(139, 166, 0.0350, 1),
	(140, 167, 0.5560, 1),
	(141, 168, 0.0770, 1),
	(142, 169, 35.9700, 1),
	(143, 170, 26.4900, 1),
	(144, 171, 25.0500, 1),
	(145, 172, 25.8200, 1),
	(146, 173, 21.5300, 1),
	(147, 174, 8.8000, 1),
	(148, 175, 1810.0000, 1),
	(149, 176, 1430.0000, 1),
	(150, 177, 3500.0000, 1),
	(151, 178, 4470.0000, 1),
	(152, 179, 675.0000, 1),
	(153, 180, 3922.0000, 1),
	(154, 181, 3985.0000, 1),
	(155, 182, 1774.0000, 1),
	(156, 183, 2088.0000, 1),
	(157, 184, 2346.0000, 1),
	(158, 185, 2729.0000, 1),
	(159, 186, 4.0000, 1),
	(160, 187, 1.0000, 1),
	(161, 188, 1.0000, 1),
	(162, 189, 1387.0000, 1),
	(163, 190, 1397.0000, 1),
	(164, 191, 601.0000, 1),
	(165, 192, 698.0000, 1),
	(166, 193, 631.0000, 1);
/*!40000 ALTER TABLE `category_answer_equivalents` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.category_question wordt geschreven
DROP TABLE IF EXISTS `category_question`;
CREATE TABLE IF NOT EXISTS `category_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `is_parent` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.category_question: ~34 rows (ongeveer)
DELETE FROM `category_question`;
/*!40000 ALTER TABLE `category_question` DISABLE KEYS */;
INSERT INTO `category_question` (`id`, `category_id`, `question`, `excerpt`, `is_parent`) VALUES
	(1, 1, 'Welke types brandstof worden er gebruikt voor het personenvervoer?', NULL, 1),
	(2, 1, 'Welke vervoersmiddelen worden gebruikt voor het woon-werk verkeer?', NULL, 1),
	(3, 2, 'Welke types brandstof worden er gebruikt voor het goederenvervoer?', NULL, 1),
	(4, 2, 'Welke vervoersmiddelen worden gebruikt voor het transport van bulkgoederen?', NULL, 1),
	(5, 2, 'Welke vervoersmiddelen worden gebruikt voor het transport van containers?', NULL, 1),
	(6, 0, 'Welk type motor gebruiken de auto(s)', NULL, 0),
	(7, 0, 'In welke categorie valt de auto', NULL, 0),
	(8, 0, 'In welke categorie valt de auto', NULL, 0),
	(9, 0, 'In welke categorie valt de auto', NULL, 0),
	(10, 0, 'In welke categorie valt de auto', NULL, 0),
	(11, 0, 'In welke categorie valt de auto', NULL, 0),
	(12, 0, 'In welke categorie valt de auto', NULL, 0),
	(13, 0, 'In welke categorie valt de auto', NULL, 0),
	(14, 0, 'Hoe wordt de fiets aangedreven?', NULL, 0),
	(15, 0, 'Via welke energie wordt de fiets aangedreven?', NULL, 0),
	(16, 0, 'Hoe wordt de scooter / fiets aangedreven?', NULL, 0),
	(17, 0, 'Via welke energie wordt de scooter / motor aangedreven?', NULL, 0),
	(18, 0, 'Hoe wordt de minibus aangedreven?', NULL, 0),
	(19, 0, 'Op welke manier worden de kilometers aangegeven?', NULL, 0),
	(20, 0, 'Welk soort openbaar vervoer wordt er gebruikt?', NULL, 0),
	(21, 0, 'Welke soort vluchten worden gebruikt? ', NULL, 0),
	(22, 0, 'In welke categorie valt de belauto?', NULL, 0),
	(23, 0, 'In welke categorie vallen de vrachtwagen(s)?', NULL, 0),
	(24, 0, 'In welke categorie vallen de treinen? ', NULL, 0),
	(25, 0, 'In welke categorie vallen de schepen voor binnenvaart? ', NULL, 0),
	(26, 0, 'In welke categorie vallen de schepen voor zeevaart? ', NULL, 0),
	(27, 0, 'In welke categorie vallen de vrachtwagen(s)?', NULL, 0),
	(28, 0, 'In welke categorie vallen de treinen? ', NULL, 0),
	(29, 0, 'In welke categorie vallen de schepen voor binnenvaart? ', NULL, 0),
	(30, 0, 'In welke categorie vallen de schepen voor zeevaart? ', NULL, 0),
	(31, 3, 'Welke soort elektriciteit wordt er gebruikt? ', NULL, 1),
	(32, 4, 'Welke soort brandstoffen worden gebruikt voor warmteopwekking?', NULL, 1),
	(33, 5, 'Welke warmtelevering wordt er gebruikt? ', NULL, 1),
	(34, 6, 'Welke koel- en koudemiddelen worden er gebruikt? ', NULL, 1);
/*!40000 ALTER TABLE `category_question` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.companies wordt geschreven
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `kvk_number` varchar(8) DEFAULT NULL,
  `sbi_code` varchar(255) DEFAULT NULL,
  `sbi_code_description` varchar(255) DEFAULT NULL,
  `establishment_pc` varchar(255) DEFAULT NULL,
  `establishment_street` varchar(255) DEFAULT NULL,
  `establishment_number` varchar(255) DEFAULT NULL,
  `establishment_city` varchar(255) DEFAULT NULL,
  `establishment_country` varchar(255) DEFAULT NULL,
  `establishment_code` varchar(255) DEFAULT NULL,
  `main_branch` int(11) DEFAULT '0',
  `fte` varchar(15) DEFAULT NULL,
  `sqft` varchar(15) DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=hp8;

-- Dumpen data van tabel roaming_souls_c.companies: ~6 rows (ongeveer)
DELETE FROM `companies`;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `user_id`, `company_name`, `kvk_number`, `sbi_code`, `sbi_code_description`, `establishment_pc`, `establishment_street`, `establishment_number`, `establishment_city`, `establishment_country`, `establishment_code`, `main_branch`, `fte`, `sqft`, `is_deleted`) VALUES
	(1, 15, 'V.O.F. van den Oord', '73411221', '01422', 'Overige vleesveehouderij en zoogkoeienbedrijven', '5688LK', 'Nieuwedijk', '47 ', 'Oirschot', 'Nederland', '000023420952', 1, NULL, NULL, '0'),
	(2, 16, 'De Haagsche Hondentrimmer', '74727931', '9609', 'Overige dienstverlening (rest)', '2552NZ', 'Folke Bernadottestraat', '4 ', '\'s-Gravenhage', 'Nederland', '000042664020', 1, NULL, NULL, '0'),
	(5, 1, 'Power-ED', '66110467', '70222', 'Advisering op het gebied van management en bedrijfsvoering (geen public relations en organisatie- adviesbureaus)', '5688GA', 'Moorland', '4 ', 'Oirschot', 'Nederland', '000034764232', 1, '305', '150', '0'),
	(6, 1, 'Deloitte', NULL, NULL, NULL, '1090', 'Gateway ', '1J', 'Zaventem', 'België', NULL, 0, '45000', '85000', '0'),
	(7, 0, 'De Haagsche Hondentrimmer', '74727931', '9609', 'Overige dienstverlening (rest)', '2552NZ', 'Folke Bernadottestraat', '4 ', '\'s-Gravenhage', 'Nederland', '000042664020', 1, NULL, NULL, '1'),
	(8, 0, 'De Haagsche Hondentrimmer', '74727931', '9609', 'Overige dienstverlening (rest)', '2552NZ', 'Folke Bernadottestraat', '4 ', '\'s-Gravenhage', 'Nederland', '000042664020', 1, NULL, NULL, '1');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.footprint_answers wordt geschreven
DROP TABLE IF EXISTS `footprint_answers`;
CREATE TABLE IF NOT EXISTS `footprint_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `parent_question_id` int(11) DEFAULT NULL,
  `review_parent_question_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `review_question_id` int(11) DEFAULT NULL,
  `input_value` varchar(255) DEFAULT NULL,
  `review_input_value` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.footprint_answers: ~8 rows (ongeveer)
DELETE FROM `footprint_answers`;
/*!40000 ALTER TABLE `footprint_answers` DISABLE KEYS */;
INSERT INTO `footprint_answers` (`id`, `survey_id`, `user_id`, `reviewer_id`, `parent_question_id`, `review_parent_question_id`, `question_id`, `review_question_id`, `input_value`, `review_input_value`, `company_id`) VALUES
	(1, 1, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, 5),
	(2, 1, 1, NULL, 1, NULL, 10, NULL, NULL, NULL, 5),
	(3, 1, 1, NULL, 1, NULL, NULL, NULL, '3500', NULL, 5),
	(4, 1, 1, NULL, 10, NULL, NULL, NULL, '12500', NULL, 5),
	(5, 1, 1, NULL, 2, NULL, 26, NULL, NULL, NULL, 5),
	(6, 1, 1, NULL, 26, NULL, 35, NULL, NULL, NULL, 5),
	(7, 1, 1, NULL, 35, NULL, 50, NULL, NULL, NULL, 5),
	(8, 1, 1, NULL, 50, NULL, NULL, NULL, '358006', NULL, 5);
/*!40000 ALTER TABLE `footprint_answers` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.footprint_collection wordt geschreven
DROP TABLE IF EXISTS `footprint_collection`;
CREATE TABLE IF NOT EXISTS `footprint_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `submodule_id` int(11) DEFAULT NULL,
  `input_value` varchar(255) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `equivalent_result` decimal(16,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.footprint_collection: ~3 rows (ongeveer)
DELETE FROM `footprint_collection`;
/*!40000 ALTER TABLE `footprint_collection` DISABLE KEYS */;
INSERT INTO `footprint_collection` (`id`, `user_id`, `company_id`, `survey_id`, `submodule_id`, `input_value`, `question_id`, `equivalent_result`) VALUES
	(1, 1, 5, 1, 10, '3500', 1, 9590.0000),
	(2, 1, 5, 1, 10, '12500', 10, 40000.0000),
	(3, 1, 5, 1, 10, '358006', 50, 86279.4460);
/*!40000 ALTER TABLE `footprint_collection` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.goal wordt geschreven
DROP TABLE IF EXISTS `goal`;
CREATE TABLE IF NOT EXISTS `goal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goal` varchar(255) DEFAULT NULL,
  `goal_description` varchar(255) DEFAULT NULL,
  `goal_version` varchar(255) DEFAULT NULL,
  `goal_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.goal: ~15 rows (ongeveer)
DELETE FROM `goal`;
/*!40000 ALTER TABLE `goal` DISABLE KEYS */;
INSERT INTO `goal` (`id`, `goal`, `goal_description`, `goal_version`, `goal_location`) VALUES
	(1, 'Geen armoede', 'Beëindig armoede overal en in al haar vormen\r\n', 'nl', 'resources/images/sdg/'),
	(2, 'Geen honger', 'Beëindig honger, bereik voedselzekerheid en verbeterde voeding en promoot duurzame landbouw', 'nl', 'resources/images/sdg/'),
	(3, 'Goede gezondheid en welzijn', 'Verzeker een goede gezondheid en promoot welvaart voor alle leeftijden\r\n', 'nl', 'resources/images/sdg/'),
	(4, 'Kwaliteitsonderwijs', 'Verzeker gelijke toegang tot kwaliteitsvol onderwijs en bevorder levenslang leren voor iedereen', 'nl', 'resources/images/sdg/'),
	(5, 'Gendergelijkheid', 'Bereik gendergelijkheid en empowerment voor alle vrouwen en meisjes', 'nl', 'resources/images/sdg/'),
	(6, 'Schoon water en sanitair', 'Verzeker toegang tot duurzaam beheer van water en sanitatie voor iedereen\r\n', 'nl', 'resources/images/sdg/'),
	(7, 'Betaalbare en duurzame energie', 'Verzeker toegang tot betaalbare, betrouwbare, duurzame en moderne energie voor iedereen\r\n', 'nl', 'resources/images/sdg/'),
	(8, 'Waardig werk en economische groei', 'Bevorder aanhoudende, inclusieve, en duurzame economische groei, volledige en productieve tewerkstelling en waardig werk voor iedereen\r\n', 'nl', 'resources/images/sdg/'),
	(9, 'Industrie, innovatie en infrastructuur', 'Bouw veerkrachtige infrastructuur, bevorder inclusieve en duurzame industrialisering en stimuleer innovatie\r\n', 'nl', 'resources/images/sdg/'),
	(10, 'Ongelijkheid verminderen', 'Dring ongelijkheid in en tussen landen terug\r\n', 'nl', 'resources/images/sdg/'),
	(11, 'Duurzame steden en gemeenschappen', 'Maak steden en menselijke nederzettingen inclusief, veilig, veerkrachtig en duurzaam\r\n', 'nl', 'resources/images/sdg/'),
	(12, 'Verantwoorde consumptie en productie', 'Verzeker duurzame consumptie-, en productiepatronen\r\n', 'nl', 'resources/images/sdg/'),
	(13, 'Klimaatactie', 'Neem dringend actie om klimaatverandering en haar impact te bestrijden\r\n', 'nl', 'resources/images/sdg/'),
	(14, 'Leven in het water', 'Behoud en maak duurzaam gebruik van de oceanen, de zeeën en maritieme hulpbronnen\r\n', 'nl', 'resources/images/sdg/'),
	(15, 'Leven op het land', 'Bescherm, herstel en bevorder het duurzaam gebruik van ecosystemen, beheer bossen duurzaam, bestrijd woestijnvorming en landdegradatie en draai het terug en roep het verlies aan biodiversiteit een halt toe\r\n', 'nl', 'resources/images/sdg/'),
	(16, 'Vrede, justitie en sterke publieke diensten', 'Bevorder vreedzame en inclusieve samenlevingen met het oog op duurzame ontwikkeling, verzeker toegang tot justitie voor iedereen en creëer op alle niveaus doeltreffende, verantwoordelijke en open instellingen\r\n', 'nl', 'resources/images/sdg/'),
	(17, 'Partnerschap om de doelen te bereiken', 'Versterk de implementatiemiddelen en revitaliseer het wereldwijd partnerschap voor duurzame ontwikkeling\r\n', 'nl', 'resources/images/sdg/');
/*!40000 ALTER TABLE `goal` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.module wordt geschreven
DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.module: ~3 rows (ongeveer)
DELETE FROM `module`;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `alias`, `name`) VALUES
	(1, 'mvo', 'MVO scan'),
	(2, 'footprint', 'CO2 scan'),
	(3, 'sdg', 'SDG scan');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.open_question_answers wordt geschreven
DROP TABLE IF EXISTS `open_question_answers`;
CREATE TABLE IF NOT EXISTS `open_question_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `open_question_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `answer_date` int(11) NOT NULL,
  `review_answer_date` int(11) DEFAULT NULL,
  `answer` text NOT NULL,
  `review_answer` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.open_question_answers: ~27 rows (ongeveer)
DELETE FROM `open_question_answers`;
/*!40000 ALTER TABLE `open_question_answers` DISABLE KEYS */;
INSERT INTO `open_question_answers` (`id`, `user_id`, `reviewer_id`, `open_question_id`, `survey_id`, `answer_date`, `review_answer_date`, `answer`, `review_answer`) VALUES
	(1, 1, 1, 1, 1, 1612168138, 1612175687, '', 'Bloedbaan'),
	(2, 1, 1, 2, 1, 1612168138, 1612175687, '', ''),
	(3, 1, 1, 3, 1, 1612168138, 1612175687, 'Soms', 'Soms'),
	(4, 1, 1, 4, 1, 1612168138, 1612175687, '', ''),
	(5, 1, 1, 5, 1, 1612168138, 1612175687, '', ''),
	(6, 1, 1, 6, 1, 1612168138, 1612175687, '', ''),
	(7, 1, 1, 7, 1, 1612168138, 1612175687, '', ''),
	(8, 1, 1, 8, 1, 1612168138, 1612175687, 'Ja, heel vaak zelfs.', 'Ja, heel vaak zelfs.'),
	(9, 1, 1, 9, 1, 1612168138, 1612175687, '', ''),
	(10, 1, NULL, 1, 2, 1620384050, NULL, 'test', NULL),
	(11, 1, NULL, 2, 2, 1620384050, NULL, 'test 2', NULL),
	(12, 1, NULL, 3, 2, 1620384050, NULL, '', NULL),
	(13, 1, NULL, 4, 2, 1620384050, NULL, 'setret', NULL),
	(14, 1, NULL, 5, 2, 1620384050, NULL, 'yes', NULL),
	(15, 1, NULL, 6, 2, 1620384050, NULL, 'taees', NULL),
	(16, 1, NULL, 7, 2, 1620384050, NULL, 'azerty', NULL),
	(17, 1, NULL, 8, 2, 1620384050, NULL, '', NULL),
	(18, 1, NULL, 9, 2, 1620384050, NULL, '', NULL),
	(19, 1, NULL, 1, 6, 1625593649, NULL, '', NULL),
	(20, 1, NULL, 2, 6, 1625593649, NULL, '', NULL),
	(21, 1, NULL, 3, 6, 1625593649, NULL, '', NULL),
	(22, 1, NULL, 4, 6, 1625593649, NULL, '', NULL),
	(23, 1, NULL, 5, 6, 1625593649, NULL, '', NULL),
	(24, 1, NULL, 6, 6, 1625593649, NULL, '', NULL),
	(25, 1, NULL, 7, 6, 1625593649, NULL, '', NULL),
	(26, 1, NULL, 8, 6, 1625593649, NULL, '', NULL),
	(27, 1, NULL, 9, 6, 1625593649, NULL, '', NULL);
/*!40000 ALTER TABLE `open_question_answers` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.open_question_description wordt geschreven
DROP TABLE IF EXISTS `open_question_description`;
CREATE TABLE IF NOT EXISTS `open_question_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submodule_id` int(11) DEFAULT NULL,
  `question_description` text,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.open_question_description: ~9 rows (ongeveer)
DELETE FROM `open_question_description`;
/*!40000 ALTER TABLE `open_question_description` DISABLE KEYS */;
INSERT INTO `open_question_description` (`id`, `submodule_id`, `question_description`, `version`) VALUES
	(1, 1, 'In welke documenten heb je dit vastgelegd en hoe houd je medewerkers op de hoogte van wijzigingen?', 1),
	(2, 2, 'Hoe blijven jullie op de hoogte van de laatste Wet en regelgeving?', 1),
	(3, 3, 'Is het voor de medewerkers duidelijk welke koers je wilt varen? Hoe neem je medewerkers daar in mee? Geef je als leiding het goede voorbeeld?', 1),
	(4, 4, 'Hoe zijn de arbeidsomstandigheden, is er aandacht voor Maatschappelijk Verantwoord Ondernemen?', 1),
	(5, 5, 'Hoe professionaliseren jullie de relatie met jullie klanten?', 1),
	(6, 6, 'Hoe geeft jou bedrijf invulling aan de lokale economie / omgeving?', 1),
	(7, 7, 'Hoe betrekken jullie leveranciers en klanten bij jullie bedrijf? Zoekt jouw bedrijf samen met hun naar de beste oplossingen?', 1),
	(8, 8, 'Hoe zuinig gaan jullie om met grondstoffen? Word je bewust gemaakt hoe het beter kan?', 1),
	(9, 9, 'Hoe geven jullie invulling aan goede communicatie?', 1);
/*!40000 ALTER TABLE `open_question_description` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.permission_sets wordt geschreven
DROP TABLE IF EXISTS `permission_sets`;
CREATE TABLE IF NOT EXISTS `permission_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL COMMENT 'Don''t change values!',
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=hp8;

-- Dumpen data van tabel roaming_souls_c.permission_sets: ~4 rows (ongeveer)
DELETE FROM `permission_sets`;
/*!40000 ALTER TABLE `permission_sets` DISABLE KEYS */;
INSERT INTO `permission_sets` (`id`, `label`, `permissions`) VALUES
	(1, 'Super Administrator', '{"api":{"view_api":true,"create_api":true,"update_api":true,"delete_api":true},"clients":{"view_clients":true,"create_clients":true,"update_clients":true,"delete_clients":true},"contacts":{"view_contacts":true,"create_contacts":true,"update_contacts":true,"delete_contacts":true},"surveys":{"view_surveys":true,"create_surveys":true,"update_surveys":true,"delete_surveys":true},"permissions":{"view_permissions":true,"create_permissions":true,"update_permissions":true,"delete_permissions":true},"users":{"view_users":true,"create_users":true,"update_users":true,"delete_users":true}}'),
	(2, 'Administrator', '{"users":{"view_users":true,"create_users":true,"update_users":true,"delete_users":false},"surveys":{"view_surveys":true,"create_surveys":true,"update_surveys":false,"delete_surveys":false},"permissions":{"view_permissions":true,"create_permissions":true,"update_permissions":false,"delete_permissions":false},"contacts":{"view_contacts":true,"create_contacts":true,"update_contacts":true,"delete_contacts":false}}'),
	(3, 'Owner', '{"users":{"view_users":false,"create_users":false,"update_users":false,"delete_users":false},"surveys":{"view_surveys":true,"create_surveys":false,"update_surveys":false,"delete_surveys":false},"permissions":{"view_permissions":false,"create_permissions":false,"update_permissions":false,"delete_permissions":false},"contacts":{"view_contacts":true,"create_contacts":true,"update_contacts":true,"delete_contacts":false}}'),
	(4, 'Contact', '{"users":{"view_users":false,"create_users":false,"update_users":false,"delete_users":false},"surveys":{"view_surveys":true,"create_surveys":false,"update_surveys":false,"delete_surveys":false},"permissions":{"view_permissions":false,"create_permissions":false,"update_permissions":false,"delete_permissions":false},"contacts":{"view_contacts":false,"create_contacts":false,"update_contacts":false,"delete_contacts":false}}');
/*!40000 ALTER TABLE `permission_sets` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.question wordt geschreven
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `has_answer_set_id` int(11) NOT NULL,
  `answer_set_id` int(11) NOT NULL,
  `question_intro_id` int(11) NOT NULL,
  `question_description_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.question: ~121 rows (ongeveer)
DELETE FROM `question`;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` (`id`, `type`, `has_answer_set_id`, `answer_set_id`, `question_intro_id`, `question_description_id`) VALUES
	(1, 'dropdown', 1, 1, 1, 1),
	(2, 'dropdown', 1, 1, 1, 2),
	(3, 'dropdown', 1, 1, 1, 3),
	(4, 'dropdown', 1, 1, 1, 4),
	(5, 'dropdown', 1, 1, 1, 5),
	(6, 'dropdown', 1, 1, 1, 6),
	(7, 'dropdown', 1, 1, 1, 7),
	(8, 'dropdown', 1, 1, 2, 8),
	(9, 'dropdown', 1, 1, 2, 9),
	(10, 'dropdown', 1, 1, 2, 10),
	(11, 'dropdown', 1, 1, 2, 11),
	(12, 'dropdown', 1, 1, 2, 12),
	(13, 'dropdown', 1, 1, 2, 13),
	(14, 'dropdown', 1, 1, 2, 14),
	(15, 'dropdown', 1, 1, 3, 15),
	(16, 'dropdown', 1, 1, 3, 16),
	(17, 'dropdown', 1, 1, 3, 17),
	(18, 'dropdown', 1, 1, 3, 18),
	(19, 'dropdown', 1, 1, 3, 19),
	(20, 'dropdown', 1, 1, 3, 20),
	(21, 'dropdown', 1, 1, 3, 21),
	(22, 'dropdown', 1, 1, 3, 22),
	(23, 'dropdown', 1, 1, 3, 23),
	(24, 'dropdown', 1, 1, 3, 24),
	(25, 'dropdown', 1, 1, 4, 25),
	(26, 'dropdown', 1, 1, 4, 26),
	(27, 'dropdown', 1, 1, 4, 27),
	(28, 'dropdown', 1, 1, 4, 28),
	(29, 'dropdown', 1, 1, 4, 29),
	(30, 'dropdown', 1, 1, 4, 30),
	(31, 'dropdown', 1, 1, 4, 31),
	(32, 'dropdown', 1, 1, 4, 32),
	(33, 'dropdown', 1, 1, 4, 33),
	(34, 'dropdown', 1, 1, 4, 34),
	(35, 'dropdown', 1, 1, 4, 35),
	(36, 'dropdown', 1, 1, 5, 36),
	(37, 'dropdown', 1, 1, 5, 37),
	(38, 'dropdown', 1, 1, 5, 38),
	(39, 'dropdown', 1, 1, 5, 39),
	(40, 'dropdown', 1, 1, 5, 40),
	(41, 'dropdown', 1, 1, 5, 41),
	(42, 'dropdown', 1, 1, 5, 42),
	(43, 'dropdown', 1, 1, 5, 43),
	(44, 'dropdown', 1, 1, 5, 44),
	(45, 'dropdown', 1, 1, 5, 45),
	(46, 'dropdown', 1, 1, 5, 46),
	(47, 'dropdown', 1, 1, 6, 47),
	(48, 'dropdown', 1, 1, 6, 48),
	(49, 'dropdown', 1, 1, 6, 49),
	(50, 'dropdown', 1, 1, 6, 50),
	(51, 'dropdown', 1, 1, 6, 51),
	(52, 'dropdown', 1, 1, 6, 52),
	(53, 'dropdown', 1, 1, 7, 53),
	(54, 'dropdown', 1, 1, 7, 54),
	(55, 'dropdown', 1, 1, 7, 55),
	(56, 'dropdown', 1, 1, 7, 56),
	(57, 'dropdown', 1, 1, 7, 57),
	(58, 'dropdown', 1, 1, 7, 58),
	(59, 'dropdown', 1, 1, 7, 59),
	(60, 'dropdown', 1, 1, 8, 60),
	(61, 'dropdown', 1, 1, 8, 61),
	(62, 'dropdown', 1, 1, 8, 62),
	(63, 'dropdown', 1, 1, 8, 63),
	(64, 'dropdown', 1, 1, 8, 64),
	(65, 'dropdown', 1, 1, 8, 65),
	(66, 'dropdown', 1, 1, 8, 66),
	(67, 'dropdown', 1, 1, 8, 67),
	(68, 'dropdown', 1, 1, 9, 68),
	(69, 'dropdown', 1, 1, 9, 69),
	(70, 'dropdown', 1, 1, 9, 70),
	(71, 'dropdown', 1, 1, 9, 71),
	(72, 'dropdown', 1, 1, 9, 72),
	(73, 'dropdown', 1, 1, 9, 73),
	(74, 'dropdown', 1, 1, 9, 74),
	(75, 'dropdown', 1, 1, 9, 75),
	(76, 'dropdown', 1, 1, 1, 76),
	(77, 'dropdown', 1, 1, 1, 77),
	(78, 'dropdown', 1, 1, 1, 78),
	(79, 'dropdown', 1, 1, 1, 79),
	(80, 'dropdown', 1, 1, 2, 80),
	(81, 'dropdown', 1, 1, 2, 81),
	(82, 'dropdown', 1, 1, 2, 82),
	(83, 'dropdown', 1, 1, 2, 83),
	(84, 'dropdown', 1, 1, 3, 84),
	(85, 'dropdown', 1, 1, 3, 85),
	(86, 'dropdown', 1, 1, 3, 86),
	(87, 'dropdown', 1, 1, 3, 87),
	(88, 'dropdown', 1, 1, 3, 88),
	(89, 'dropdown', 1, 1, 4, 89),
	(90, 'dropdown', 1, 1, 4, 90),
	(91, 'dropdown', 1, 1, 4, 91),
	(92, 'dropdown', 1, 1, 4, 92),
	(93, 'dropdown', 1, 1, 4, 93),
	(94, 'dropdown', 1, 1, 4, 94),
	(95, 'dropdown', 1, 1, 4, 95),
	(96, 'dropdown', 1, 1, 4, 96),
	(97, 'dropdown', 1, 1, 5, 97),
	(98, 'dropdown', 1, 1, 5, 98),
	(99, 'dropdown', 1, 1, 5, 99),
	(100, 'dropdown', 1, 1, 5, 100),
	(101, 'dropdown', 1, 1, 5, 101),
	(102, 'dropdown', 1, 1, 6, 102),
	(103, 'dropdown', 1, 1, 6, 103),
	(104, 'dropdown', 1, 1, 6, 104),
	(105, 'dropdown', 1, 1, 6, 105),
	(106, 'dropdown', 1, 1, 6, 106),
	(107, 'dropdown', 1, 1, 6, 107),
	(108, 'dropdown', 1, 1, 7, 108),
	(109, 'dropdown', 1, 1, 7, 109),
	(110, 'dropdown', 1, 1, 7, 110),
	(111, 'dropdown', 1, 1, 7, 111),
	(112, 'dropdown', 1, 1, 8, 112),
	(113, 'dropdown', 1, 1, 8, 113),
	(114, 'dropdown', 1, 1, 8, 114),
	(115, 'dropdown', 1, 1, 8, 115),
	(116, 'dropdown', 1, 1, 9, 116),
	(117, 'dropdown', 1, 1, 9, 117),
	(118, 'dropdown', 1, 1, 9, 118),
	(119, 'dropdown', 1, 1, 9, 119),
	(120, 'dropdown', 1, 1, 9, 120),
	(121, 'dropdown', 1, 1, 9, 121);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.question_description wordt geschreven
DROP TABLE IF EXISTS `question_description`;
CREATE TABLE IF NOT EXISTS `question_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_id` int(11) NOT NULL,
  `question_intro_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.question_description: ~121 rows (ongeveer)
DELETE FROM `question_description`;
/*!40000 ALTER TABLE `question_description` DISABLE KEYS */;
INSERT INTO `question_description` (`id`, `original_id`, `question_intro_id`, `description`, `version`) VALUES
	(1, 1, 1, 'Dat er niet wordt gediscrimineert', 1),
	(2, 2, 1, 'Dat de organisatie open en eerlijk (transparant en integer) is', 1),
	(3, 3, 1, 'Dat de organisatie rekening houdt met alle partijen: degene met wie zaken wordt gedaan of die belangrijk zijn voor de organisatie (dus ook de medewerkers) en met de maatschappij en het milieu', 1),
	(4, 4, 1, 'Dat de organisatie wet- en regelgeving nakomt en ook de eigen afspraken', 1),
	(5, 5, 1, 'Dat de organisatie verantwoordelijk is voor de gevolgen van het doen en laten en hierop kan worden aangesproken', 1),
	(6, 6, 1, 'Dat de organisatie regelmatig toetst of iedereen zich houdt aan deze regels', 1),
	(7, 7, 1, 'Dat duidelijk is wat de gevolgen zijn wanneer men zich niet aan de regels houdt', 1),
	(8, 8, 2, 'Weet aan welke wet- en regelgeving zij moet voldoen en heeft dit vastgelegd', 1),
	(9, 9, 2, 'Heeft een systeem waardoor zij op de hoogte blijft van de wet- en regelgeving', 1),
	(10, 10, 2, 'Toetst (zelf of door derden) met regelmaat of zij nog steeds voldoet aan de wet- en regelgeving', 1),
	(11, 11, 2, 'Is lid van een brancheorganisatie', 1),
	(12, 12, 2, 'Werkt met een ISO of HKZ of INK managementmodel of een soortgelijk model', 1),
	(13, 13, 2, 'Houdt waar mogelijk rekening met mensenrechten', 1),
	(14, 14, 2, 'Werkt volgens wet- en regelgeving (voor zover ik kan beoordelen)', 1),
	(15, 15, 3, 'Heeft een missie en een visie vastgelegd waarin staat wat zij onder Maatschappelijk Verantwoord Ondernemen/ Duurzaamheid verstaat', 1),
	(16, 16, 3, 'Heeft voor het komend jaar een plan waarin MVO doelstellingen zijn opgenomen', 1),
	(17, 17, 3, 'Heeft in kaart welke belanghebbenden / stakeholders een negatieve of positieve invloed hebben op de bedrijfsvoering', 1),
	(18, 18, 3, 'Heeft de risico\'s voor het bedrijf in kaart gebracht en de maatregelen om risico?s te verminderen / uit te schakelen vastgelegd', 1),
	(19, 19, 3, 'Heeft de taken, bevoegdheden en verantwoordelijkheden vastgelegd, bijvoorbeeld in functieomschrijvingen', 1),
	(20, 20, 3, 'Heeft rondom het financiële gedeelte een planning- en control cyclus', 1),
	(21, 21, 3, 'Volgt nieuwe (MVO) ontwikkelingen of initieert zelf nieuwe ontwikkelingen en past deze toe waar mogelijk', 1),
	(22, 22, 3, 'Neemt de wensen van de belanghebbenden / stakeholders mee in de besluitvorming', 1),
	(23, 23, 3, 'Toetst periodiek via een MVO Zelfevaluatie of MVO een integraal onderdeel vormt van de bedrijfsuitvoering en of doelstellingen zijn behaald', 1),
	(24, 24, 3, 'Geeft op directie- en managementniveau zelf het goede voorbeeld op het gebied van MVO', 1),
	(25, 25, 4, 'Heeft een visie op het gebied van personeelsbeleid', 1),
	(26, 26, 4, 'Vraagt naar houding t.a.v. MVO aan sollicitanten', 1),
	(27, 27, 4, 'Heeft voor elke medewerker een (door beide partijen) getekende arbeidsovereenkomst', 1),
	(28, 28, 4, 'Heeft het thema MVO opgenomen in het inwerkprogramma', 1),
	(29, 29, 4, 'Biedt opleidingen en trainingen, die aansluiten bij de functie', 1),
	(30, 30, 4, 'Heeft een officiële overleg- of vergaderstructuur', 1),
	(31, 31, 4, 'Heeft Arbo-beleid vastgelegd (kan onderdeel zijn van het personeelsbeleid)', 1),
	(32, 32, 4, 'Houdt tenminste eenmaal per jaar functioneringsgesprekken', 1),
	(33, 33, 4, 'Heeft een OR of heeft op een andere, formele, manier de inspraak geregeld voor het personeel', 1),
	(34, 34, 4, 'Heeft een klachtenregeling voor medewerkers', 1),
	(35, 35, 4, 'Zorgt goed voor haar personeel (incl. stagiaires, vrijwilligers en derden) voor zover ik kan beoordelen', 1),
	(36, 36, 5, 'Heeft beschrijvingen of informatie, waar mogelijk inclusief prijzen, van diensten en producten, zodat een klant weet wat de organisatie te bieden heeft tegen welke prijs en voorwaarden', 1),
	(37, 37, 5, 'Gebruikt algemene voorwaarden', 1),
	(38, 38, 5, 'Heeft de afspraken met klanten vastgelegd (bij een contract is dit door beide partijen getekend)', 1),
	(39, 39, 5, 'Heeft veranderingen in de afspraken met de klant vastgelegd en gecommuniceerd, zodat medewerkers hiervan op de hoogte zijn', 1),
	(40, 40, 5, 'Levert alleen producten/diensten die veilig en niet-schadelijk zijn voor de gezondheid van de klant', 1),
	(41, 41, 5, 'Levert een instructie of een gebruiksaanwijzing voor het gebruik van het product/de dienst aan de klant (indien van toepassing)', 1),
	(42, 42, 5, 'Gebruikt (hulp)middelen die van een (ISO en NEN) certificaat en/ of van een (CE) keurmerk zijn voorzien (indien van toepassing)', 1),
	(43, 43, 5, 'Heeft vastgelegd op welke manier ze service en / of garantie verleent', 1),
	(44, 44, 5, 'Heeft een klachtenprocedure voor klanten', 1),
	(45, 45, 5, 'Heeft op een bepaalde manier geregeld dat klanten inspraak hebben', 1),
	(46, 46, 5, 'Gaat op een goede manier met klanten om, voor zover ik kan beoordelen', 1),
	(47, 47, 6, 'Geeft aan op welke manier zij een positieve bijdrage levert aan de maatschappij en economische ontwikkeling van de omgeving', 1),
	(48, 48, 6, 'Heeft relaties met de sociale omgeving/ lokale bedrijven/ onderwijs/ belangengroepen om de omgeving verder te ontwikkelen', 1),
	(49, 49, 6, 'Houdt bij investeringen rekening met de lokale economie en de omgeving', 1),
	(50, 50, 6, 'Neemt bij voorkeur personeel uit de omgeving aan', 1),
	(51, 51, 6, 'Biedt stageplaatsen aan', 1),
	(52, 52, 6, 'Gaat op een goede manier om met de omgeving, voor zover ik kan beoordelen', 1),
	(53, 53, 7, 'Heeft een beleid dat gericht is op een win-win situatie met ketenpartners', 1),
	(54, 54, 7, 'Gebruikt haar invloed ten goede en zal anderen geen tekort doen', 1),
	(55, 55, 7, 'Gaat eerlijke competitie aan', 1),
	(56, 56, 7, 'Bespreekt het thema duurzaamheid met degene die belangrijk zijn voor de organisatie zoals toeleveranciers en klanten', 1),
	(57, 57, 7, 'Geeft zelf het goede voorbeeld in de keten m.b.t. MVO', 1),
	(58, 58, 7, 'Respecteert de eigendomsrechten van anderen (zoals copyright, trademark etc.)', 1),
	(59, 59, 7, 'Gaat als schakel in de keten op een goede manier om met andere organisaties, voor zover ik kan beoordelen', 1),
	(60, 60, 8, 'Heeft milieuafspraken op papier vastgelegd (milieubeleid)', 1),
	(61, 61, 8, 'Koopt zoveel mogelijk duurzame en milieuvriendelijke producten en diensten in', 1),
	(62, 62, 8, 'Gaat zo zuinig mogelijk met energie, water en materialen om ', 1),
	(63, 63, 8, 'Neemt maatregelen om schade aan het milieu te beperken', 1),
	(64, 64, 8, 'Heeft gescheiden afvalstromen', 1),
	(65, 65, 8, 'Maakt mij en anderen bewust van het milieu', 1),
	(66, 66, 8, 'Geeft zelf het goed voorbeeld op het gebied van milieu', 1),
	(67, 67, 8, 'Gaat op een goede manier om met het milieu, voor zover ik kan beoordelen', 1),
	(68, 68, 9, 'Heeft een schematische voorstelling van de organisatie in functies en /of afdelingen', 1),
	(69, 69, 9, 'Heeft een vergaderschema/ overlegstructuur die past bij de organisatie', 1),
	(70, 70, 9, 'Informeert mij op de juiste manier en op het juiste moment', 1),
	(71, 71, 9, 'Informeert anderen (klanten en externe partijen) op de juiste manier en op het juiste moment', 1),
	(72, 72, 9, 'Heeft een (communicatie)plan waarin stap voor stap is beschreven wat er tegen wie, hoe wordt gecommuniceerd', 1),
	(73, 73, 9, 'Heeft een communicatieplan dat onderdeel is van het (kwaliteits)systeem', 1),
	(74, 74, 9, 'Heeft inzichtelijk gemaakt welke functies er zijn binnen de organisatie en welke producten/ diensten verkocht worden', 1),
	(75, 75, 9, 'Geeft op tijd informatie door', 1),
	(76, 0, 1, 'Dat er niet wordt gediscrimineerd', 2),
	(77, 0, 1, 'Dat de organisatie open en eerlijk (transparant en integer) is', 2),
	(78, 0, 1, 'Dat de organisatie rekening houdt met alle partijen: degene met wie zaken wordt gedaan of die belangrijk zijn voor de organisatie (dus ook de medewerkers) en met de maatschappij en het milieu', 2),
	(79, 0, 1, 'Dat de organisatie verantwoordelijk is voor de gevolgen van het doen en laten en hierop kan worden aangesproken', 2),
	(80, 0, 2, 'Weet aan welke wet- en regelgeving zij moet voldoen en heeft dit vastgelegd', 2),
	(81, 0, 2, 'Heeft een systeem waardoor zij op de hoogte blijft van de wet- en regelgeving', 2),
	(82, 0, 2, 'Werkt met een ISO of HKZ of INK managementmodel of een soortgelijk model', 2),
	(83, 0, 2, 'Werkt volgens wet- en regelgeving (voor zover ik kan beoordelen)', 2),
	(84, 0, 3, 'Heeft een missie en een visie vastgelegd waarin staat wat zij onder Maatschappelijk Verantwoord Ondernemen/ Duurzaamheid verstaat', 2),
	(85, 0, 3, 'Heeft in kaart welke belanghebbenden / stakeholders een negatieve of positieve invloed hebben op de bedrijfsvoering', 2),
	(86, 0, 3, 'Heeft de risico’s voor het bedrijf in kaart gebracht en de maatregelen om risico’s te verminderen / uit te schakelen vastgelegd', 2),
	(87, 0, 3, 'Heeft de taken, bevoegdheden en verantwoordelijkheden vastgelegd, bijvoorbeeld in functieomschrijvingen', 2),
	(88, 0, 3, 'Geeft op directie- en managementniveau zelf het goede voorbeeld op het gebied van MVO', 2),
	(89, 0, 4, 'Heeft een visie op het gebied van personeelsbeleid', 2),
	(90, 0, 4, 'Heeft voor elke medewerker een (door beide partijen) getekende arbeidsovereenkomst', 2),
	(91, 0, 4, 'Heeft het thema MVO opgenomen in het inwerkprogramma', 2),
	(92, 0, 4, 'Biedt opleidingen en trainingen, die aansluiten bij de functie', 2),
	(93, 0, 4, 'Heeft een officiële overleg- of vergaderstructuur', 2),
	(94, 0, 4, 'Houdt tenminste eenmaal per jaar functioneringsgesprekken', 2),
	(95, 0, 4, 'Heeft een klachtenregeling voor medewerkers', 2),
	(96, 0, 4, 'Zorgt goed voor haar personeel (incl. stagiaires, vrijwilligers en derden) voor zover ik kan beoordelen', 2),
	(97, 0, 5, 'Heeft beschrijvingen of informatie, waar mogelijk inclusief prijzen, van diensten en producten, zodat een klant weet wat de organisatie te bieden heeft tegen welke prijs en voorwaarden', 2),
	(98, 0, 5, 'Heeft de afspraken met klanten vastgelegd (bij een contract is dit door beide partijen getekend)', 2),
	(99, 0, 5, 'Heeft veranderingen in de afspraken met de klant vastgelegd en gecommuniceerd, zodat medewerkers hiervan op de hoogte zijn', 2),
	(100, 0, 5, 'Heeft vastgelegd op welke manier ze service en / of garantie verleent', 2),
	(101, 0, 5, 'Heeft een klachtenprocedure voor klanten', 2),
	(102, 0, 6, 'Geeft aan op welke manier zij een positieve bijdrage levert aan de maatschappij en economische ontwikkeling van de omgeving', 2),
	(103, 0, 6, 'Heeft relaties met de sociale omgeving/ lokale bedrijven/ onderwijs/ belangengroepen om de omgeving verder te ontwikkelen', 2),
	(104, 0, 6, 'Houdt bij investeringen rekening met de lokale economie en de omgeving', 2),
	(105, 0, 6, 'Neemt bij voorkeur personeel uit de omgeving aan', 2),
	(106, 0, 6, 'Biedt stageplaatsen aan', 2),
	(107, 0, 6, 'Gaat op een goede manier om met de omgeving, voor zover ik kan beoordelen', 2),
	(108, 0, 7, 'Gebruikt haar invloed ten goede en zal anderen geen tekort doen', 2),
	(109, 0, 7, 'Gaat eerlijke competitie aan', 2),
	(110, 0, 7, 'Bespreekt het thema duurzaamheid met degene die belangrijk zijn voor de organisatie zoals toeleveranciers en klanten', 2),
	(111, 0, 7, 'Geeft zelf het goede voorbeeld in de keten m.b.t. MVO', 2),
	(112, 0, 8, 'Heeft milieuafspraken op papier vastgelegd (milieubeleid)', 2),
	(113, 0, 8, 'Koopt zoveel mogelijk duurzame en milieuvriendelijke producten en diensten in', 2),
	(114, 0, 8, 'Gaat zo zuinig mogelijk met energie, water en materialen om ', 2),
	(115, 0, 8, 'Maakt mij en anderen bewust van het milieu', 2),
	(116, 0, 9, 'Heeft een schematische voorstelling van de organisatie in functies en /of afdelingen', 2),
	(117, 0, 9, 'Heeft een vergaderschema/ overlegstructuur die past bij de organisatie', 2),
	(118, 0, 9, 'Informeert mij op de juiste manier en op het juiste moment', 2),
	(119, 0, 9, 'Informeert anderen (klanten en externe partijen) op de juiste manier en op het juiste moment', 2),
	(120, 0, 9, 'Heeft een (communicatie)plan waarin stap voor stap is beschreven wat er tegen wie, hoe wordt gecommuniceerd', 2),
	(121, 0, 9, 'Geeft op tijd informatie door', 2);
/*!40000 ALTER TABLE `question_description` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.question_intro wordt geschreven
DROP TABLE IF EXISTS `question_intro`;
CREATE TABLE IF NOT EXISTS `question_intro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submodule_id` int(11) NOT NULL,
  `excerpt` text NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.question_intro: ~8 rows (ongeveer)
DELETE FROM `question_intro`;
/*!40000 ALTER TABLE `question_intro` DISABLE KEYS */;
INSERT INTO `question_intro` (`id`, `submodule_id`, `excerpt`, `sequence`) VALUES
	(1, 1, 'De organisatie heeft een gedragscode waarin onder andere  is opgenomen:', 1),
	(2, 2, 'De organisatie:', 2),
	(3, 3, 'De organisatie:', 3),
	(4, 4, 'De organisatie:', 4),
	(5, 5, 'De organisatie:', 5),
	(6, 6, 'De organisatie:', 6),
	(7, 7, 'De organisatie:', 7),
	(8, 8, 'De organisatie:', 8),
	(9, 9, 'De organisatie:', 9);
/*!40000 ALTER TABLE `question_intro` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.selected_goals wordt geschreven
DROP TABLE IF EXISTS `selected_goals`;
CREATE TABLE IF NOT EXISTS `selected_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `goal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.selected_goals: ~28 rows (ongeveer)
DELETE FROM `selected_goals`;
/*!40000 ALTER TABLE `selected_goals` DISABLE KEYS */;
INSERT INTO `selected_goals` (`id`, `user_id`, `survey_id`, `goal_id`) VALUES
	(1, 1, 3, 4),
	(2, 1, 3, 6),
	(3, 1, 3, 11),
	(4, 1, 3, 14),
	(5, 1, 3, 15),
	(6, 1, NULL, 4),
	(7, 1, NULL, 6),
	(8, 1, NULL, 11),
	(9, 1, NULL, 14),
	(10, 1, NULL, 15),
	(11, 1, NULL, 4),
	(12, 1, NULL, 6),
	(13, 1, NULL, 11),
	(14, 1, NULL, 14),
	(15, 1, NULL, 3),
	(16, 1, NULL, 6),
	(17, 1, NULL, 11),
	(18, 1, NULL, 14),
	(19, 1, NULL, 15),
	(20, 1, NULL, 3),
	(21, 1, NULL, 6),
	(22, 1, NULL, 11),
	(23, 1, NULL, 14),
	(24, 1, NULL, 15),
	(25, 1, 4, 2),
	(26, 1, 4, 7),
	(27, 1, 4, 10),
	(28, 1, 4, 15),
	(29, 1, 4, 16);
/*!40000 ALTER TABLE `selected_goals` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.selected_goals_reviewed wordt geschreven
DROP TABLE IF EXISTS `selected_goals_reviewed`;
CREATE TABLE IF NOT EXISTS `selected_goals_reviewed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `review_goal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumpen data van tabel roaming_souls_c.selected_goals_reviewed: ~0 rows (ongeveer)
DELETE FROM `selected_goals_reviewed`;
/*!40000 ALTER TABLE `selected_goals_reviewed` DISABLE KEYS */;
/*!40000 ALTER TABLE `selected_goals_reviewed` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.submodule wordt geschreven
DROP TABLE IF EXISTS `submodule`;
CREATE TABLE IF NOT EXISTS `submodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `introduction` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.submodule: ~11 rows (ongeveer)
DELETE FROM `submodule`;
/*!40000 ALTER TABLE `submodule` DISABLE KEYS */;
INSERT INTO `submodule` (`id`, `module_id`, `name`, `introduction`) VALUES
	(1, 1, 'Gedragscode (interne eisen)', 'Met deze categorie geef je aan hoe jullie met elkaar omgaan binnen het bedrijf en hoe je aandacht hebt voor anderen. Vaak weet je wel hoe je onderling met elkaar om moet gaan; dat je niet mag pesten of discrimineren op de werkvloer.'),
	(2, 1, 'Wet- en regelgeving (externe eisen)', 'Bij deze categorie geef je aan hoe allerlei wet en regelgeving wordt toegepast binnen jullie bedrijf. Ieder bedrijf heeft te maken met wetten en regels. Veel regels ken je al en pas je vaak al automatisch toe in het dagelijkse werk. Denk bijvoorbeeld maar aan veiligheidsvoorschriften. De overheid veranderd regelmatig de regels.'),
	(3, 1, 'Goed bestuur', 'Ieder bedrijf wordt bestuurd door een of enkele personen. Het is belangrijk dat je medewerkers meeneemt in de beslissingen die je (wilt) nemen.'),
	(4, 1, 'Personeelsbeleid', 'Dit gaat over een goede relatie tussen werknemer en werkgever, van beide kanten. Denk aan; de cultuur binnen het bedrijf, een getekende arbeidsovereenkomst of de overlegstructuur. Medewerkers ervaren arbeidsomstandigheden vaak om een andere manier, waarop de leiding dat doet.'),
	(5, 1, 'Klantenbeleid', 'De klanten zijn erg belangrijk voor jullie bedrijf. Zonder klanten is er geen werk, geen brood op de plank voor iedereen die werkt. Bij klantenbeleid gaat het over om de klanten op lange termijn te binden aan je bedrijf. Over het eerlijk en volledig informeren van de klant, bijvoorbeeld het oplossen van klachten.'),
	(6, 1, 'Omgevingsbeleid', 'Een bedrijf heeft altijd te maken met zijn omgeving en het netwerk waar zij zich in bevindt. Hoe ervaart de buurt / dorp of stad jullie bedrijf? Profiteert de lokale gemeenschap mee van de activiteiten van jouw bedrijf? Kan een lokale vereniging bijvoorbeeld materialen lenen voor hun activiteiten?'),
	(7, 1, 'Ketenbeleid', 'Ieder bedrijf maakt onderdeel uit van een keten. Er wordt iets ingekocht bij leverancier en er wordt iets verkocht aan klanten. Hierdoor maak je deel uit van een keten.'),
	(8, 1, 'Milieubeleid', 'Bij MVO wordt vaak meteen aan milieu gedacht. Zoals je gemerkt hebt is MVO veel meer dan milieu alleen. Bij milieu gaat het er om, of jouw bedrijf actief bezig is om milieuvervuiling tegen te gaan. Dat kan al heel klein zijn door bijvoorbeeld afval te scheiden'),
	(9, 1, 'Communicatie', 'Communicatie is erg belangrijk voor werkplezier en een goed draaiend en groeiend bedrijf. Communicatie is heel makkelijk, maar ook gelijk het aller moeilijkst. Hoe is de communicatie in jouw bedrijf? Is er regelmatig overleg tussen werknemers en de werkgever? Word je goed geïnformeerd binnen het bedrijf? Kun je makkelijk met je vragen of suggesties voor verbetering terecht bij de leiding? Worden klanten en leveranciers op de hoogte gehouden?'),
	(10, 2, 'Vervoer', NULL),
	(12, 2, 'Gebouw', NULL);
/*!40000 ALTER TABLE `submodule` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.survey_answers wordt geschreven
DROP TABLE IF EXISTS `survey_answers`;
CREATE TABLE IF NOT EXISTS `survey_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_description_id` int(11) DEFAULT NULL,
  `answer_set_id` int(11) DEFAULT NULL,
  `review_answer_set_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `answer_date` int(11) DEFAULT NULL,
  `review_answer_date` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.survey_answers: ~92 rows (ongeveer)
DELETE FROM `survey_answers`;
/*!40000 ALTER TABLE `survey_answers` DISABLE KEYS */;
INSERT INTO `survey_answers` (`id`, `question_description_id`, `answer_set_id`, `review_answer_set_id`, `user_id`, `reviewer_id`, `answer_date`, `review_answer_date`, `survey_id`) VALUES
	(1, 76, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(2, 77, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(3, 78, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(4, 79, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(5, 80, 1, NULL, 1, NULL, 1620384050, NULL, 2),
	(6, 81, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(7, 82, 1, NULL, 1, NULL, 1620384050, NULL, 2),
	(8, 83, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(9, 84, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(10, 85, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(11, 86, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(12, 87, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(13, 88, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(14, 89, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(15, 90, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(16, 91, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(17, 92, 1, NULL, 1, NULL, 1620384050, NULL, 2),
	(18, 93, 5, NULL, 1, NULL, 1620384050, NULL, 2),
	(19, 94, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(20, 95, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(21, 96, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(22, 97, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(23, 98, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(24, 99, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(25, 100, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(26, 101, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(27, 102, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(28, 103, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(29, 104, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(30, 105, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(31, 106, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(32, 107, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(33, 108, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(34, 109, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(35, 110, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(36, 111, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(37, 112, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(38, 113, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(39, 114, 2, NULL, 1, NULL, 1620384050, NULL, 2),
	(40, 115, 1, NULL, 1, NULL, 1620384050, NULL, 2),
	(41, 116, 1, NULL, 1, NULL, 1620384050, NULL, 2),
	(42, 117, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(43, 118, 4, NULL, 1, NULL, 1620384050, NULL, 2),
	(44, 119, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(45, 120, 3, NULL, 1, NULL, 1620384050, NULL, 2),
	(46, 121, 5, NULL, 1, NULL, 1620384050, NULL, 2),
	(47, 76, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(48, 77, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(49, 78, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(50, 79, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(51, 80, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(52, 81, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(53, 82, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(54, 83, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(55, 84, 4, NULL, 1, NULL, 1625593649, NULL, 6),
	(56, 85, 4, NULL, 1, NULL, 1625593649, NULL, 6),
	(57, 86, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(58, 87, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(59, 88, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(60, 89, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(61, 90, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(62, 91, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(63, 92, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(64, 93, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(65, 94, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(66, 95, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(67, 96, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(68, 97, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(69, 98, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(70, 99, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(71, 100, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(72, 101, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(73, 102, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(74, 103, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(75, 104, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(76, 105, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(77, 106, 4, NULL, 1, NULL, 1625593649, NULL, 6),
	(78, 107, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(79, 108, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(80, 109, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(81, 110, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(82, 111, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(83, 112, 4, NULL, 1, NULL, 1625593649, NULL, 6),
	(84, 113, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(85, 114, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(86, 115, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(87, 116, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(88, 117, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(89, 118, 1, NULL, 1, NULL, 1625593649, NULL, 6),
	(90, 119, 3, NULL, 1, NULL, 1625593649, NULL, 6),
	(91, 120, 2, NULL, 1, NULL, 1625593649, NULL, 6),
	(92, 121, 2, NULL, 1, NULL, 1625593649, NULL, 6);
/*!40000 ALTER TABLE `survey_answers` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.survey_attendees wordt geschreven
DROP TABLE IF EXISTS `survey_attendees`;
CREATE TABLE IF NOT EXISTS `survey_attendees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` int(11) NOT NULL,
  `completion_date` int(11) NOT NULL,
  `completed` int(11) NOT NULL DEFAULT '0',
  `subject_year` int(4) DEFAULT NULL,
  `is_reviewed` int(11) NOT NULL DEFAULT '0',
  `reviewer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel roaming_souls_c.survey_attendees: ~7 rows (ongeveer)
DELETE FROM `survey_attendees`;
/*!40000 ALTER TABLE `survey_attendees` DISABLE KEYS */;
INSERT INTO `survey_attendees` (`id`, `module_id`, `user_id`, `creation_date`, `completion_date`, `completed`, `subject_year`, `is_reviewed`, `reviewer`) VALUES
	(1, 2, 1, 1620330064, 1620330088, 1, 2021, 0, NULL),
	(2, 1, 1, 1620383975, 1620384050, 1, NULL, 0, NULL),
	(3, 3, 1, 1620384841, 1620384850, 1, NULL, 0, NULL),
	(4, 3, 1, 1620384851, 1625512201, 1, NULL, 0, NULL),
	(5, 2, 1, 1621968710, 0, 0, 2020, 0, NULL),
	(6, 1, 1, 1623073673, 1625593649, 1, 2020, 0, NULL),
	(7, 3, 1, 1625513503, 0, 0, NULL, 0, NULL);
/*!40000 ALTER TABLE `survey_attendees` ENABLE KEYS */;

-- Structuur van  tabel roaming_souls_c.users wordt geschreven
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `insertion` varchar(255) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `permission_set_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `is_deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=hp8;

-- Dumpen data van tabel roaming_souls_c.users: ~1 rows (ongeveer)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `parent_id`, `firstname`, `lastname`, `insertion`, `phone`, `country`, `city`, `zipcode`, `street`, `number`, `email`, `password`, `status`, `permission_set_id`, `token`, `is_deleted`) VALUES
	(1, 0, 'Jeroen', 'Bellemans', '', NULL, 'België', 'Zeveneken', '9080', 'Zeveneken-Dorp', '113', 'jeroenbellemans@hotmail.com', '$2y$10$NEyOqHFoo6UaWmWmoTUhbuM0UL53aErYK5YkZARS1VBdb44pOQNEm', 1, 1, 'c2a0a56e1a145ea539aa9d3482d0963972ee0614ef9923257e3be0d5fd6240aca0771264e1a71ba4adf98f24a009baf2ea2ad34745ee85364c8c7a6749de12ecdd3e6fd6e70b56585e34e4b86c1a', '0'),
	(15, 0, 'Kees', 'Oord', 'Van Den', NULL, NULL, NULL, NULL, NULL, NULL, 'keesvdo@vdo.com', NULL, 0, 3, '29fd85357410b29c8da6d040e97971f22b49d0e0c477477bc16be30ed1f7b6856f75720f7ec864164642746dccfa1949d71313515c4d2315d87de9a5980a6e3dd1db6fc618954ad11217153f9eb8', '1'),
	(16, 0, 'Eline', 'Allemeersch', '', NULL, 'België', 'Zeveneken', '9080', 'Zeveneken-Dorp', '113', 'eline_allemeersch@hotmail.com', NULL, 0, 3, '70383efa6e326971a0bcc3ee0ae61cebf6a180064d94b02747e1d50765492206ec70575ce41303c620bece1057246b8167d5d638b549f05b5fcf6d0460f6fb07ccd1fa7863e2e6d834e449501e70', '0'),
	(17, 0, 'Eline', 'Allemeersch', '', NULL, NULL, NULL, NULL, NULL, NULL, 'eline_allemeerscsh@hotmail.com', NULL, 0, 3, 'df70acdb82c4cab3756374ddab385ba72f4c36a5f40607e2d07e5af10f6b8bc6ae872d0f306944f2aadf0376b01d5421b5d4579aeb1156aed2a2434f24db9fe941499b576ad138b2962ab7d5c3e3', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
