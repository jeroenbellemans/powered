<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->sdgSurveys->completed) : ?>
                    <?php foreach ($data->sdgSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/sdg/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->sdgSurveys->incompleted) : ?>
                    <?php foreach ($data->sdgSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/sdg/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <?php // debug_print($data); ?>

        <div class="col-md-9">
            <h1>SDG scan</h1>
            <p>De Sustainable Development Goals (SDG's) zijn door de Verenigde Naties vastgestelde wereldwijde doelstellingen voor duurzame ontwikkeling. Er zijn 17 SDG's met 169 subdoelen. De doelen zijn breed geformuleerd om daarmee zowel richting te geven voor bedrijven als landen. De SDG's worden gezien als dé taal van de toekomst en steeds meer bedrijven communiceren erover in hun jaarverslag. Waarom? Omdat verwacht wordt dat de SDG's in de toekomst als maatstaf worden gebruikt door opdrachtgevers, zoals bijvoorbeeld overheidsinstanties, zorginstellingen en bedrijven.</p>
            <p>In een vervolgstap brengen we dit voor jou als ondernemer terug naar de meest relevante SDG's voor jouw bedrijf. Daarvoor selecteren we samen met jouw medewerkers 3 thema’s die het beste bij de impact en missie van jouw bedrijf aansluiten. Vervolgens maken we de SDG's concreet en worden er KPI's geformuleerd, waarover je kunt rapporteren.</p>
            <p>Welke SDG's zijn voor jouw gevoel het meest relevant voor jouw bedrijf? Selecteer die SDG's in de scan.</p>
            
            <?php if (!$data->sdgSurveys->completed && !$data->sdgSurveys->incompleted) : ?>
                <div class="mt-5">
                    <?php echo Routes::build('survey/sdg/start', 'Start de SDG scan <i class="fas fa-arrow-right ml-3"></i>', array("class" => "btn btn-primary btn-highlight")); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>