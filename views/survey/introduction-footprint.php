<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->coSurveys->completed) : ?>
                    <?php foreach ($data->coSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->coSurveys->incompleted) : ?>
                    <?php foreach ($data->coSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <?php // debug_print($data); ?>

        <?php if ($data->establishment) : ?>
            <div class="col-md-9">
                <h1>CO2 scan</h1>
                <p>Hoeveel broeikasgas stoot jouw bedrijf uit? CO2 ofwel koolstofdioxide is het belangrijkste broeikasgas. Aan de hand van onder andere uw brandstofgebruik en energienota wordt de CO2 footprint berekend.</p>
                <p>Broeikasgassen komen van nature in de atmosfeer voor, maar als gevolg van menselijke activiteiten, neemt de hoeveelheid extreem sterk toe en verandert het klimaat. De EU wil de uitstoot in 2030 met 55% terugbrengen ten opzichte van 1990.</p>
                <div class="mt-5">
                    <?php echo Routes::build('survey/footprint/start', 'Start de CO2 scan <i class="fas fa-arrow-right ml-3"></i>', array("class" => "btn btn-primary btn-highlight")); ?>
                </div>
            </div>
        <?php else : ?>
            <div class="col-md-9">
                <h1>Helaas</h1>
                <?php if ($data->guardian->passportCheck('api@create_api', $data->accountInfo->permission->id)): ?>
                    <p>U kan de CO2 scan nog niet starten, omdat u nog geen bedrijfsgegevens hebt opgegeven.</p>
                    <p>Om uw bedrijfsgegevens op te halen, kan u via het wijzigen van uw account connectie maken met de API van de Kamer van Koophandel.</p>
                <?php else : ?>
                    <p>U kan de CO2 scan nog niet starten, omdat uw administrator nog geen bedrijfsgegevens heeft opgegeven.</p>
                <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>