<?php
    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['sdg-selection'])) {
            $postData->sdg = $_POST['sdg'];
            $postData->moduleId = $data->instance->fetchModule('sdg')[0]->id;
            $postData->module = 'sdg';
            $postData->surveyId = $data->context;

            $admin = $data->instance->saveSdgSelection($postData, 'survey');

            debug_print($admin);

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->sdgSurveys->completed) : ?>
                    <?php foreach ($data->sdgSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/sdg/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->sdgSurveys->incompleted) : ?>
                    <?php foreach ($data->sdgSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/sdg/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <?php // debug_print($data); ?>

        <div class="col-md-9">
            <h1>SDG scan</h1>
            <p>Selecteer tot 5 doelen waarvan je denkt dat jouw bedrijf inspanningen en moeite doet om deze te behalen.</p>
            <h2>Duurzame Ontwikkelingsdoelen</h2>

            <div id="sdggoal" class="alert alert-warning hidden" role="alert">
                U heeft reeds het maximum van 5 duurzame ontwikkelingsdoelen geselecteerd.
            </div>

            <?php if ($data->sdgScan->goals) : ?>
                <div class="row">
                <?php $i = 1; ?>
                <?php foreach ($data->sdgScan->goals as $sdgGoal) : ?>
                    <div class="col-md-3 col-xs-2 mb-4">
                        <div class="card" onclick="toggleGoals(this)" data-id="<?php echo $sdgGoal->id; ?>">
                            <img src="<?php echo config_get_root(); ?><?php echo $sdgGoal->goal_location; ?><?php echo $sdgGoal->goal_version; ?>/sdg-goal-<?php echo $i++; ?>.png" class="card-img-top" alt="<?php echo $sdgGoal->goal; ?>">
                            <div class="selection">
                                <i class="fas fa-check"></i>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="row mb-5 mt-5">
                    <div class="col-md-12 text-right">
                        <form action="" method="post">
                            <input type="hidden" name="sdg" value="" />
                            <button type="submit" name="sdg-selection" class="btn btn-success btn-highlight-link">Selectie bevestigen</button>
                        </form>
                    </div>
                </div>
            <?php else : ?>
                <p>We kunnen momenteel geen Duurzame Ontwikkelingsdoelen weergeven.</p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>