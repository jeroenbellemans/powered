<?php
    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['sdg-selection'])) {
            $postData->sdg = $_POST['sdg'];
            $postData->moduleId = $data->moduleId;
            $postData->surveyId = $data->context;

            $admin = $data->instance->saveSdgSelection($postData);

        } else

        if (isset($_POST['add-establishment'])) {

            $postData->companyName = $_POST['companyname'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->postalCode = $_POST['zipcode'];
            $postData->country = $_POST['country'];
            $postData->fte = $_POST['fte-add'];
            $postData->sqft = $_POST['sqft-add'];
            $postData->mainBranch = '0';

            $company = $data->instance->createSubCompany($postData);

        } else
        
        if (isset($_POST['start-scan'])) {

            $postData->year = $_POST['year'];

            $survey = $data->instance->setYearForScan($postData);

        } else 

        if (isset($_POST['submit-answers'])) {
            $survey = $data->instance->saveFootprintAnswers($_POST);

            //debug_print($survey);
            //exit;
        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->coSurveys->completed) : ?>
                    <?php foreach ($data->coSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->coSurveys->incompleted) : ?>
                    <?php foreach ($data->coSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <?php if ($data->establishment) : ?>
            <div class="col-md-9">
                <h1><?php echo $data->survey->module[0]->name; ?><span class="float-right" style="margin-top: 5px;"><?php echo Routes::build('survey/footprint/' . $data->coSurveys->incompleted[0]->module_id, 'Terug naar algemene informatie', array("class" => "btn btn-primary btn-highlight-link small")); ?></span></h1>

                <?php
                    // Start incrementor
                    $i = 0;

                    echo '<form action="" method="post">';
                    echo '<input type="hidden" name="module" value="' . $data->survey->module[0]->id . '" />';
                    echo '<div class="tab-content" id="nav-tabContent">';

                    $totalModules = count($data->survey->submodules);

                    // Loop submodules, create tab per submodule
                    foreach ($data->survey->submodules as $submodule) {

                        $showAsActive = ($i == 0) ? 'show active' : '';

                        echo '<div class="tab-pane fade ' . $showAsActive . ' submodule" id="submodule-' . $i . '" role="tabpanel" aria-labelledby="nav-home-tab">';

                        if ($submodule->id == '12') {
                    
                            echo '<p>De gebouwen component brengt de CO2 uitstoot in beeld van energiegebruik van je eigen en gehuurde bedrijfspanden. Je vult dit per locatie in. Het energiegebruik (gas, elektra) leidt je makkelijk af van de nota(‘s) van uw energieleverancier. Uit nota(‘s) voor onderhoud aan je installaties voor het binnenklimaat, leidt je af hoeveel kg koudemiddelen er gebruikt zijn.</p>';
                            echo '<p>Extra info bij invullen energiegebruik (extra regel eigen laadpalen): in deze extra regel geef je (een inschatting van) de hoeveelheid elektriciteit (KWh) aan, waarmee je eigen elektrische auto’s via uw eigen laadpalen geladen zijn. De elektriciteit hoort niet bij het gebouw en wordt automatisch meegerekend bij de vervoercomponent.</p>';
                            echo '<p>Voor deze onderdelen zijn in het begin al het aantal fte en m2 per locatie ingegeven. We kunnen hier een extra vraag opvoeren, waarin het bedrijf zijn aantal elektrische auto’s in kan geven. Dat geeft over de jaren heen een mooi beeld (net als bij vdKaa).</p>';
                        
                        } else {
                            echo '<p>De vervoer component brengt de CO2 uitstoot in beeld van brandstofgebruik door eigen auto’s, vergoedingen privé auto’s werknemers & woon-werk verkeer en eventuele vliegreizen. Het brandstofgebruik leidt u makkelijk af van het overzicht van uw brandstofleverancier (tankpassen). Uit de vergoedingen aan werknemers leidt u het aantal kilometers af.</p>';
                            echo '<p>Extra info bij invullen km: U hoeft niet elke auto individueel uit te splitsen: u kunt volstaan met een gemiddelde auto per brandstoftype.</p>';
                            echo '<p>Extra info bij invullen brandstof (elektriciteit): hier vult u de kosten van (snel)laadstations in (Fastned, Tesla, Shell, et cetera). Het elektriciteitsgebruik van uw eigen laadpalen, wordt hier automatisch berekend.</p>';
                        }
                        
                        echo '<h2>' . $submodule->name . ' <span class="float-right">' . ($i + 1) . '/' . $totalModules   . '</span></h2>';

                        // Specific check for building
                        if (count($data->getAllEstablishments) > 1) {
                            if ($submodule->id == '12') {
                                echo '<div class="scan-highlight mb-4">';
                                echo '<h3>Vestiging</h3>';
                                echo '<div class="answer-area">';
                                echo '<p>Indien u verschillende vestigingen hebt, kan u de vragenlijst die slaat op het thema "Gebouw" invullen per vestiging. Selecteer de vestiging waarvoor u de vragenlijst wenst in te vullen.</p>';
                                
                                echo '<select class="form-control" onChange="showFormForEstablishment(this);">';
                                foreach ($data->getAllEstablishments as $establishment) {
                                    echo '<option value="' . $establishment->id . '">' . $establishment->company_name . ': ' . $establishment->establishment_street . ' ' . $establishment->establishment_number . ', ' . $establishment->establishment_city . '</option>';
                                }
                                echo '</select>';
                                
                                echo '</div>';
                                echo '</div>';
                            }
                        }
                        
                        if ($submodule->id == '12') {
                        
                            if (count($data->getAllEstablishments) > 0) {

                                $k = 0;

                                foreach ($data->getAllEstablishments as $establishment) {

                                    $activeTab = ($k == 0) ? '' : 'hidden';

                                    echo '<div class="sub-tab sub-tab-' . $establishment->id . ' ' . $activeTab . '">';

                                    foreach ($data->survey->categories[$submodule->id] as $category) {

                                        echo '<div class="scan-highlight mb-4">';
                                        echo '<h3>' . $category->name . '</h3>';
                                        echo $category->excerpt;

                                        echo '<div class="answer-area">';

                                        foreach ($data->survey->parentQuestions[$category->id] as $parentQuestion) {

                                            echo '<p>' . $parentQuestion->question . '</p>';
                                            
                                            $j = 0;

                                            if (isset($data->survey->answersPerQuestion[$parentQuestion->id])) {
                                                
                                                echo '<div class="row mb-2">';

                                                foreach ($data->survey->answersPerQuestion[$parentQuestion->id] as $answer) {
                                                    
                                                    echo '<div class="col-md-6">';
                                                        echo '<label data-id="' . $answer->id . '" data-establishment="' . $establishment->id . '" data-parent-id="' . $parentQuestion->id . '" data-answer="' . $answer->answer . '" data-has-child="' . $answer->has_child_question . '" data-child-id="' . $answer->child_question_id . '" data-unit="' . $answer->unit . '" data-answer-type="' . $answer->answer_type . '" onclick="loadQuestionOrAnswer(this, event, ' . $submodule->id . ');"><input type="checkbox" name="answer_' . $parentQuestion->id . '@' . $establishment->id . '+' . $submodule->id . '[]" value="' . $answer->id . '">&nbsp;' . $answer->answer . '</label>';
                                                    echo '</div>';
                                                    echo '<div class="col-md-6">';
                                                        echo '<div class="answer-loaded-' . $answer->id . '"></div>';
                                                    echo '</div>';
                                                    echo '<div class="col-md-12 ml-5">';
                                                        echo '<div class="question-loaded-' . $answer->id . '"></div>';
                                                    echo '</div>';

                                                    $j++;
                                                }

                                                echo '</div>';
                                            } else {
                                                echo '<p>Deze vraag heeft geen mogelijke antwoorden.</p>';
                                            }

                                        }

                                        echo '</div>';
                                        echo '</div>';

                                    }

                                    echo '</div>';

                                    $k++;
                                }
                            }

                        } else {
                            
                            foreach ($data->survey->categories[$submodule->id] as $category) {

                                echo '<div class="scan-highlight mb-4">';
                                echo '<h3>' . $category->name . '</h3>';
                                echo $category->excerpt;
                                echo '<div class="answer-area">';

                                foreach ($data->survey->parentQuestions[$category->id] as $parentQuestion) {

                                    echo '<p>' . $parentQuestion->question . '</p>';
                                    
                                    $j = 0;

                                    if (isset($data->survey->answersPerQuestion[$parentQuestion->id])) {
                                        
                                        echo '<div class="row mb-2">';

                                        foreach ($data->survey->answersPerQuestion[$parentQuestion->id] as $answer) {
                                            
                                            echo '<div class="col-md-6">';
                                                echo '<label data-id="' . $answer->id . '" data-parent-id="' . $parentQuestion->id . '" data-answer="' . $answer->answer . '" data-has-child="' . $answer->has_child_question . '" data-child-id="' . $answer->child_question_id . '" data-unit="' . $answer->unit . '" data-answer-type="' . $answer->answer_type . '" onclick="loadQuestionOrAnswer(this, event, ' . $submodule->id . ');"><input type="checkbox" name="answer_' . $parentQuestion->id . '+' . $submodule->id . '[]" value="' . $answer->id . '">&nbsp;' . $answer->answer . '</label>';
                                            echo '</div>';
                                            echo '<div class="col-md-6">';
                                                echo '<div class="answer-loaded-' . $answer->id . '"></div>';
                                            echo '</div>';
                                            echo '<div class="col-md-12 ml-5">';
                                                echo '<div class="question-loaded-' . $answer->id . '"></div>';
                                            echo '</div>';

                                            $j++;
                                        }

                                        echo '</div>';
                                    } else {
                                        echo '<p>Deze vraag heeft geen mogelijke antwoorden.</p>';
                                    }

                                }

                            echo '</div>';
                            echo '</div>';

                            }
                        }

                        // Navigation ----->
                        echo '<div class="row mb-5">';
                        // Previous button => browse submodule
                        echo '<div class="col-md-6 text-left">';
                        if ($i != 0) {
                            echo '<a class="btn btn-primary btn-highlight-outline-link" onclick="previousModule(this, false);" data-index="' . $i . '"><i class="fas fa-arrow-left mr-3"></i> Vorige module</a>';
                        };
                        echo '</div>';
                        // Next button => browse submodule
                        if (($i + 1) == $totalModules) {
                            echo '<div class="col-md-6 text-right"><button name="submit-answers" class="btn btn-success btn-highlight-link" data-index="' . $i . '">Antwoorden indienen</button></div>';
                        } else {
                            echo '<div class="col-md-6 text-right"><a class="btn btn-primary btn-highlight-outline-link" onclick="nextModule(this, false);" data-index="' . $i . '">Volgende module  <i class="fas fa-arrow-right ml-3"></i></a></div>';
                        }
                        echo '</div>';
                        echo '</div>';
                        // <----- End Navigation

                        $i++;

                    }

                    echo '</div>';
                ?>
            </div>
        <?php else : ?>
            <div class="col-md-9">
                <h1>Helaas</h1>
                <p>Aangezien er nog geen bedrijfsgegevens zijn geregistreerd voor uw account, kan u de CO2 scan niet starten of afwerken.</p>
                <p>Gelieve contact op te nemen met een administrator zodat zij uw bedrijfsgegevens kunnen registreren.</p>
        <?php endif; ?>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>