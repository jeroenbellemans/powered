<?php
    if (form_posted()) {
        
        $postData = new \StdClass();

        if (isset($_POST['module'])) {
            $data->instance->createAdminAnswersForQuestions($_POST);
        }

    }
?>

<?php include 'views/partials/headers/header.php'; ?>

<?php // debug_print($data->test); ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>  
        </div>

        <div class="col-md-9">
            <?php // debug_print($data->survey); ?>
            <h1><?php echo $data->survey->module[0]->name; ?></h1>
            <?php
                // Start incrementor
                $i = 0;

                echo '<form action="" method="post">';
                echo '<input type="hidden" name="module" value="' . $data->survey->module[0]->id . '" />';
                echo '<div class="tab-content" id="nav-tabContent">';

                $totalModules = count($data->survey->submodules);

                // Loop submodules, create tab per submodule
                foreach ($data->survey->submodules as $submodule) {

                    $showAsActive = ($i == 0) ? 'show active' : '';

                    echo '<div class="tab-pane fade ' . $showAsActive . ' submodule" id="submodule-' . $i . '" role="tabpanel" aria-labelledby="nav-home-tab">';
                    echo '<h2>' . $submodule->name . ' <span class="float-right">' . ($i + 1) . '/' . $totalModules   . '</span></h2>';

                    echo '<div class="alert alert-warning hidden" role="alert">';
                    echo 'Gelieve alle vragen van een antwoord te voorzien.';
                    echo '</div>';

                    // Implementation of open questions should come here

                    if (!empty($submodule->introduction)) {
                        echo '<div class="introduction">';
                            echo '<div class="answer-area">';
                                echo '<p style="margin-bottom: 0;">' . $submodule->introduction . '</p>';
                                echo '<br />';
                                echo '<p>' . $submodule->open_question[0]->question_description . '</p>';
                                echo '<textarea class="form-control" rows="5" name="open-question_' . $submodule->open_question[0]->id . '">';
                                if ($data->survey->openAnswers[$submodule->open_question[0]->id] != $data->survey->reviewedOpenAnswers[$submodule->open_question[0]->id]) {
                                    echo $data->survey->reviewedOpenAnswers[$submodule->open_question[0]->id];
                                } else {
                                    echo $data->survey->openAnswers[$submodule->open_question[0]->id];
                                }
                                echo '</textarea>';
                                if ($data->survey->openAnswers[$submodule->open_question[0]->id] != $data->survey->reviewedOpenAnswers[$submodule->open_question[0]->id]) {
                                    $original_answer = ($data->survey->openAnswers[$submodule->open_question[0]->id] == '') ? '[Niet ingevuld]' : $data->survey->openAnswers[$submodule->open_question[0]->id];
                                    echo '<div class="original-answer light-bg">';
                                        echo '<p class="mt-2 mb-2 small-text">Origineel antwoord: <strong>' . $original_answer . '</strong></p>';
                                    echo '</div>';
                                }
                            echo '</div>';
                        echo '</div>';
                    }

                    // Get the question introduction for this submodule

                    echo '<div class="scan-highlight">';
                    echo '<h3>' . $submodule->questionIntro[0]->excerpt . '</h3>';

                    echo '<div class="answer-area">';
                    // Loop over questions
                    foreach ($submodule->questionIntro[0]->questions as $question) {

                        echo '<div class="row">';
                        echo '<div class="col-md-12 mb-4 question-box">';
                        echo '<p>' . $question->question_description . '</p>';

                        switch ($question->question_type) {
                            // Radio input type => single option as answer
                            case 'single':
                                foreach ($question->possibleAnswers as $possible_answer) {
                                    if ($data->survey->answers[$question->question_description_id] != $data->survey->reviewedAnswers[$question->question_description_id]) {
                                        $checked = ($possible_answer->id == $data->survey->reviewedAnswers[$question->question_description_id]) ? 'checked' : '';
                                    } else {
                                        $checked = ($possible_answer->id == $data->survey->answers[$question->question_description_id]) ? 'checked' : '';
                                    }
                                    echo '<label><input type="radio" name="question_' . $data->survey->answerIds[$question->question_description_id] . '" value="' . $possible_answer->id . '" ' . $checked . '>&nbsp; ' . $possible_answer->description . '</label><br />';
                                }
                                if ($data->survey->answers[$question->question_description_id] != $data->survey->reviewedAnswers[$question->question_description_id]) {
                                    foreach ($question->possibleAnswers as $possible_answer) {
                                        if ($possible_answer->id == $data->survey->answers[$question->question_description_id]) {
                                            echo '<div class="original-answer dark-bg">';
                                                echo '<p class="mt-2 mb-2 small-text">Origineel antwoord:  <strong>' . $possible_answer->description . '</strong></p>';
                                            echo '</div>';
                                        }
                                    }
                                }
                            break;

                            // Dropdown to select an option => single selection only
                            case 'dropdown':
                                echo '<select class="form-control" name="question_' . $data->survey->answerIds[$question->question_description_id] . '">';
                                    echo '<option value="-">Selecteer antwoord</option>';

                                foreach ($question->possibleAnswers as $possible_answer) {
                                    if ($data->survey->answers[$question->question_description_id] != $data->survey->reviewedAnswers[$question->question_description_id]) {
                                        $selected = ($possible_answer->id == $data->survey->reviewedAnswers[$question->question_description_id]) ? 'selected="selected"' : '';
                                    } else {
                                        $selected = ($possible_answer->id == $data->survey->answers[$question->question_description_id]) ? 'selected="selected"' : '';
                                    }
                                    echo '<option value="' . $possible_answer->id . '" ' . $selected . '>' . $possible_answer->description . '</option>';
                                }
                                echo '</select>';
                                if ($data->survey->answers[$question->question_description_id] != $data->survey->reviewedAnswers[$question->question_description_id]) {
                                    foreach ($question->possibleAnswers as $possible_answer) {
                                        if ($possible_answer->id == $data->survey->answers[$question->question_description_id]) {
                                            echo '<div class="original-answer dark-bg">';
                                                echo '<p class="mt-2 mb-2 small-text">Origineel antwoord:  <strong>' . $possible_answer->description . '</strong></p>';
                                            echo '</div>';
                                        }
                                    }
                                }
                            break;
                            
                            default:
                                # code...
                                break;
                        }

                        echo '</div>';
                        echo '</div>';

                    }

                    echo '</div>';
                    echo '</div>';

                    echo '<div class="row mb-5">';
                    
                    // Previous button => browse submodule
                    echo '<div class="col-md-6 text-left">';
                    if ($i != 0) {
                        echo '<a class="btn btn-primary btn-highlight-outline-link" onclick="previousModule(this);" data-index="' . $i . '"><i class="fas fa-arrow-left mr-3"></i> Vorige module</a>';
                    };
                    echo '</div>';

                    // Next button => browse submodule
                    if (($i + 1) == $totalModules) {
                        echo '<div class="col-md-6 text-right"><button class="btn btn-success btn-highlight-link" data-index="' . $i . '">Review opslaan</button></div>';
                    } else {
                        echo '<div class="col-md-6 text-right"><a class="btn btn-primary btn-highlight-outline-link" onclick="nextModule(this);" data-index="' . $i . '">Volgende module  <i class="fas fa-arrow-right ml-3"></i></a></div>';
                    }
                    echo '</div>';

                    echo '</div>';

                    $i++;

                }

                echo '</div>';
                echo '</form>';

            ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>