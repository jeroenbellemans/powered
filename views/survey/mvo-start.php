<?php
    
    if (isset($_POST['start-scan'])) {
        $postData = new \StdClass();

        $postData->year = $_POST['year'];

        $survey = $data->instance->setYearForScan($postData, 'survey/mvo/start/scan');

    }
?>

<?php include 'views/partials/headers/header.php'; ?>

<div class="container">

    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->mvoSurveys->completed) : ?>
                    <?php foreach ($data->mvoSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/mvo/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->mvoSurveys->incompleted) : ?>
                    <?php foreach ($data->mvoSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/mvo/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <div class="col-md-9">
            <h1>MVO scan</h1>
            <p>Maatschappelijk Verantwoord Ondernemen is niet alleen een laadpaal voor de deur of zonnepanelen op je dak. Het houdt veel meer in dan dat. En weet u wat het leukste is? Na het invullen van deze vragenlijst weet je dat u al een hele hoop zaken goed geregeld heeft! We gaan jouw bedrijf toetsen op 9 thema’s.</p>
            <p>Deze thema's hebben een link naar de ISO 26.000, een richtlijn voor maatschappelijke verantwoordelijkheid van organisaties. De richtlijn legt uit wat MVO betekent en helpt organisaties bij de toepassing in de praktijk.</p>
            <h2>Algemene informatie</h2>
            <form action="" method="post">
                <div class="row mt-4">
                    <div class="col-md-3 text-right">
                        <div class="form-group">
                            <label for="role">Jaar</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php $year = ($data->incompleteSurvey[0]->subject_year != '') ? $data->incompleteSurvey[0]->subject_year : ''; ?>
                            <input type="text" class="form-control form-control-sm" name="year" required="required" placeholder="Jaar" value="<?php echo $year; ?>">
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-3 text-right">
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <button type="submit" name="start-scan" class="btn btn-primary btn-highlight-link">De gegevens zijn volledig <i class="fas fa-check ml-3"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>