<?php
    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['sdg-selection'])) {
            $postData->sdg = $_POST['sdg'];
            $postData->moduleId = $data->moduleId;
            $postData->surveyId = $data->context;

            $admin = $data->instance->saveSdgSelection($postData);

            debug_print($admin);

        } else

        if (isset($_POST['add-establishment'])) {

            $postData->companyName = $_POST['companyname'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->postalCode = $_POST['zipcode'];
            $postData->country = $_POST['country'];
            $postData->fte = $_POST['fte-add'];
            $postData->sqft = $_POST['sqft-add'];
            $postData->mainBranch = '0';

            $company = $data->instance->createSubCompany($postData);

            debug_print($company);

        } else
        
        if (isset($_POST['start-scan'])) {

            $postData->year = $_POST['year'];

            $survey = $data->instance->setYearForScan($postData, 'survey/footprint/start/scan');

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Voltooide scans<i class="fas fa-check float-right mt-1"></i></li>
                <?php if ($data->coSurveys->completed) : ?>
                    <?php foreach ($data->coSurveys->completed as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/results/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completion_date, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>

            <ul class="nav flex-column sidenavpanel mt-4">
                <li class="sidenavpanel-title">Lopende scans<i class="fas fa-pen float-right mt-1"></i></li>
                <?php if ($data->coSurveys->incompleted) : ?>
                    <?php foreach ($data->coSurveys->incompleted as $completed_survey) : ?>
                    <?php 
                        $dateTime = new \DateTime();
                        $dateTime = $dateTime->setTimestamp($completed_survey->module_completion_date);
                        $completion_date = $dateTime->format('d/m/Y');    
                    ?>
                    <?php echo '<li class="nav-item">' . Routes::build('survey/footprint/' . $completed_survey->module_id .'/' . $completed_survey->survey_id, '<i class="fas fa-angle-right mr-2"></i>' . $completed_survey->module_name, array("class" => "nav-link")) . '</li>'; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li class="nav-item no-result-list-item">Geen resultaten</li>
                <?php endif; ?>
            </ul>
        </div>

        <?php // debug_print($data); ?>

        <div class="col-md-9">
            <h1>CO2 scan</h1>
            <p>Hoeveel broeikasgas stoot jouw bedrijf uit? CO2 ofwel koolstofdioxide is het belangrijkste broeikasgas. Aan de hand van onder andere uw brandstofgebruik en energienota wordt de CO2 footprint berekend.</p>
            <p>Broeikasgassen komen van nature in de atmosfeer voor, maar als gevolg van menselijke activiteiten, neemt de hoeveelheid extreem sterk toe en verandert het klimaat. De EU wil de uitstoot in 2030 met 55% terugbrengen ten opzichte van 1990.</p>
            <h2>Algemene informatie</h2>
            <form action="" method="post">
                <div class="row mt-4">
                    <div class="col-md-3 text-right">
                        <div class="form-group">
                            <label for="role">Jaar</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php $year = ($data->incompleteSurvey[0]->subject_year != '') ? $data->incompleteSurvey[0]->subject_year : ''; ?>
                            <input type="text" class="form-control form-control-sm" name="year" required="required" placeholder="Jaar" value="<?php echo $year; ?>">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-3 text-right">
                        <div class="form-group">
                            <label for="role">Hoofdvestiging</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <?php if ($data->establishment) : ?>
                            <?php foreach ($data->establishment as $main_establishment) : ?>
                            <div class='result mt-2 mb-4'>
                                <p class='result-title'><?php echo $main_establishment->company_name; ?></p>
                                <p class='result-data' style='margin-bottom: 0'>
                                    <?php echo $main_establishment->establishment_street; ?> <?php echo $main_establishment->establishment_number; ?><br />
                                    <?php echo $main_establishment->establishment_city; ?> <?php echo $main_establishment->establishment_pc; ?><br />
                                    <?php echo $main_establishment->establishment_country; ?>
                                </p>
                            </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                        <div class="form-group">
                            <p class="small-text">Gelieve uw administrator te contacteren zodat deze uw hoofdvestiging kan bevestigen.</p>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if ($data->establishment) : ?>
                        <?php foreach ($data->establishment as $main_establishment) : ?>
                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Hoofdvestiging FTE</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php $fte = ($main_establishment->fte != '') ? $main_establishment->fte : ''; ?>
                                        <input type="text" class="form-control form-control-sm" name="fte" required="required" placeholder="FTE" value="<?php echo $fte; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary btn-highlight-link small" onclick="saveField(this, 'fte', '<?php echo $main_establishment->id; ?>');">Opslaan</button>
                                </div>
                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Hoofdvestiging M²</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?php $sqft = ($main_establishment->sqft != '') ? $main_establishment->sqft : ''; ?>
                                        <input type="text" class="form-control form-control-sm" name="sqft" required="required" placeholder="M²" value="<?php echo $sqft; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-primary btn-highlight-link small" onclick="saveField(this, 'sqft', '<?php echo $main_establishment->id; ?>');">Opslaan</button>
                                </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="col-md-3 text-right">
                        <div class="form-group">
                            <label for="role">Andere vestigingen</label><br />
                            
                        </div>
                    </div>
                    <div class="col-md-9">
                        <?php if ($data->subEstablishment) : ?>
                            <?php foreach ($data->subEstablishment as $sub_establishment) : ?>
                            <div class='result sub mt-2'>
                                <p class='result-title'><?php echo $sub_establishment->company_name; ?></p>
                                <p class='result-data' style='margin-bottom: 0'>
                                    <?php echo $sub_establishment->establishment_street; ?> <?php echo $sub_establishment->establishment_number; ?><br />
                                    <?php echo $sub_establishment->establishment_city; ?> <?php echo $sub_establishment->establishment_pc; ?><br />
                                    <?php echo $sub_establishment->establishment_country; ?>
                                </p>
                                <!--
                                <div class="delete-establishment">
                                    <i class="fas fa-trash-alt delete" onclick="deleteEstablishment('<?php echo $user->id; ?>');"></i>
                                </div>
                                -->
                            </div>
                            <?php endforeach; ?>
                            <div class="form-group">
                                <p class="small-text"><span class="float-right" style="margin-top: -2px;"><a class="btn btn-primary btn-highlight-outline-link small-outline mb-5" href="javascript:void(0);" onclick="toggleAddEstablishment();">Vestiging toevoegen <i class="fas fa-plus ml-3"></i></a></span></p>
                            </div>
                        <?php else : ?>
                        <div class="form-group">
                            <p class="small-text">U heeft momenteel geen andere vestigingen.<span class="float-right" style="margin-top: -2px;"><a class="btn btn-primary btn-highlight-outline-link small-outline" href="javascript:void(0);" onclick="toggleAddEstablishment();">Vestiging toevoegen <i class="fas fa-plus ml-3"></i></a></span></p>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-3 text-right">
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <button type="submit" name="start-scan" class="btn btn-primary btn-highlight-link">De gegevens zijn volledig <i class="fas fa-check ml-3"></i></button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="add-establishment hidden">
                <h2>Vestiging toevoegen</h2>
                <form action="" method="post">
                    <div class="row">
                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">Bedrijfsnaam</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="companyname" required="required" placeholder="Bedrijfsnaam">
                            </div>
                        </div>

                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">Straat + huisnummer</label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="street" required="required" placeholder="Straat">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="number" required="required" placeholder="Huisnummer">
                            </div>
                        </div>

                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">Plaats + postcode</label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="city" required="required" placeholder="Plaats">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="zipcode" required="required" placeholder="Postcode">
                            </div>
                        </div>

                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">Land</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="country" required="required" placeholder="Land">
                            </div>
                        </div>

                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">FTE</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="fte-add" required="required" placeholder="FTE">
                            </div>
                        </div>

                        <div class="col-md-3 text-right">
                            <div class="form-group">
                                <label for="role">M²</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-sm" name="sqft-add" required="required" placeholder="M²">
                                <button type="submit" name="add-establishment" class="btn btn-primary btn-highlight-link mt-5">Vestiging toevoegen <i class="fas fa-save ml-3"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>