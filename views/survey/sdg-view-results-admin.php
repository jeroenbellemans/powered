<?php
    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['sdg-selection'])) {
            $postData->sdg = $_POST['sdg'];
            $postData->originalSdg = $_POST['original-sdg'];
            $postData->moduleId = $data->moduleId;
            $postData->surveyId = $data->context;

            $admin = $data->instance->saveSdgSelection($postData, $data->userInfo->personal->id, true);

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <?php // debug_print($data); ?>

        <div class="col-md-9">
            <h1>SDG scan</h1>
            <p>Selecteer tot 5 doelen waarvan je denkt dat jouw bedrijf inspanningen en moeite doet om deze te behalen.</p>
            <h2>Duurzame Ontwikkelingsdoelen</h2>

            <div id="sdggoal" class="alert alert-warning hidden" role="alert">
                U heeft reeds het maximum van 5 duurzame ontwikkelingsdoelen geselecteerd.
            </div>

            <?php if ($data->sdgScan->goals) : ?>
                <div class="row">
                <?php $i = 1; ?>
                <?php foreach ($data->sdgScan->goals as $sdgGoal) : ?>
                    <?php $selected = (in_array($sdgGoal->id, $data->selectedSdg)) ? 'selected' : ''; ?>
                    <div class="col-md-3 col-xs-2 mb-4">
                        <div class="card <?php echo $selected; ?>" onclick="toggleGoals(this)" data-id="<?php echo $sdgGoal->id; ?>">
                            <img src="<?php echo config_get_root(); ?><?php echo $sdgGoal->goal_location; ?><?php echo $sdgGoal->goal_version; ?>/sdg-goal-<?php echo $i++; ?>.png" class="card-img-top" alt="<?php echo $sdgGoal->goal; ?>">
                            <div class="selection">
                                <i class="fas fa-check"></i>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="row mb-5 mt-5">
                    <div class="col-md-12 text-right">
                        <form action="" method="post">
                            <input type="hidden" name="sdg" value="<?php echo implode("/", $data->selectedSdg); ?>/" />
                            <input type="hidden" name="original-sdg" value="<?php echo implode("/", $data->selectedSdg); ?>/" />
                            <button type="submit" name="sdg-selection" class="btn btn-success btn-highlight-link">Selectie bevestigen</button>
                        </form>
                    </div>
                </div>
            <?php else : ?>
                <p>We kunnen momenteel geen Duurzame Ontwikkelingsdoelen weergeven.</p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>