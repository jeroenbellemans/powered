<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/myaccount-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Mijn reisgenoten</h1>
            <p>U weet natuurlijk goed hoe uw bedrijf presteert op het gebied van duurzaamheid en Maatschappelijk Verantwoord Ondernemen. Als u een medewerker ook de duurzame inovatiescan wenst te laten invullen kunt u hier snel een account voor hun aanmaken.</p>

            <h2 class="mt-5">Overzicht reisgenoten <?php if ($data->contacts) : ?><span class="float-right" style="margin-top: -2px;"><?php echo Routes::build('my-account/my-contacts/create', 'Reisgenoot toevoegen <i class="fas fa-plus ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link small")); ?></span><?php endif; ?></h2>

            <?php if ($data->contacts) : ?>

                <?php foreach ($data->contacts as $contact) : ?>
                <div class="row mb-1">
                    <div class="col-md-12 list-item">
                        <div class="row">
                            <div class="col-md-4"><?php echo Routes::build('my-account/my-contacts/view/' . $contact->id, $contact->firstname . ' ' . $contact->lastname); ?></div>
                            <div class="col-md-5"><?php echo $contact->email; ?></div>
                            <div class="col-md-3 text-right">
                                <?php 
                                if ($contact->status == '0') :
                                    echo '<span class="badge badge-warning">In afwachting</span>';
                                elseif ($contact->status == '1') :
                                    echo '<span class="badge badge-success">Geactiveerd</span>';
                                elseif ($contact->status == '2') :
                                    echo '<span class="badge badge-danger">Geblokkeerd</span>';
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            <?php else : ?>
                    <p>Je hebt nog geen reisgenoten toegevoegd aan jouw account.</p>
                    <?php if ($data->guardian->passportCheck('contacts@create_contacts', $data->accountInfo->permission->id)): ?>
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 30px;">
                                <?php echo Routes::build('my-account/my-contacts/create', 'Reisgenoot toevoegen <i class="fas fa-plus ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link mt-4")); ?>
                            </div>
                        </div>
                    <?php endif; ?>
            <?php endif; ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>