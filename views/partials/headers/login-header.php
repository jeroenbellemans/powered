<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo config_get_title(); ?></title>
        <link rel="icon" type="image/png" href="<?php config_get_root(); ?>/resources/images/favicon.png">
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
        <link rel="stylesheet" href="https://use.typekit.net/oqz8ruh.css">
        <link rel="stylesheet" href="<?php config_get_root(); ?>/resources/style/css/power-ed.css">
        <script src="https://kit.fontawesome.com/104cee8e58.js" crossorigin="anonymous" SameSite=None></script>
    </head>
    <body>