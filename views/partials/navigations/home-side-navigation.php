<?php // debug_print($data->survey); ?>

<ul class="nav flex-column sidenavpanel">

    <?php 
        /*foreach ($data->sidenavigation->modules as $module) {
            echo '<li class="nav-item">' . Routes::build('survey/' . strtolower($module->alias), '<i class="fas fa-angle-right mr-2"></i>' . $module->name, array("class" => "nav-link")) . '</li>';
        } */
    ?>
    
    <li class="nav-item">
        <?php echo Routes::build('survey/mvo', '<i class="fas fa-angle-right mr-2"></i> MVO scan', array("class" => "nav-link")); ?>
        <!--<ul>
            <li class="nav-item sub"><?php echo Routes::build('footprint', '2020', array("class" => "nav-link")); ?></li>
            <li class="nav-item sub"><?php echo Routes::build('footprint', '2021', array("class" => "nav-link")); ?></li>
        </ul> -->
    </li>
    <li class="nav-item"><?php echo Routes::build('survey/footprint', '<i class="fas fa-angle-right mr-2"></i> CO2 scan', array("class" => "nav-link")); ?></li>
    <li class="nav-item"><?php echo Routes::build('survey/sdg', '<i class="fas fa-angle-right mr-2"></i> SDG scan', array("class" => "nav-link")); ?></li>
    

</ul>