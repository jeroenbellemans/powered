<ul class="nav flex-column sidenavpanel">
<?php // debug_print($data->accountInfo); ?>
    <?php if ($data->guardian->passportCheck('users@view_users', $data->accountInfo->permission->id)): ?>
    <li class="nav-item">
        <?php echo Routes::build('admin/users/overview', '<i class="fas fa-angle-right mr-2"></i> Gebruikers', array("class" => "nav-link")); ?>
    </li>
    <?php endif; ?>
    <?php if ($data->guardian->passportCheck('permissions@view_permissions', $data->accountInfo->permission->id)): ?>
        <li class="nav-item"><?php echo Routes::build('admin/permissions/overview', '<i class="fas fa-angle-right mr-2"></i> Profielen', array("class" => "nav-link")); ?></li>
    <?php endif; ?>
    <?php if ($data->guardian->passportCheck('clients@view_clients', $data->accountInfo->permission->id)): ?>
        <li class="nav-item"><?php echo Routes::build('admin/clients/overview', '<i class="fas fa-angle-right mr-2"></i> Klanten', array("class" => "nav-link")); ?></li>
    <?php endif; ?>
</ul>