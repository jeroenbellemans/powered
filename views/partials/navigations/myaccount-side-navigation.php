<ul class="nav flex-column sidenavpanel">
    <li class="nav-item"><?php echo Routes::build('my-account/personal-details', '<i class="fas fa-angle-right mr-2"></i> Mijn gegevens', array("class" => "nav-link"))?></li>
    <?php if ($data->guardian->passportCheck('contacts@create_contacts', $data->accountInfo->permission->id)): ?>
        <li class="nav-item"><?php echo Routes::build('my-account/my-contacts', '<i class="fas fa-angle-right mr-2"></i> Mijn reisgenoten', array("class" => "nav-link")); ?></li>
    <?php endif; ?>
</ul>