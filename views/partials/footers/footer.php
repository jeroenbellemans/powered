
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
        <script src="<?php echo config_get_root(); ?>resources/js/moment.js"></script>
        <script type="text/javascript">

            <?php
                if (isset($data->survey->answersPerQuestion)) {
                    $answers = json_encode($data->survey->answersPerQuestion);
                }

                if (isset($data->survey->allQuestions)) {
                    $questions = json_encode($data->survey->allQuestions);
                }
            ?>

            <?php if (isset($answer)) : ?>var answers = '<?php echo $answers; ?>';<?php endif; ?>
            <?php if (isset($questions)) : ?>var questions = '<?php echo $questions; ?>';<?php endif; ?>

            $(document).ready(function() {

                if(window.location.hash != "") {
                    $('a[href="' + window.location.hash + '"]').click()
                }

                var price = '8877.00000';
                console.log(price);

                var parsedPrice = parseInt(price);
                console.log(parsedPrice);

            });

            function setRole(context) {
                var role = $(context).val();

                window.location = '/admin/permissions/overview/' + role;
            }

            function previousModule(context, validate) {

                // Get the current index
                currentSubmodule = $(context).attr("data-index");
                previousSubmodule = parseInt(currentSubmodule) - 1;

                // Validate the current active submodule, and check if all questions have been answered

                // Hide the current submodule
                $("#submodule-" + currentSubmodule).removeClass("show active");

                // Set the next submodule active
                $("#submodule-" + previousSubmodule).delay(1500).addClass("show active");

                // Scroll back to top
                window.scrollTo(0, 0);

            }

            function nextModule(context, validate) {

                // Get the current index
                currentSubmodule = $(context).attr("data-index");
                nextSubmodule = parseInt(currentSubmodule) + 1;
                
                // Validate the current active submodule, and check if all questions have been answered
                if (validateModule(context, currentSubmodule, validate)) {
                    // Hide (eventual) error message
                    $("#submodule-" + currentSubmodule + " .alert").addClass("hidden");

                    // Hide the current submodule
                    $("#submodule-" + currentSubmodule).removeClass("show active");

                    // Set the next submodule active
                    $("#submodule-" + nextSubmodule).delay(1500).addClass("show active");

                } else {
                    // Show error message
                    $("#submodule-" + currentSubmodule + " .alert").removeClass("hidden");
                }

                // Scroll back to top
                window.scrollTo(0, 0);

            }

            function validateModule(context, currentSubmodule, validate) {

                var errorInForm = false;

                if (validate) {
                    $("#submodule-" + currentSubmodule + " input, #submodule-" + currentSubmodule + " select, #submodule-" + currentSubmodule + " textarea").each(function() {
                        console.log(this.type);
                        switch (this.type) {
                            case 'radio':
                                // Get radio's group name
                                var radioGroup = this.name;
                                if (!$("input[name=" + radioGroup + "]").is(":checked")) {
                                    errorInForm = true;
                                }
                            break;

                            case 'checkbox':
                                // Get checkbox's group name
                                var checkGroup = this.name;
                                console.log(checkGroup);
                                if (!$("input[name=" + checkGroup + "]").is(":checked")) {
                                    errorInForm = true;
                                }
                            break;

                            case 'textarea':
                                // Get textarea value
                                // var area = this.name;
                                // if ($("textarea[name=" + area + "]").val().trim() == '') {
                                //     errorInForm = true;
                                // }
                            break;

                            case 'text':
                                // Get input value
                                var area = this.name;
                                if (!$("input[name=" + area + "]").val().trim() == '') {
                                    errorInForm = true;
                                }
                            break;

                            case 'select-one':
                                if (this.value == '-') {
                                    errorInForm = true;
                                } 
                            break;
                        }
                    });
                }

                if (!errorInForm) {
                    return true;
                }

                return false;

            }

            function startSearch() {
                if ($("input[name='kvk']").val().trim() != '') {
                    searchKvkNumber();
                }

                return;
            }

            function searchKvkNumber() {

                var search = $("input[name='kvk']").val().trim();

                $.ajax({
                    type: "post",
                    url: "/resources/js/ajax/search-kvk-number.php",
                    data: { "search": search },
                    beforeSend: function() {
                        $(".search-result").append("<p>Even geduld aub...</p>");
                    },
                    success: function(resp) {

                        var response = $.parseJSON(resp);
                        var error = response.result.error;

                        if (error) {
                            $(".search-result").empty();
                            $(".search-result").append("<h2 class='error-response'>Er is een fout opgetreden</p>");
                            $(".search-result").append("<p class='error-response'>Volgens de gegevens van de Kamer Van Koophandel, kunnen we geen gegevens tonen voor het KVK nummer: \"<strong>" + search + "</strong>\".</p>");
                            $(".search-result").append("<p class='error-response'>Dit houdt u niet tegen om opnieuw te proberen.</p>");
                        } else
                        if (response.success) {
                            $(".search-result").empty();

                            var html = "";

                            response.result.resultaten.forEach(function(result) {

                                if (result.type != 'rechtspersoon') {
                                
                                    html += "<div class='result mt-2'>";
                                    html += "<p class='result-title'>" + result.handelsnaam + "</p>";
                                    html += "<p class='result-data' style='margin-bottom: 0'>";
                                    html += "<strong>" + result.type + "</strong><br>";
                                    html += result.straatnaam + ", " + result.plaats + "<br />";
                                    html += "</p>";
                                    html += "<div class='save'>";
                                    html += "<button type='button'  onclick='prefillCompanyDetails(" + JSON.stringify(result) + ")' class='btn btn-primary small'><i class='fas fa-check mr-1'></i>Gebruik deze vestiging</button>";
                                    html += "</div>";
                                    html += "</div>";
                                    
                                }
                                
                            });

                            $(".search-result").append(html);
                        }
                    }
                });
            }

            function prefillCompanyDetails(companyDetails) {

                let basicProfile = companyDetails.links.filter(function(link) {
                    return (link.rel == "vestigingsprofiel");
                });

                $.ajax({
                    type: "post",
                    url: "/resources/js/ajax/search-profile.php",
                    data: { "search": basicProfile[0].href },
                    success: function(resp) {

                        var response = $.parseJSON(resp);
                        var error = response.result.error;

                        if (error) {
                            $(".search-result").empty();
                            $(".search-result").append("<h2 class='error-response'>Er is een fout opgetreden</p>");
                            $(".search-result").append("<p class='error-response'>Volgens de gegevens van de Kamer Van Koophandel kunnen we geen gegevens profiel tonen voor het volgende KVK nummer: \"<strong>" + search + "</strong>\".</p>");
                            $(".search-result").append("<p class='error-response'>Dit houdt u niet tegen om opnieuw te proberen.</p>");
                        } else
                        if (response.success) {
                
                            $("#companyName").val(companyDetails.handelsnaam);
                            $("#kvkNumber").val(companyDetails.kvkNummer);

                            response.result.sbiActiviteiten.forEach(function(businessActivity) {
                                if (businessActivity.indHoofdactiviteit) {
                                    $("#sbiCode").val(businessActivity.sbiCode);
                                    $("#sbiCodeDescription").val(businessActivity.sbiOmschrijving);
                                }
                            });

                            let vestiging = response.result.adressen.filter(function(adres) {
                                return (adres.type == "bezoekadres");
                            });

                            $("#zipCode").val(vestiging[0].postcode);
                            $("#street").val(vestiging[0].straatnaam);
                            $("#number").val(vestiging[0].huisnummer);
                            $("#city").val(vestiging[0].plaats);
                            $("#country").val(vestiging[0].land);

                            var isMainBranch = (companyDetails.type == "hoofdvestiging");

                            $("#ismainsbi").val(isMainBranch);
                            $("#establishmentCode").val(companyDetails.vestigingsnummer);

                            $(".company-details-form").show();

                        }
                    }
                });
            }

            /*
            function saveCompanyDetails(object) {
                $.ajax({
                    type: "post",
                    url: "/resources/js/ajax/save-company-details.php",
                    data: { "data": object },
                    beforeSend: function() {
                        $(".search-result").append("<p>Even geduld, we slaan de gegevens van jouw bedrijf op...</p>");
                    },
                    success: function(resp) {
                        var response = $.parseJSON(resp);
                        if (response.success) {
                            $(".search-result").empty();
                            window.location = '/my-account/company-details';
                        } else {
                            $(".search-result").empty();
                            $(".search-result").append("<h2>Oeps</h2>");
                            $(".search-result").append("<p>Er is een fout opgetreden. Wij konden de gegevens niet opslaan. Gelieve contact op te nemen met uw administrator.</p>");
                        }
                    }
                });
            }
            */

            function toggleGoals(context) {
                var maxGoals = 5;
                var selectedGoals = 0;

                // Hide (eventual) error message
                $("#sdggoal").addClass("hidden");

                if (!$(context).hasClass("selected")) {
                    $(".card.selected").each(function() {
                        selectedGoals++;
                    });
                }
                
                if (selectedGoals == 5) {
                    // Show error message
                    $("#sdggoal").removeClass("hidden");

                    // Scroll back to top
                    window.scrollTo(0, 0);
                } else {

                    $(context).toggleClass("selected");

                    var selectedSdg = '';
                    $(".card.selected").each(function() {
                        selectedSdg += $(this).attr("data-id") + '/';
                    });

                    $("input[name='sdg']").val(selectedSdg);
                }
            }

            function showFormForEstablishment(context) {
                var targetTabId = $(context).val();

                $(".sub-tab").each(function() {
                    if (!$(this).hasClass("hidden")) {
                        $(this).addClass("hidden");
                    }
                });

                $(".sub-tab-" + targetTabId).removeClass("hidden");
            }

            function saveField(context, fieldName, companyId) {

                var fieldValue = $("input[name='" + fieldName + "']").val();

                $.ajax({
                    type: "post",
                    url: "/resources/js/ajax/save-company-details-field.php",
                    data: { "fieldValue": fieldValue, "fieldName": fieldName, "companyId": companyId },
                    success: function(resp) {
                        var response = $.parseJSON(resp);
                        if (response.success) {
                            window.location = '/survey/footprint/start';
                        }
                    },
                    error: function (resp) {
                        console.log(resp);
                    }
                });
            }

            function toggleAddEstablishment() {
                $(".add-establishment").toggleClass("hidden");
            }

            function copyUtility() {
                var copyText = document.getElementById("copy-text");
                console.log(copyText);
                copyText.select();
                copyText.setSelectionRange(0, 99999);
                document.execCommand("copy");
                alert("De activatielink is gekopiëerd!");
            }

            function loadQuestionOrAnswer(context, event, submoduleId) {
                console.log("coucou");
                event.preventDefault();
                event.stopPropagation();

                allAnswers = JSON.parse(answers);
                allQuestions = JSON.parse(questions);

                if (!$(context).find("input[type='checkbox']").is(":checked")) {
                    $(context).find("input[type='checkbox'").prop("checked", true);
                } else {
                    $(context).find("input[type='checkbox'").prop("checked", false);
                }
                
                var id = $(context).attr("data-id"),
                    answer = $(context).attr("data-answer"),
                    parent = $(context).attr("data-parent-id"),
                    hasChild = $(context).attr("data-has-child"),
                    childQuestionId = $(context).attr("data-child-id"),
                    unit = $(context).attr("data-unit"),
                    establishment = ($(context).attr("data-establishment")) ? $(context).attr("data-establishment") : '',
                    answerType = $(context).attr("data-answer-type");

                if ($(context).find("input[type='checkbox']").is(":checked")) {

                    if (hasChild == '1') {

                        // Fetch the next question based upon an answer
                        var filteredQuestions = _.where(allQuestions, {"id": childQuestionId});

                        _.each(filteredQuestions, function(filteredQuestion) {

                            console.log(filteredQuestion);

                            var html = '';
                                html += '<p>' + filteredQuestion.question + '</p>';

                            // Check if the filteredQuestion has answers available
                            if (allAnswers[filteredQuestion.id]) {
                                _.each(allAnswers[filteredQuestion.id], function(relatedAnswer) {
                                    html += '<div class="col-md-6">';
                                        html += '<label data-id="' + relatedAnswer.id + '" data-establishment="' + establishment + '" data-parent-id="' + filteredQuestion.id + '" data-answer="' + relatedAnswer.answer + '" data-has-child="' + relatedAnswer.has_child_question + '" data-child-id="' + relatedAnswer.child_question_id + '" data-unit="' + relatedAnswer.unit + '" data-answer-type="' + relatedAnswer.answer_type + '" onclick="loadQuestionOrAnswer(this, event, ' + submoduleId + ');"><input type="checkbox" name="answer_' + id + '@' + establishment + '+' + submoduleId + '[]" value="' + relatedAnswer.id + '">&nbsp;' + relatedAnswer.answer + '</label>';
                                    html += '</div>';
                                    html += '<div class="col-md-6">';
                                        html += '<div class="answer-loaded-' + relatedAnswer.id + '"></div>';
                                    html += '</div>';
                                    html += '<div class="col-md-12 ml-5">';
                                        html += '<div class="question-loaded-' + relatedAnswer.id + '"></div>';
                                    html += '</div>';
                                });
                            }

                            $(".question-loaded-" + id).append(html);
                        });

                    } else {

                        var targetElement = $(context).parent().next().find(".answer-loaded-" + id);

                        switch (answerType) {
                            case 'input':

                                var html = '';
                                    html += '<div class="row mb-2">';
                                        html += '<div class="col-md-12">';
                                            html += '<div class="input-group">';
                                                html += '<div class="input-group-prepend">';
                                                    html += '<div class="input-group-text">Hoeveelheid</div>';
                                                html += '</div>';
                                                html += '<input class="form-control form-control-sm" type="text" value="" name="input-answer_' + id + '@' + establishment + '+' + submoduleId + '" placeholder="' + answer + '" />';
                                                html += '<div class="input-group-append">';
                                                    html += '<div class="input-group-text">' + unit + '</div>';
                                                html += '</div>';
                                            html += '</div>';
                                        html += '</div>';
                                    html += '</div>';

                                    $(targetElement).append(html);
                                break;
                        
                            default:
                                break;
                        }

                    }

                } else {
                    var targetElement = $(".answer-loaded-" + id).empty();
                    var targetElement = $(".question-loaded-" + id).empty();
                }
            }

            function deleteUser(context) {
                if (confirm('Bent u zeker dat u deze gebruiker wenst te verwijderen?')) {
                    $.ajax({
                        type: "post",
                        url: "/resources/js/ajax/delete-user.php",
                        data: { "id": context },
                        success: function(resp) {
                            var response = $.parseJSON(resp);
                            if (response.success) {
                                window.location = '/admin/users/overview';
                            }
                        }
                    });
                }
            }

            function deleteClient(context) {
                if (confirm('Bent u zeker dat u deze klant wenst te verwijderen?')) {
                    $.ajax({
                        type: "post",
                        url: "/resources/js/ajax/delete-client.php",
                        data: { "id": context },
                        success: function(resp) {
                            var response = $.parseJSON(resp);
                            if (response.success) {
                                window.location = '/admin/clients/overview';
                            }
                        }
                    });
                }
            }

        </script>

    </body>
</html>