<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/home-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Welkom</h1>
            <p>Uw klantreis is begonnen! Om u te voorzien van een goed advies is het belangrijk dat u categorieën goed doorloopt. We maken als het ware een foto van uw bedrijf hoe u ervoor staat op het gebied van Maatschappelijk Verantwoord Ondernemen.</p>
            <p>Na het vullen van de 3 scans ontvangt u van ons een advies.</p>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>