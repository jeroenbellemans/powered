<?php
    $response = new StdClass();
    $response->success = true;

    if (form_posted()) {

        $postData = (object) $_POST;

        if (!filter_var($postData->email, FILTER_VALIDATE_EMAIL)) {
            $response->message = 'This is an invalid e-mail address';
            $response->success = false;
        } else {
            $account = new \App\controllers\AccountController();
            $response = $account->sign_in($postData);
            
            if ($response) {
                $response->success = true;
            } else {
                $response->success = false;
                $response->message = 'Het opgegeven e-mail adres of wachtwoord is verkeerd.';
            }
        }

    }

?> 

<?php include 'views/partials/headers/login-header.php'; ?>

    <div class="container"> 
        <div class="row mt-5">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
                <img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo">
                <?php if (!$response->success) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $response->message; ?>
                </div>  
                <?php endif; ?>
                <div class="scan-highlight">
                    <h1 class="login">Aanmelden</h1>
                    <p class="login mb-5">Om toegang te krijgen tot je account moet je je hier aanmelden.</p>
                    <div class="login-box">
                        <form method="post">
                            <label for="inputEmail" class="sr-only">E-mail adres</label>
                            <input class="mb-2 form-control" type="email" name="email" class="form-control" placeholder="E-mail adres" required>
                            <label for="inputPassword" class="sr-only">Wachtwoord</label>
                            <input class="mb-3 form-control" type="password" name="password" class="form-control" placeholder="Wachtwoord" required>
                            <button class="btn btn-md btn-primary btn-block btn-highlight-link" type="submit">Aanmelden</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <?php echo Routes::build('support/forgot-password', 'Wachtwoord vergeten', array("class" => "link")); ?>
            </div>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>