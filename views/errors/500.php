<?php include 'views/partials/headers/error-header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-12">
            <h1>Er is een fout opgetreden</h1>
            <p>Tijdens het verwerken van uw actie, is er zojuist helaas een fout opgetreden.</p>
            <p>
                De administrators zijn op de hoogte van wat er net is fout gegaan, en doen er alles aan om dit zo snel mogelijk op te lossen.<br />
                In afwachting van de oplossing kan u gerust nogmaals proberen.
            </p>
            <p>Onze excuses voor dit ongemak.</p>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12">
            <a href="javascript:history.go(-1);">Ga terug</a>
        </div>
    </div>
</div>
<?php include 'views/partials/footers/footer.php'; ?>