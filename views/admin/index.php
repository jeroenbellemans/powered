<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Beheer</h1>
            <p>Via deze omgeving kunnen administrators de data op het platform beheren. Via het menu aan de linkerkant, kunnen ze een entiteit selecteren en bepaalde acties ondernemen.</p>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>