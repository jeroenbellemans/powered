<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Klanten</h1>
            <p>Op deze pagina kan je een overzicht terugvinden van alle klanten die zich hebben geregistreerd. Per klant kan u nadien alle gebruikers terugvinden.</p>

            <h2 class="mt-5">Overzicht klanten<span class="float-right" style="margin-top: -2px;"><?php echo Routes::build('admin/clients/create', 'Klant toevoegen <i class="fas fa-plus ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link small")); ?></span></h2>

            <?php if ($data->clients) : ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Bedrijfsnaam</th>
                        <th scope="col">Owner</th>
                        <th scope="col" class="text-right">KvK nummer</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($data->clients as $client) : ?>
                <tr>
                    <td>
                        <?php $class = ($client->client_is_deleted) ? 'deleted' : ''; ?>
                        <?php if ($data->guardian->passportCheck('clients@view_clients', $data->accountInfo->permission->id)): ?>
                            <span class="<?php echo $class; ?>"><?php echo Routes::build('admin/clients/view/' . $client->client_id, $client->company_name); ?></span>
                        <?php else : ?>
                            <span class="<?php echo $class; ?>"><?php echo $client->company_name; ?></span>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php $class = ($client->user_is_deleted) ? 'deleted' : ''; ?>
                        <span class="<?php echo $class; ?>"><?php echo $client->firstname; ?> <?php echo $client->insertion . ' '; ?><?php echo $client->lastname; ?></span>
                    </td>
                    <td class="text-right"><?php echo $client->kvk_number; ?></td>
                    <td class="text-right">
                        <i class="fas fa-trash-alt delete" onclick="deleteClient('<?php echo $client->client_id; ?>');"></i>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>