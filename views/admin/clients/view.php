<?php include 'views/partials/headers/header.php'; ?>

<?php // debug_print($data); ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1><?php echo $data->currentClient->company_name; ?></h1>
            <p class="company-description"><?php echo $data->currentClient->sbi_code_description; ?></p>

            <div class="row mt-5">
                <div class="col-md-6">
                    <h2>Adresgegevens</h2>
                    <p>
                        <?php echo $data->currentClient->establishment_street; ?> <?php echo $data->currentClient->establishment_number; ?><br />
                        <?php echo $data->currentClient->establishment_pc; ?> <?php echo $data->currentClient->establishment_city; ?><br />
                        <?php echo $data->currentClient->establishment_country; ?>
                    </p>
                </div>
                <div class="col-md-6">
                    <h2>Contactpersoon</h2>
                    <p>
                        <?php echo $data->currentClient->firstname . ' '; ?><?php echo $data->currentClient->insertion . ' '; ?><?php echo $data->currentClient->lastname; ?>
                    </p>
                </div>
            </div>
            
            <div class="row mt-5">
                <div class="col-md-12">
                    <h2>Overzicht reisgenoten</h2>
                    <?php if ($data->contacts) : ?>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Naam</th>
                                <th scope="col" class="text-right">Type account</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data->contacts as $contact) : ?>
                        <tr>
                            <td>
                                <?php echo Routes::build('admin/clients/contact/' . $contact->id, $contact->firstname . ' ' . $contact->insertion . ' ' . $contact->lastname); ?>
                            </td>
                            <td class="text-right">
                                <?php if ($contact->parent_id == '0') : ?>
                                Owner
                                <?php else : ?>
                                Contact
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>