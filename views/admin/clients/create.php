<?php

    $success = true;

    if (form_posted()) {

        $postData = new \StdClass();

        if (isset($_POST['create-account'])) {

            if (empty($_POST['email'])) {
                $response = false;
                $respose_message = 'Het e-mail adres is verplicht in te vullen';
            } else 
            if (empty($_POST['firstname'])) {
                $response = false;
                $response_message = 'De voornaam is verplicht in te vullen';
            } else
            if (empty($_POST['lastname'])) {
                $response = false;
                $response_message = 'De achternaam is verplicht in te vullen';
            } else {
                $postData->email = $_POST['email'];
                $postData->firstname = $_POST['firstname'];
                $postData->insertion = $_POST['insertion'];
                $postData->lastname = $_POST['lastname'];
                $postData->parentId = '0';

                $postData->businessName = $_POST['company-name'];
                $postData->kvkNumber = $_POST['kvk-number'];
                $postData->sbiCode = $_POST['sbi-code'];
                $postData->sbiCodeDescription = $_POST['sbi-code-description'];
                $postData->street = $_POST['street'];
                $postData->number = $_POST['number'];
                $postData->postalCode = $_POST['zipcode'];
                $postData->city = $_POST['city'];
                $postData->country = $_POST['country'];
                $postData->isMainBranch = $_POST['is-main-sbi'];
                $postData->code = $_POST['establishment-code'];

                $result = $data->instance->createAccountAndCompanyDetails($postData);

                if ($result) {
                    $success = $result["success"];
                    $response_message = $result["response"];
                }
            }
        }
    }
?>
<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>
        <div class="col-md-9">
            <h1>Klanten</h1>
            <p>Hier kan u klanten toevoegen zodat zij hun klantreis kunnen starten. Zowel de gegevens van de eigenaar, als de gegevens vans diens bedrijf moeten worden ingevuld. Om de bedrijfsgegevens op te zoeken kan u via het KvK nummer snel de gegevens opzoeken.</p>
            <form action="" method="post">
                <div class="mt-5">
                    <h2>Eigenaar</h2>
                    <?php if (!$success) : ?>
                    <div class="alert alert-warning">
                        <?php echo $response_message; ?>
                    </div>
                    <?php endif; ?>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Naam</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="firstname" required="required" placeholder="Voornaam">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="insertion" placeholder="Voorvoegsel">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="lastname" required="required" placeholder="Achternaam">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">E-mail adres</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-sm" name="email" required="required" placeholder="E-mail adres">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">KvK nummer</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control form-control-sm" name="kvk" required="required" placeholder="KvK nummer">
                            </div>
                            <div class="col-md-3 text-right">
                                <button type="button" name="search-kvk" class="btn btn-primary btn-highlight-link small" style="width: 100%;" onclick="startSearch();">Start met zoeken <i class="fas fa-search ml-3"></i></button>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="mt-1 search-result"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 company-details-form">
                    <h2>Bedrijfsgegevens</h2>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Bedrijfsnaam</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="companyName" name="company-name" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">KvK nummer</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="kvkNumber" name="kvk-number" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">SBI code</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="sbiCode" name="sbi-code" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">SBI code omschrijving</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="sbiCodeDescription" name="sbi-code-description" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Straat + huisnummer</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" id="street" name="street" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" id="number" name="number" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Plaats + postcode</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" id="city" name="city" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" id="zipCode" name="zipcode" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Land</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="country" name="country" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="hidden" id="ismainsbi" name="is-main-sbi" />
                                    <input type="hidden" id="establishmentCode" name="establishment-code" />
                                    <button type="submit" name="create-account" class="btn btn-primary btn-highlight-link mt-5">Klant opslaan <i class="fas fa-save ml-3"></i></button>
                                    <?php echo Routes::build('admin/clients/overview', 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>