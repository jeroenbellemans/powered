<?php include 'views/partials/headers/header.php'; ?>

<?php // debug_print($data); ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1><?php echo $data->currentContact->personal->firstname; ?><?php echo ' ' . $data->currentContact->personal->insertion; ?><?php echo ' ' . $data->currentContact->personal->lastname; ?></h1>

            <div class="row">
                <div class="col-md-12">
                    <h2>Adresgegevens</h2>
                    <p>
                        <?php echo $data->currentContact->personal->street; ?> <?php echo $data->currentContact->personal->number; ?><br />
                        <?php echo $data->currentContact->personal->zipcode; ?> <?php echo $data->currentContact->personal->city; ?><br />
                        <?php echo $data->currentContact->personal->country; ?>
                    </p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Scans</h2>
                    <p>Hier kan je een overzicht vinden van alle scans waarvoor de gebruiker zich heeft opgegeven. Als administrator kunt u langs deze weg een afgewerkte scan aanpassen.</p>
                    <?php if ($data->surveys) : ?>
                        <?php foreach ($data->surveys as $survey) : ?>
                        <div class="row mb-1">
                            <div class="col-md-12 list-item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?php if ($survey->completed == '1') : ?>
                                            <?php echo Routes::build('admin/clients/survey/' . $survey->module_alias . '/' . $survey->module_id . '/' . $survey->survey_id . '/' . $data->currentContact->personal->id, $survey->module_name); ?>
                                        <?php else : ?>
                                            <?php echo $survey->module_name; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-2">
                                    <?php 
                                        $dateTime = new \DateTime();
                                        $dateTime = $dateTime->setTimestamp($survey->creation_date);
                                        $creation_date = $dateTime->format('d/m/Y');

                                        echo $creation_date;
                                    ?>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <?php if ($survey->is_reviewed == '1') : ?>
                                            Reviewed
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <?php if ($survey->completed == '1') : ?>
                                            Voltooid
                                        <?php else : ?>
                                            In progressie
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class="alert alert-warning" role="alert">
                            Er zijn nog geen gestarte of afgeronde scans om weer te geven.
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>