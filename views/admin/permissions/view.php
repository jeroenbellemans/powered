<?php include 'views/partials/headers/header.php'; ?>


<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

    <?php if ($data->permission) : ?>
        <?php foreach ($data->permission as $permission) : ?>

        <div class="col-md-9">
            <h1>Rechten</h1>
            <p>Op deze pagina zie je een overzicht van alles beschikbare rechten voor het geselecteerde profiel.</p>
            
            <h2 class="mt-5"><?php echo $permission->label; ?><?php if ($data->guardian->passportCheck('permissions@update_permissions', $data->accountInfo->permission->id)): ?><span class="float-right" style="margin-top: -2px;"><?php echo Routes::build('admin/permissions/edit/' . $permission->id, 'Wijzigen <i class="fas fa-edit ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link small")); ?></span><?php endif; ?></h2>

            <?php if ($data->context) : ?>

                <?php

                    $categories = [
                        "api" => "API",
                        "clients" => "Klanten",
                        "contacts" => "Contacten",
                        "surveys" => "MVO-Wijzer",
                        "permissions" => "Profielen",
                        "users" => "Gebruikers",
                    ];

                    $actions = [
                        "view",
                        "create",
                        "update",
                        "delete"
                    ];

                ?>

                <?php if ($data->permission) : ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col" class="text-center">Lezen</th>
                            <th scope="col" class="text-center">Aanmaken</th>
                            <th scope="col" class="text-center">Wijzigen</th>
                            <th scope="col" class="text-center">Verwijderen</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($data->permission as $permission) : ?>
                        <?php $permissionSets = json_decode($permission->permissions); ?>
                        <?php foreach ($categories as $key => $value) : ?>
                            <tr>
                                <td>
                                    <?php echo $value; ?>
                                </td>
                                <?php foreach ($actions as $action) : ?>
                                    <?php $property = $action . '_' . $key; ?>
                                    <td class="text-center">
                                    <?php 
                                        if (isset($permissionSets->$key->$property)) {
                                            echo $permissionSets->$key->$property ? '<i class="fas fa-check"></i>' : '';
                                        } else {
                                            echo '';
                                        }
                                    ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>

            <?php else : ?>

                <div class="alert alert-warning" role="alert">
                    Om de rechten van een profiel te bekijken of aan te passen, moet je een profiel selecteren.
                </div>

            <?php endif; ?>

        </div>
        <?php endforeach; ?>
    <?php endif; ?>

<?php include 'views/partials/footers/footer.php'; ?>