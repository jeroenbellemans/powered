<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Profielen</h1>
            <p>Via deze pagina is het mogelijk om per beschikbaar profiel er de rechten van te beheren. In de onderstaande lijst staan alle beschikbare profielen.</p>

            <h2 class="mt-5">Overzicht profielen</h2>
            
            <?php if ($data->permissions) : ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Profiel</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($data->permissions as $permission) : ?>
                <tr>
                    <td>
                        <?php echo Routes::build('admin/permissions/view/' . $permission->id, $permission->label, array("class" => "")); ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>