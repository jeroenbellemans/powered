<?php
    if (form_posted()) {
        $postData = new \StdClass();
        $postData->permissionId = $data->context;
        $postData->data = $_POST;

        $actions = [
            "view",
            "create",
            "update",
            "delete"
        ];

        if (isset($_POST['save-permission'])) {
            $data->instance->updatePermissionsForRole($postData);
        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

<?php if ($data->permission) : ?>
    <?php foreach ($data->permission as $permission) : ?>

        <div class="col-md-9">
            <h1>Rechten</h1>
            <p>Op deze pagina kan je de rechten van het geselecteerde profiel wijzigen.</p>
            
            <h2 class="mt-5"><?php echo $permission->label; ?></h2>

            <?php if ($data->context) : ?>

                <?php

                    $categories = [
                        "api" => "API",
                        "clients" => "Klanten",
                        "contacts" => "Contacten",
                        "surveys" => "MVO-Wijzer",
                        "permissions" => "Profielen",
                        "users" => "Gebruikers",
                    ];

                    $actions = [
                        "view",
                        "create",
                        "update",
                        "delete"
                    ];

                ?>
                <form action="" method="post">
                
                    <?php if ($data->permission) : ?>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" class="text-center">Lezen</th>
                                <th scope="col" class="text-center">Aanmaken</th>
                                <th scope="col" class="text-center">Wijzigen</th>
                                <th scope="col" class="text-center">Verwijderen</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($data->permission as $permission) : ?>
                            <?php $permissionSets = json_decode($permission->permissions); ?>
                            <?php foreach ($categories as $key => $value) : ?>
                            <tr>
                                <td>
                                    <?php echo $value; ?> <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $key; ?>"/>
                                </td>
                                <?php foreach ($actions as $action) : ?>
                                <?php $property = $action . '_' . $key; ?>
                                <td class="text-center">
                                <?php
                                if (isset($permissionSets->$key->$property)) {
                                    echo ($permissionSets->$key->$property) ? '<input type="checkbox" name="' . $property . '" checked="checked" value="1" />' : '<input type="checkbox" name="' . $property . '" value="0"/>';
                                } else {
                                    echo '<input type="checkbox" name="' . $property . '" value="0"/>';
                                }
                                ?>
                                </td>

                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif; ?>
                    
                <button type="submit" name="save-permission" class="btn btn-primary btn-highlight-link mt-5">Rechten opslaan <i class="fas fa-save ml-3"></i></button>
                <?php echo Routes::build('admin/permissions/view/' . $data->context, 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                </form>

            <?php else : ?>

                <div class="alert alert-warning" role="alert">
                    Om de rechten van een profiel te bekijken of aan te passen, moet je een profiel selecteren.
                </div>

            <?php endif; ?>

        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php include 'views/partials/footers/footer.php'; ?>