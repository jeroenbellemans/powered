<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1>Gebruikers</h1>
            <p>Hier kan u een overzicht terug vinden van alle gebruikers die beschikbaar zijn op het platform. Om snel te zoeken kan u via de zoekfunctie een bepaalde naam of e-mail adres opzoeken.</p>

            <h2 class="mt-5">Overzicht gebruikers<span class="float-right" style="margin-top: -2px;"><?php echo Routes::build('admin/users/create', 'Gebruiker toevoegen <i class="fas fa-plus ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link small")); ?></span></h2>

            <?php if ($data->users) : ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Naam</th>
                        <th scope="col">E-mail</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($data->users as $user) : ?>
                <tr>
                    <td>
                    <?php if ($data->guardian->passportCheck('users@update_users', $data->accountInfo->permission->id)): ?>
                        <?php echo Routes::build('admin/users/view/' . $user->id, $user->firstname . ' ' . $user->insertion . ' ' . $user->lastname); ?>
                    <?php else : ?>
                        <?php echo $user->firstname . ' ' . $user->insertion . ' ' . $user->lastname; ?>
                    <?php endif; ?>
                    </td>
                    <td>
                        <?php echo $user->email; ?>
                    </td>
                    <td align="right">
                        <i class="fas fa-trash-alt delete" onclick="deleteUser('<?php echo $user->id; ?>');"></i>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>