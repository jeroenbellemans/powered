<?php include 'views/partials/headers/header.php'; ?>

<?php // debug_print($data); ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1><?php echo $data->userInfo->personal->firstname; ?> <?php echo $data->userInfo->personal->insertion; ?> <?php echo $data->userInfo->personal->lastname; ?></h1>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Persoonlijke gegevens<?php if ($data->guardian->passportCheck('users@update_users', $data->accountInfo->permission->id)): ?><span class="float-right" style="margin-top: -2px;"><?php echo Routes::build('admin/users/edit/' . $data->userInfo->personal->id, 'Wijzigen <i class="fas fa-edit ml-3"></i>', array("class" => "btn btn-primary btn-highlight-link small")); ?></span><?php endif; ?></h2>
                    <p>
                        <?php echo $data->userInfo->personal->email; ?><br />
                        <?php echo $data->userInfo->personal->phone; ?>
                    </p>
                    <p>
                        <?php echo $data->userInfo->personal->street; ?> <?php echo $data->userInfo->personal->number; ?><br />
                        <?php echo $data->userInfo->personal->zipcode; ?> <?php echo $data->userInfo->personal->city; ?><br />
                        <?php echo $data->userInfo->personal->country; ?>
                    </p>
                </div>
                <div class="col-md-12">
                    <h2>Rol</h2>
                    <p><?php echo $data->userInfo->permission->label; ?></p>
                </div>
                <div class="col-md-12 mt-5">
                    <h2>Support</h2>
                    <div class="row">
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="role">Account activatielink</label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" id="copy-text" class="form-control form-control-sm" name="text" value="http://tool.power-ed.nl/activate/account/<?php echo $data->userInfo->personal->id; ?>/<?php echo $data->userInfo->personal->token; ?>">
                                <button class="btn btn-primary btn-highlight-link mt-5" onclick="copyUtility(this);">Activatielink kopiëren <i class="fas fa-copy ml-3"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include 'views/partials/footers/footer.php'; ?>