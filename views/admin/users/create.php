<?php

    $response = false;
    $response_message;

    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['create-contact'])) {

            $postData->email = $_POST['email'];
            $postData->firstname = $_POST['firstname'];
            $postData->insertion = $_POST['insertion'];
            $postData->lastname = $_POST['lastname'];
            $postData->parentId = $_SESSION['id'];

            $result = $data->instance->createAccount($postData);

            if ($result) {
                $response = true;
                $response_message = $result["response"];
            }

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>

    <div class="container">
        <div class="row mt-4">
            <div class="col-md-3">
                <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
            </div>

            <div class="col-md-9">
                <h1>Gebruikers</h1>
                <p>Nadat u de betaling van een nieuwe klant heeft ontvangen, kan u via dit formulier een account aanmaken voor deze klant.</p>

                <?php if ($response) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $response_message; ?>
                </div>
                <?php endif; ?>

                <div class="row mt-5">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Naam</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="firstname" required="required" placeholder="Voornaam">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="insertion" placeholder="Voorvoegsel">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="lastname" required="required" placeholder="Achternaam">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">E-mail adres</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-sm" name="email" required="required" placeholder="E-mail adres">
                                        <button type="submit" name="create-contact" class="btn btn-primary btn-highlight-link mt-5">Gebruiker opslaan <i class="fas fa-save ml-3"></i></button>
                                        <?php echo Routes::build('admin/users/overview', 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

<?php include 'views/partials/footers/footer.php'; ?>