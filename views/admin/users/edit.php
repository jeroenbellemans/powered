<?php

    $response = false;
    $response_message;

    if (form_posted()) {
        $postData = new \StdClass();

        if (isset($_POST['update-permission'])) {

            $postData->role = $_POST['permission'];
            $postData->userId = $_POST['user'];

            $result = $data->instance->updatePermission($postData);

            if ($result) {
                $response = true;
                $response_message = $result["response"];
            }

        } else
        if (isset($_POST['update-user'])) {

            $postData->firstname = $_POST['firstname'];
            $postData->lastname = $_POST['lastname'];
            $postData->insertion = $_POST['insertion'];
            $postData->email = $_POST['email'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->zipcode = $_POST['zipcode'];
            $postData->country = $_POST['country'];
            $postData->userId = $_POST['user'];

            $result = $data->instance->updateUser($postData);

            if ($result) {
                $response = true;
                $response_message = $result["response"];
            }
        } else 
        if (isset($_POST['update-company'])) {

            $postData->businessName = $_POST['company-name'];
            $postData->kvkNumber = $_POST['kvk-number'];
            $postData->sbiCode = $_POST['sbi-code'];
            $postData->sbiCodeDescription = $_POST['sbi-code-description'];
            $postData->isMainBranch = $_POST['is-main-sbi'];
            $postData->code = $_POST['establishment-code'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->postalCode = $_POST['zipcode'];
            $postData->country = $_POST['country'];
            $postData->userId = $_POST['user'];
            $postData->companyId = (!empty($_POST['company-id'])) ? $_POST['company-id'] : false;

            $result = $data->instance->setCompanyDetails($postData);

            if ($result) {
                $response = true;
                $response_message = $result["response"];
            }

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/admin-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
            <h1><?php echo $data->userInfo->personal->firstname; ?> <?php echo $data->userInfo->personal->insertion; ?> <?php echo $data->userInfo->personal->lastname; ?></h1>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Toegangsrechten</h2>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Rol</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <select name="permission" class="form-control">
                                    <?php foreach ($data->permissions as $permission) : ?>
                                        <?php $selected = ($data->userInfo->permission->id == $permission->id) ? 'selected="selected"' : ''; ?>
                                        <option <?php echo $selected; ?> value="<?php echo $permission->id; ?>"><?php echo $permission->label; ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="user" value="<?php echo $data->userInfo->personal->id; ?>">
                                    <button type="submit" name="update-permission" class="btn btn-primary btn-highlight-link mt-5">Toegangsrechten wijzigen <i class="fas fa-save ml-3"></i></button>
                                    <?php echo Routes::build('admin/users/view/' . $data->userInfo->personal->id, 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                    <h2 class="mt-5">Persoonlijke gegevens</h2>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Naam</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="firstname" required="required" placeholder="Voornaam" value="<?php echo $data->userInfo->personal->firstname; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="insertion" placeholder="Voorvoegsel" value="<?php echo $data->userInfo->personal->insertion; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="lastname" required="required" placeholder="Achternaam" value="<?php echo $data->userInfo->personal->lastname; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">E-mail adres</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-sm" name="email" required="required" placeholder="E-mail adres" value="<?php echo $data->userInfo->personal->email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Straat + huisnummer</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="street" required="required" placeholder="Voornaam" value="<?php echo $data->userInfo->personal->street; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="number" required="required" placeholder="Achternaam" value="<?php echo $data->userInfo->personal->number; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Plaats + postcode</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="city" required="required" placeholder="Voornaam" value="<?php echo $data->userInfo->personal->city; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="zipcode" required="required" placeholder="Achternaam" value="<?php echo $data->userInfo->personal->zipcode; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Land</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-sm" name="country" required="required" placeholder="Voornaam" value="<?php echo $data->userInfo->personal->country; ?>">
                                    <input type="hidden" name="user" value="<?php echo $data->userInfo->personal->id; ?>">
                                    <button type="submit" name="update-user" class="btn btn-primary btn-highlight-link mt-5">Persoonlijke gegevens opslaan <i class="fas fa-save ml-3"></i></button>
                                    <?php echo Routes::build('admin/users/view/' . $data->userInfo->personal->id, 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                    <h2 class="mt-5">Bedrijfsgegevens</h2>
                    <?php
                            $companyid = ($data->companyDetails) ? $data->companyDetails[0]->id : '';
                            $companyName = ($data->companyDetails) ? $data->companyDetails[0]->company_name : '';
                            $kvknumber = ($data->companyDetails) ? $data->companyDetails[0]->kvk_number : '';
                            $sbicode = ($data->companyDetails) ? $data->companyDetails[0]->sbi_code : '';
                            $sbicodedescription = ($data->companyDetails) ? $data->companyDetails[0]->sbi_code_description : '';
                            $street = ($data->companyDetails) ? $data->companyDetails[0]->establishment_street : '';
                            $number = ($data->companyDetails) ? $data->companyDetails[0]->establishment_number : '';
                            $city = ($data->companyDetails) ? $data->companyDetails[0]->establishment_city : '';
                            $zipcode = ($data->companyDetails) ? $data->companyDetails[0]->establishment_pc : '';
                            $country = ($data->companyDetails) ? $data->companyDetails[0]->establishment_country : '';
                            $ismainsbi = ($data->companyDetails) ? ($data->companyDetails[0]->main_branch == 1) ? 'true' : 'false' : '';
                            $establishmentcode = ($data->companyDetails) ? $data->companyDetails[0]->establishment_code : '';
                    ?>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">KvK nummer</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control form-control-sm" name="kvk" placeholder="KvK nummer">
                            </div>
                            <div class="col-md-3 text-right">
                                <button type="button" name="search-kvk" class="btn btn-primary btn-highlight-link small" style="width: 100%;" onclick="startSearch();">Start met zoeken <i class="fas fa-search ml-3"></i></button>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="mt-1 search-result"></div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Bedrijfsnaam</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="companyName" name="company-name" class="form-control form-control-sm" value="<?php echo $companyName; ?>">
                                </div>
                            </div>

                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">KvK nummer</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="kvkNumber" name="kvk-number" class="form-control form-control-sm" value="<?php echo $kvknumber; ?>">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">SBI code</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="sbiCode" name="sbi-code" class="form-control form-control-sm" value="<?php echo $sbicode; ?>">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">SBI code omschrijving</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="sbiCodeDescription" name="sbi-code-description" class="form-control form-control-sm" value="<?php echo $sbicodedescription; ?>">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Straat + huisnummer</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" id="street" name="street" class="form-control form-control-sm" value="<?php echo $street; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" id="number" name="number" class="form-control form-control-sm" value="<?php echo $number; ?>">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Plaats + postcode</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" id="city" name="city" class="form-control form-control-sm" value="<?php echo $city; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" id="zipCode" name="zipcode" class="form-control form-control-sm" value="<?php echo $zipcode; ?>">
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <div class="form-group">
                                    <label for="role">Land</label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" id="country" name="country" class="form-control form-control-sm" value="<?php echo $country; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="hidden" name="user" value="<?php echo $data->userInfo->personal->id; ?>">
                                    <input type="hidden" id="companyid" name="company-id" value="<?php echo $companyid; ?>" />
                                    <input type="hidden" id="ismainsbi" name="is-main-sbi" value="<?php echo $ismainsbi; ?>" />
                                    <input type="hidden" id="establishmentCode" name="establishment-code" value="<?php echo $establishmentcode; ?>" />
                                    <button type="submit" name="update-company" class="btn btn-primary btn-highlight-link mt-5">Bedrijfsgegevens opslaan <i class="fas fa-save ml-3"></i></button>
                                    <?php echo Routes::build('admin/users/view/' . $data->userInfo->personal->id, 'Annuleren', array("class" => "btn btn-primary btn-cancel-link mt-5")); ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>