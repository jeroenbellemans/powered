<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo config_get_title(); ?></title>
        <link rel="icon" type="image/png" href="<?php config_get_root(); ?>/resources/images/favicon.png">
        <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo config_get_root(); ?>resources/style/css/pinker.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row mt-10">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-4 login-bg">
                            <div class="vertical-centered text-center">
                                <h1>Welkom terug</h2>
                                <p class="highlight">Meld je aan om een betere coach te worden</p>
                                <button class="btn btn-default" onclick="login();">Aanmelden</button>
                            </div>
                        </div>
                        <div class="col-md-8 action-bg">
                            <div class="vertical-centered">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="register-form">
                                            <div class="brand text-center">
                                                <span class="glyphicon glyphicon-fire font-lg"></span><br />
                                                Powertool
                                            </div>
                                            <h1 class="text-center">Registreer</h1>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-user"></span>
                                                    </span>
                                                    <input name="email" type="email" id="email" class="form-control form-control-lg" placeholder="E-mail adres" autocomplete="off" onclick="clearErrors();">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-lock"></span>
                                                    </span>
                                                    <input name="password" type="password" id="password" class="form-control form-control-lg" placeholder="Wachtwoord" onclick="clearErrors();">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-repeat"></span>
                                                    </span>
                                                    <input name="repeat-password" type="password" id="repeat-password" class="form-control form-control-lg" placeholder="Herhaal wachtwoord" onclick="clearErrors();">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                            <div class="hidden" id="response-message"></div>
                                            <div class="text-center">
                                                <button class="btn btn-default call-to-action login-button" onclick="submitRegisterForm();">Registreer</button>
                                            </div>
                                        </div>
                                        <div class="waiting">
                                            <div class="loading"></div>
                                        </div>
                                        <div class="thanks text-center">
                                            <span class="glyphicon glyphicon-ok"></span>
                                            <h2>Bedankt</h2>
                                            <p>Je kan nu inloggen en van de gratis 14 dagen durende proefperiode gebruik maken.</p>
                                        </div>
                                        <div class="error text-center">
                                            <span class="glyphicon glyphicon-ban-circle"></span>
                                            <h2>Oeps</h2>
                                            <p>Er is een fout opgetreden. Probeer opnieuw of neem contact op met de helpdesk.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript">

        function clearErrors() {
            $(".invalid-feedback").each(function() {
                $(this).hide().removeClass("invalid-feedback").prev().removeClass("has-error");
            });
        }

        function login() {
            window.location = '/login';
        }

        function submitRegisterForm() {

            $("#response-message").show();
            
            $(".invalid-feedback").each(function() {
                $(this).hide().removeClass("invalid-feedback").prev().removeClass("has-error");
            });

            var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
            var error = false;

            var email = $("#email").val(),
                password = $("#password").val();
                rpassword = $("#repeat-password").val();

            if (email.trim() == '') {
                $("input[name='email']").addClass("has-error").parent().next().addClass('invalid-feedback').html('Gelieve uw e-mailadres in te vullen').show();
                error = true;
            } else {
                if (!email_regex.test(email)) {
                    $("input[name='email']").addClass("has-error").parent().next().addClass('invalid-feedback').html('Dit is een ongeldig e-mailadres').show();
                    error = true;
                }
            }

            if (password.trim() == '') {
                $("input[name='password']").addClass("has-error").parent().next().addClass('invalid-feedback').html('Gelieve uw wachtwoord in te vullen').show();
                error = true;
            }

            if (rpassword.trim() == '') {
                $("input[name='repeat-password']").addClass("has-error").parent().next().addClass('invalid-feedback').html('Gelieve uw wachtwoord te herhalen').show();
                error = true;
            } else {
                if (password != rpassword) {
                    $("input[name='repeat-password']").addClass("has-error").parent().next().addClass('invalid-feedback').html('Het wachtwoord klopt niet').show();
                    error = true;
                }
            }

            if (!error) {
                $(".register-form").hide();
                $(".waiting").show();

                doRegister(email, password);
            }
        }

        function doRegister(email, password) {
            
            var formData = formData || {};

            formData.email = email;
            formData.password = password;

            $.ajax({
                'type': 'POST',
                'url': '/resources/js/ajax/register.php',
                'data': formData,
                success: function(data) {

                    var response = JSON.parse(data);
                    console.log(response);

                    if (response.success) {
                        $(".waiting").hide();
                        $(".thanks").show();
                    } else {
                        $(".waiting").hide();
                        $(".error").show();
                    }
                }
            });
        }

        </script>

    </body>
</html>