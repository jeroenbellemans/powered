<?php
    if (form_posted()) {
        $postData = new \StdClass();
        $postData->userId = $data->context;

        if (isset($_POST['edit-personal'])) {

            $postData->email = $_POST['email'];
            $postData->firstname = $_POST['firstname'];
            $postData->insertion = $_POST['insertion'];
            $postData->lastname = $_POST['lastname'];
            $postData->phone = $_POST['phone'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->zipcode = $_POST['zipcode'];
            $postData->country = $_POST['country'];

            $admin = $data->instance->update('personal', $postData);

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>

<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/myaccount-side-navigation.php'; ?>
        </div>
        <div class="col-md-9">
        
        <?php if ($data->userInfo) : ?>
            <h1>Mijn gegevens</h1>
            <p>We staan graag met u in contact. Daarom hebben we wat gegevens van u nodig. Deze gegevens kunnen natuurlijk afwijken van de gegevens van uw bedrijf.</p>
            
            <?php if ($data->userInfo->personal) : ?>
                <div class="row mt-5">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Naam</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="firstname" required="required" placeholder="Voornaam" value="<?php echo $data->userInfo->personal->firstname; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="insertion" placeholder="Voorvoegsel" value="<?php echo $data->userInfo->personal->insertion; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="lastname" required="required" placeholder="Achternaam" value="<?php echo $data->userInfo->personal->lastname; ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">E-mail adres</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-sm" name="email" required="required" placeholder="E-mail adres" value="<?php echo $data->userInfo->personal->email; ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Telefoonnummer</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="phone" class="form-control form-control-sm" name="phone" required="required" placeholder="Telefoonnummer" value="<?php echo $data->userInfo->personal->phone; ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Straat + huisnummer</label>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="street" required="required" placeholder="Straat" value="<?php echo $data->userInfo->personal->street; ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="number" required="required" placeholder="Huisnummer" value="<?php echo $data->userInfo->personal->number; ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Plaats + postcode</label>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="city" required="required" placeholder="Plaats" value="<?php echo $data->userInfo->personal->city; ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="zipcode" required="required" placeholder="Postcode" value="<?php echo $data->userInfo->personal->zipcode; ?>">
                                    </div>
                                </div>

                                <div class="col-md-3 text-right">
                                    <div class="form-group">
                                        <label for="role">Land</label>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-sm" name="country" required="required" placeholder="Land" value="<?php echo $data->userInfo->personal->country; ?>">
                                        <button type="submit" name="edit-personal" class="btn btn-primary btn-highlight-link mt-5">Gegevens opslaan <i class="fas fa-save ml-3"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>