<?php
    if (form_posted()) {
        $postData = new \StdClass();
        $postData->userId = $data->context;

        if (isset($_POST['edit-role'])) {
            $postData->role = $_POST['role'];

            $admin = $data->instance->update('role', $postData);

        } else
        if (isset($_POST['edit-personal'])) {

            $postData->email = $_POST['email'];
            $postData->firstname = $_POST['firstname'];
            $postData->insertion = $_POST['insertion'];
            $postData->lastname = $_POST['lastname'];
            $postData->birthdate = $_POST['birthdate'];
            $postData->street = $_POST['street'];
            $postData->number = $_POST['number'];
            $postData->city = $_POST['city'];
            $postData->zipcode = $_POST['zipcode'];
            $postData->country = $_POST['country'];

            $admin = $data->instance->update('personal', $postData);

        }

    }
?>
<?php include 'views/partials/headers/header.php'; ?>
<?php debug_print($data->userInfo->personal); ?>s
<div class="container">
    <div class="row mt-4">
        <div class="col-md-3">
            <?php include 'views/partials/navigations/myaccount-side-navigation.php'; ?>
        </div>

        <div class="col-md-9">
        <?php if ($data->userInfo) : ?>
            <h1>Gebruikers &raquo; <?php echo $data->userInfo->personal->firstname; ?> <?php echo $data->userInfo->personal->insertion; ?> <?php echo $data->userInfo->personal->lastname; ?></h1>


            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active tab-content-block" id="personal" role="tabpanel" aria-labelledby="home-tab">            
                <?php if ($data->userInfo->personal) : ?>
                    <div class="row">
                        <div class="col-md-8">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="role">Voornaam</label>
                                            <input type="text" class="form-control form-control-sm" name="firstname" required="required" value="<?php echo $data->userInfo->personal->firstname; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="role">Voorvoegsel</label>
                                            <input type="text" class="form-control form-control-sm" name="insertion" value="<?php echo $data->userInfo->personal->insertion; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="role">Achternaam</label>
                                            <input type="text" class="form-control form-control-sm" name="lastname" required="required" value="<?php echo $data->userInfo->personal->lastname; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role">E-mail adres</label>
                                            <input type="email" class="form-control form-control-sm" name="email" required="required" value="<?php echo $data->userInfo->personal->email; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="role">Geboortedatum</label>
                                            <input type="date" class="form-control form-control-sm" name="birthdate" required="required" value="<?php echo $data->userInfo->personal->birthdate; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 30px;">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label for="role">Straat</label>
                                            <input type="text" class="form-control form-control-sm" name="street" required="required" value="<?php echo $data->userInfo->personal->street; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="role">Bus</label>
                                            <input type="text" class="form-control form-control-sm" name="number" required="required" value="<?php echo $data->userInfo->personal->number; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="role">Plaats</label>
                                            <input type="text" class="form-control form-control-sm" name="city" required="required" value="<?php echo $data->userInfo->personal->city; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="role">Postcode</label>
                                            <input type="text" class="form-control form-control-sm" name="zipcode" required="required" value="<?php echo $data->userInfo->personal->zipcode; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="role">Land</label>
                                            <input type="text" class="form-control form-control-sm" name="country" required="required" value="<?php echo $data->userInfo->personal->country; ?>">
                                        </div>
                                        <button type="submit" name="edit-personal" class="btn btn-primary">Opslaan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php endif; ?>
                </div>

                <div class="tab-pane fade tab-content-block" id="company" role="tabpanel" aria-labelledby="profile-tab">
                    <strong>N/A tot duidelijkheid over API.</strong>
                </div>
                
                <div class="tab-pane fade tab-content-block" id="role" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="role">Rol toewijzen</label>
                                    <select class="form-control form-control-sm" name="role">
                                    <?php if ($data->permissions) : ?>
                                        <?php foreach ($data->permissions as $permission) : ?>
                                        <?php $selected = ($permission->id == $data->userInfo->permission->id) ? 'selected="selected"' : ''; ?>
                                        <option value="<?php echo $permission->id; ?>" <?php echo $selected; ?>><?php echo $permission->label; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </select>
                                    <small id="emailHelp" class="form-text text-muted">Een rol heeft impact op pagina's die de gebruiker kan zien en acties die de gebruiker kan ondernemen.</small>
                                </div>
                                <button type="submit" name="edit-role" class="btn btn-primary">Opslaan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>

<?php include 'views/partials/footers/footer.php'; ?>