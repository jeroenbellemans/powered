<?php include 'views/partials/headers/login-header.php'; ?>

    <div class="container"> 
        <div class="row mt-5">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
                <form method="post">
                    <a href="/"><img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo"></a>
                    <div class="scan-highlight">
                        <h1 class="login">Gelukt!</h1>
                        <p class="text-left">We hebben uw verzoek tot het herstellen van uw wachtwoord goed ontvangen.</p>
                        <p class="text-left">Er is zonet een e-mail verzonden naar het opgegeven e-mail adres, met daarin verdere instructies.</p>
                        <p class="mb-5 text-left">U kan deze pagina nu sluiten.</p>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <?php echo Routes::build('', 'Aanmelden', array("class" => "link")); ?>
            </div>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>