<?php include 'views/partials/headers/login-header.php'; ?>

    <div class="container"> 
        <div class="row mt-5">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
                <form method="post">
                    <div class="scan-highlight">
                        <a href="/"><img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo"></a>
                        <p class="text-left">Uw nieuwe wachtwoord is succesvol opgeslagen. Deze pagina kan nu gesloten worden.</p>
                        <p class="text-left">Om in te loggen kan u op <?php echo Routes::build('', 'deze link'); ?> klikken.</p>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <?php echo Routes::build('', 'Aanmelden', array("class" => "link")); ?>
            </div>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>