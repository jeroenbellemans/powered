<?php

    if (form_posted()) {

        $postData = (object) $_POST;
        $data = new StdClass();

        if (!isset($postData->password) || !isset($postData->rpassword)) {
            $data->message = 'Je moet de velden invullen.';
            $data->success = false;
            $data->retry = true;
        } else
        if ($postData->password != $postData->rpassword) {
            $data->message = 'Deze wachtwoorden blijken niet te kloppen.';
            $data->success = false;
            $data->retry = true;
        } else {
            $account = new \App\controllers\AccountController();
            $data = $account->setPassword($postData, '');
        }

    }

?> 

<?php include 'views/partials/headers/login-header.php'; ?>

<div class="container"> 
    <div class="row mt-5">
        <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <form method="post">
                <img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo">
                <?php 
                    if ($data->success) :
                        $color = 'alert-success';
                    else :
                        $color = 'alert-danger';
                    endif;
                ?>
                <div class="alert <?php echo $color; ?>" role="alert">
                    <?php echo $data->message; ?>
                </div>
                <div class="scan-highlight">
                    <?php if ($data->success || $data->retry) : ?>
                    <h1 class="login">Stel uw wachtwoord in</h1>
                    <p class="login mb-5">U kan hier uw wachtwoord van uw net geactiveerde account instellen.</p>
                    <label for="inputPassword" class="sr-only">Herhaal wachtwoord</label>
                    <input class="mb-2 form-control" type="password" name="password" class="form-control" placeholder="Wachtwoord" required>
                    <label for="inputrPassword" class="sr-only">Bevestig wachtwoord</label>
                    <input class="mb-2 form-control" type="password" name="rpassword" class="form-control" placeholder="Bevestig wachtwoord" required>
                    <input type="hidden" name="id" value="<?php echo $data->userId; ?>" />
                    <button class="btn btn-md btn-primary btn-block btn-highlight-link" type="submit">Wijzigingen Opslaan</button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
        <?php echo Routes::build('', 'Aanmelden', array("class" => "link")); ?>
        </div>
    </div>
</div>

<?php include 'views/partials/footers/footer.php'; ?>