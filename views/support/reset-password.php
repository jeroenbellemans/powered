<?php
    $response = new StdClass();
    $response->success = true;

    if (form_posted()) {

        $postData = (object) $_POST;

        $request = $_SERVER['REQUEST_URI'];
        $request = explode("/", $request);

        $postData->id = isset($request[3]) ? $request[3] : false;
        $postData->token = isset($request[4]) ? $request[4] : false;

        if (!isset($request[3]) || !isset($request[4])) {
            $response->message = 'Deze request kan niet worden verwerkt.';
            $response->success = false;
        } else
        if (!isset($postData->password) || !isset($postData->rpassword)) {
            $response->message = 'Gelieve beide velden in te vullen';
            $response->success = false;
        } else
        if ($postData->password != $postData->rpassword) {
            $response->message = 'De wachtwoorden kloppen niet.';
            $response->success = false;
        }
        else {
            $account = new \App\controllers\AccountController();
            $data = $account->submitResetPassword($postData);

            if (!$data) {
                $response->message = 'Er is iets fout gelopen, probeer opnieuw of contacteer een administrator';
                $response->success = false;
            } 
        }

    }

?> 

<?php include 'views/partials/headers/login-header.php'; ?>

    <div class="container"> 
        <div class="row mt-5">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
                <form method="post">
                    <a href="/"><img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo"></a>
                    <?php if (!$response->success) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $response->message; ?>
                    </div>  
                    <?php endif; ?>
                    <div class="scan-highlight">
                        <h1 class="login">Stel uw wachtwoord in</h1>
                        <p class="login mb-5">Gelieve uw nieuwe wachtwoord in te voeren.</p>
                        <label for="password" class="sr-only">Nieuw wachtwoord</label>
                        <input class="mb-1 form-control" type="password" name="password" class="form-control" placeholder="Wachtwoord" required>
                        <label for="rpassword" class="sr-only">Wachtwoord bevestigen</label>
                        <input class="mb-3 form-control" type="password" name="rpassword" class="form-control" placeholder="Wachtwoord bevestigen" required>
                        <button class="btn btn-md btn-primary btn-block btn-highlight-link" type="submit">Wijzigingen opslaan</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <?php echo Routes::build('', 'Aanmelden', array("class" => "link")); ?>
            </div>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>