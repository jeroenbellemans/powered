<?php
    $response = new StdClass();
    $response->success = true;

    if (form_posted()) {

        $postData = (object) $_POST;

        if (!filter_var($postData->email, FILTER_VALIDATE_EMAIL)) {
            $response->message = 'This is an invalid e-mail address';
            $response->success = false;
        } else 
        if (!isset($postData->email)) {
            $response->message = 'Gelieve het veld in te vullen aub.';
            $response->success = false;
        }
        else {
            $account = new \App\controllers\AccountController();
            $data = $account->sendPasswordReset($postData);

            if (!$data) {
                $response->message = 'Het opgegeven e-mail adres is niet gekend in het systeem.';
                $response->success = false;
            } 
        }

    }

?> 

<?php include 'views/partials/headers/login-header.php'; ?>

    <div class="container"> 
        <div class="row mt-5">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
                <img class="mb-5" src="<?php config_get_root(); ?>/resources/images/power-logo.png" alt="Power ED Logo">
                <?php if (!$response->success) : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $response->message; ?>
                </div>  
                <?php endif; ?>
                <div class="scan-highlight">
                    <h1 class="login">Wachtwoord vergeten</h1>
                    <p class="login mb-5">Gelieve uw e-mail adres in te voeren om uw wachtwoord te herstellen.</p>
                    <div class="login-box">
                        <form method="post">
                            <label for="inputEmail" class="sr-only">E-mail adres</label>
                            <input class="mb-3 form-control" type="email" name="email" class="form-control" placeholder="E-mail adres" required>
                            <button class="btn btn-md btn-primary btn-block btn-highlight-link" type="submit">Verzend e-mail</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="offset-md-4 col-md-4 offset-sm-2 col-sm-8 text-center">
            <?php echo Routes::build('', 'Aanmelden', array("class" => "link")); ?>
            </div>
        </div>
    </div>

<?php include 'views/partials/footers/footer.php'; ?>