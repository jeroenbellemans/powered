<?php
    session_start();

    include '../../../app/handlers/Api.php';
    include '../../../app/handlers/Database.php';
    include '../../../app/handlers/Views.php';
    include '../../../app/handlers/Guardian.php';
    include '../../../app/handlers/Mailer.php';
    include '../../../app/controllers/MyaccountController.php';
    include '../../../app/controllers/ErrorsController.php';
    include '../../../app/models/User_model.php';
    include '../../../app/models/Permission_model.php';
    include '../../../vendor/phpmailer/phpmailer/src/PHPMailer.php';

    if (isset($_POST)) {

        $input = $_POST['fieldValue'];
        $field = $_POST['fieldName'];
        $companyId = $_POST['companyId'];

        $myaccount = new \App\controllers\MyaccountController();
        $companyDetails = $myaccount->saveCompanyDetailsField($input, $field, $companyId);

        if ($companyDetails) {

            echo json_encode(array(
                "success"   =>  true,
                "result"    =>  $companyDetails,
                "input"     =>  $input,
                "field"     =>  $field
            ));

        } else {
            echo json_encode(array(
                "success"   =>  false,
                "result"    =>  $companyDetails,
                "input"     =>  $input,
                "field"     =>  $field
            ));
        }

    }

?>