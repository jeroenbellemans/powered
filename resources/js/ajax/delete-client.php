<?php
    include '../../../app/handlers/Database.php';
    include '../../../app/handlers/Views.php';
    include '../../../app/controllers/AccountController.php';
    include '../../../app/controllers/MyaccountController.php';
    include '../../../app/controllers/ErrorsController.php';
    include '../../../app/models/User_model.php';
    include '../../../app/models/Permission_model.php';
    include '../../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
    include '../../../vendor/phpmailer/phpmailer/src/Exception.php';

    if (isset($_POST)) {

        $input = $_POST['id'];

        $account = new \App\controllers\AccountController();
        $account = $account->deleteClient($input);

        if ($account) {

            echo json_encode(array(
                "success"   =>  true,
                "result"    =>  $account,
                "input"     =>  $input
            ));

        } else {
            echo json_encode(array(
                "success"   =>  false,
                "result"    =>  $account,
                "input"     =>  $input
            ));
        }

    }

?>