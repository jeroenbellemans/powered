<?php

    namespace App\handlers;
	
	class Redirect {

		public static function location($url) {
			header("Location: " . config_get_root() . $url);
			exit;
		}

		public static function reload() {
			header("Location: " . $_SERVER['PHP_SELF']);
			exit;
		}

	}