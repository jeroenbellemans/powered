<?php

    namespace App\handlers;

    class Request {

        public $request;
        public $user_id;

        public function __construct($request = null) {
            
            $request = explode('?', $request);
            $request = $request[0];
            $request = explode("/", $request, 2);
            $request = $request[1];
            $request = trim($request, '/');
            $request = explode('/', $request);

            $this->request = $request;
            $this->user_id = (isset($_SESSION['id'])) ? $_SESSION['id'] : false;

        }

        public function handle() {

            if (isset($this->request[0]) && (!empty($this->request[0]))) {
                    
                $controller = ucfirst(str_replace("-", "", $this->request[0])) . 'Controller';
                $controller = '\\App\\controllers\\' . $controller;
                $controller = new $controller($this->user_id, $this->request);
                
                if (isset($this->request[1])) {

                    $controller->{str_replace("-", "_", $this->request[1])}();

                } else {
                    $controller->index();
                }
            } else {

                $controller = new \App\controllers\LoginController($this->user_id, $this->request);
                $controller->index();

            }

        }

    }