<?php

    namespace App\handlers;

    define('API_KEY', 'l7xxa3a67661919e4c4f874df99f16302c73');
	
	class Api {

        public $kvkSearch;
        public $profileSearch;

        private function doCurl ($endpoint) {
            $ch = curl_init();

            // TradeName
            // @Todo
            
            // Fix 60 SSL Error
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            curl_setopt($ch, CURLOPT_URL, $endpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

            $response = curl_exec($ch);
            $error = (!$response) ?  curl_errno($ch) .': ' . curl_error($ch) : false;
            curl_close($ch);

            return ($response) ? json_decode($response) : $error;
        }

        public function searchByKvK ($kvk) {
            $endpoint = 'https://api.kvk.nl/api/v1/zoeken?kvkNummer=' . $kvk . '&user_key=' . API_KEY;
            return $this->doCurl($endpoint);

            // $endpoint = 'https://api.kvk.nl/api/v1/basisprofielen/' . $kvk . '/?user_key=' . API_KEY;
        }

        public function searchProfileForKvK ($url) {
            $endpoint = $url . '/?user_key=' . API_KEY;
            return $this->doCurl($endpoint);
        }

	}