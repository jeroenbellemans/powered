<?php

    namespace App\handlers;

    class Validation {

        public $message;

        public function is_valid($validators = array()) {
            foreach ($_POST as $post_name => $post_value) {
                if (isset($validators[$post_name])) {
                    $rules = explode("|", $validators[$post_name]);
                    foreach ($rules as $rule) {
                        $options = explode(":", $rule);

                        switch ($options[0]) {
                            case 'required':
                                $this->validate_required_field($post_name, $post_value);
                                break;

                            case 'min':
                                $this->validate_min_chars($post_name, $post_value, $options[1]);
                                break;

                            case 'max':
                                $this->validate_max_chars($post_name, $post_value, $options[1]);
                                break;

                            case 'email':
                                $this->validate_email_type($post_name, $post_value);
                                break;
                            
                            default:
                                die("The validation rule <strong>" . $options[0] . "</strong> does not exists.");
                                break;
                        }
                        unset($options);
                    }
                }
            }
            return true;
        }

        public function get_message() {
            return $this->message;
        }

        private function available_validation() {
            return [
                "required",
                "min",
                "max",
                "email"
            ];
        }

        private function validate_required_field($input, $value) {
            if (!$value || empty($value) || !trim($value)) {
                $this->message = 'The field <strong>' . $input . '</strong> is a required field!';
                return false;
            }
            return true;
        }

        private function validate_min_chars($input, $value, $amount) {
            if(strlen($value) < $amount) {
                $this->message = 'The field <strong>' . $input . '</strong> requires a minumum of <strong>' . $amount . '</strong> characters for ' . $value . '!';
                return false;
            }
            return true;
        }

        private function validate_max_chars($input, $value, $amount) {
            if (strlen($value) > $amount) {
                $this->message = 'The field <strong>' . $input . '</strong> requires a maximum of <strong>' . $amount . '</strong> characters for ' . $value . '!';
                return false;
            }
            return true;
        }

        private function validate_email_type($input, $value) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->message = 'The field <strong>' . $input . '</strong> is not a valid e-mail!';
                return false;
            }
            return true;
        }
    }