<?php

    namespace App\handlers;

    class Views {

        public function render($path, $data = false, $component = false) {
            
            $data = $data;
            $component = (isset($component)) ? $component : false;
            require_once $path;

        }

    }