<?php

    namespace App\handlers;

    use \App\controllers\ErrorsController as errorsController;

    class Database {

        private $exceptionHandler;
        
        private $env = 'live';
        private $environments = [
            "local"     =>  [
                "username"  =>  "root",
                "password"  =>  "bzr001",
                "database"  =>  "db347325_tool", //roaming_souls_c
                "host"      =>  "localhost"
            ],
            "staging"   =>  [
                "username"  =>  "roaming_souls_c",
                "password"  =>  "BzQRfxzL",
                "database"  =>  "roaming_souls_c",
                "host"      =>  "roaming-souls.com.mysql"
            ],
            "live"      =>  [
                "username"  =>  "u347325_tool",
                "password"  =>  "vGhouLjz@4q",
                "database"  =>  "db347325_tool",
                "host"      =>  "mysql1443int.cp.hostnet.nl"
            ]
        ];

        public $db;

        public function __construct() {

            $this->exceptionHandler = new errorsController();

            try {
                $connection = new \PDO(
                    'mysql:dbname=' . $this->environments[$this->env]["database"] . ';host=' . $this->environments[$this->env]["host"] . ';charset=UTF8', 
                    $this->environments[$this->env]["username"], 
                    $this->environments[$this->env]["password"]
                );

                $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

                $this->db = $connection;

            } catch(\PDOException $e) {
                $this->exceptionHandler->logError($e, '500');
                Redirect::location('errors/internal');
            }

        }

        public function __clone() {}
            
        public function getConnection() {
            return $this->db;
        }

        public function get_results($sql, $params = array(), $type = null) {
            
            $db = self::getConnection();

            switch ($type) {
                case 'array':
                    $type = \PDO::FETCH_ASSOC; // Array
                    break;
                
                default:
                    $type = \PDO::FETCH_OBJ; // Object
                    break;
            }
            
            try {
                $query = $db->prepare($sql);
                $query->execute($params);

                $result = $query->fetchAll($type);

                if (count($result) > 0) {
                    return $result;
                } else {
                    return false;
                }
            } catch (\PDOException $e) {
                $this->exceptionHandler->logError($e, '500');
                Redirect::location('errors/internal');
            }

        }

        public function insert($query, $params) {
            $db = self::getConnection();

            try {
                $query = $db->prepare($query);
                $result = $query->execute($params);

                return ($result) ? $db->lastInsertId() : false;
            } catch (\PDOException $e) {
                $this->exceptionHandler->logError($e, '500');
                Redirect::location('errors/internal');
            }
        }

        public function update($query, $params) {
            $db = self::getConnection();

            try {
                $query = $db->prepare($query);
                $result = $query->execute($params);

                return ($result) ? $result : false;
            } catch (\PDOException $e) {
                $this->exceptionHandler->logError($e, '500');
                Redirect::location('errors/internal');
            }
        }

        public function delete($query, $params) {
            $db = self::getConnection();

            try {
                $query = $db->prepare($query);
                $result = $query->execute($params);

                return $result;
            } catch (\PDOException $e) {
                $this->exceptionHandler->logError($e, '500');
                Redirect::location('errors/internal');
            }
        }

    }