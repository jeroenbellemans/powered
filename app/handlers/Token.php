<?php

    namespace App\handlers;

    class Token {

        public static function generate() {

			if (function_exists('mcrypt_create_iv')) {
				$_SESSION['token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
			} else {
				$_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
			}

			return $_SESSION['token'];

		}

        public static function matches($token) {

			if ($_SESSION['token'] == $token) {
				unset($_SESSION['token']);
				return true;
			}

			return false;

		}

    }