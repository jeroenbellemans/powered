<?php

    namespace App\handlers;
    
    class Mailer {

        public $from;
        public $to = array();
        public $subject;
        public $message;
        public $headers = array();

        public function __construct() {}

        private function build() {
            return true;
        }

        public function send() {
            return true;
        }

        public function setRecipients($recipients) {
            $this->to = $recipients;
        }

        public function setSender($email) {
            $this->from = $email;
        }

        public function setSubject($subject) {
            $this->subject = $subject;
        }

        public function setMessage($message, $params = array()) {
            $this->message = $message;
        }

        public function setHeaders() {
            $this->headers[] = 'MIME-Version: 1.0';
            $this->headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $this->headers[] = 'From: Power-ED <contact@power-ed.nl>';
        }

        public function sendMessage() {
            $mail = mail($this->to, $this->subject, $this->message, implode("\r\n", $this->headers));
            return $mail;
        }

    }