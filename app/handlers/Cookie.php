<?php

    namespace App\handlers;

    use \App\models\User_model as User;
    use \App\handlers\Redirect;
	
	class Cookie {

		public static function setCookie($data) {
            $_SESSION['is_logged_in'] = true;
            $cookieHash = password_hash($data->email, PASSWORD_BCRYPT);
            setcookie("s_sparkle", $cookieHash, time() + 3600 * 24 * 365, 'powertool.localhost');

            $model = new User();
            $model->updateUser($cookieHash, $data);
        }
        
        public static function validateCookie($data) {
            $model = new User();

            if ($model->checkSecuredHash($data)) {
                Redirect::location('dashboard');
            }
        }

	}