<?php

    namespace App\handlers;
    
    use \App\models\Permission_model as Permission;
    use \App\handlers\Redirect;

    class Guardian {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new Permission();
        }

        // Todo
        public function arrivalCheck($permission, $permissionId) {
            $permissionsForCurrentRole = $this->model->getPermissionsById($permissionId);
            $permissionsForCurrentRole = json_decode($permissionsForCurrentRole->permissions);

            return ($permissionsForCurrentRole->$permission);
        }

        public function passportCheck($permission, $permissionId) {
            $permissionsForCurrentRole = $this->model->getPermissionsById($permissionId);
            $permissionsForCurrentRole = json_decode($permissionsForCurrentRole[0]->permissions);

            $permissions = explode("@", $permission);
            $permissionsTopic = $permissions[0];
            $permissionsLine = $permissions[1];

            if (!isset($permissionsForCurrentRole->$permissionsTopic->$permissionsLine)) {
                return false;
            }

            return ($permissionsForCurrentRole->$permissionsTopic->$permissionsLine == true) ? true : false;
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }