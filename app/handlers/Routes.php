<?php

    class Routes {

        public static function build($path, $name, $params = array(), $exception = false, $external = false) {
            $defaults = array(
                "id"        =>  "",
                "title"     =>  "",
                "target"    =>  "_self",
                "class"     =>  ""
            );

            $defaults = $params + $defaults;
            $options = '';

            $request = $_SERVER['REQUEST_URI'];
            $request = explode('?', $request);
            $request = $request[0];
            $request = explode("/", $request, 2);
            $request = $request[1];
            $request = trim($request, '/');
            $request = explode('/', $request);
            
            foreach ($defaults as $option => $value) {
                if ($option == 'class') {
                    $active = (implode("/", $request) == $path) ? 'active' : '';
                    $options .= $option . '="' . $value . ' ' . $active . '" ';
                } else {
                    $options .= $option . '="' . $value . '" ';
                }
            }
            
            $options = substr($options, 0, -1);
            $url = ($external) ? '<a href="' . $path . '" ' . $options . '>' . $name . '</a>' : '<a href="' . config_get_root() . $path . '" ' . $options . '>' . $name . '</a>';

            if ($exception == 'mailto') {
                $url = '<a href="mailto: ' . $path . '" ' . $options . '>' . $name . '</a>';
            }

            return $url;

        }

    }