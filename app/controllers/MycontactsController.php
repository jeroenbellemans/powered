<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\models\Permission_model as Permission;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;
    use \App\handlers\Guardian;

    class MycontactsController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->permissionModel = new Permission();
            $this->view = new View();
            $this->guardian = new Guardian();
            $this->mailer = new Mailer();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function overview() {
            
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->contacts = $this->model->getAllContactsForOwner();
            $data->guardian = $this->guardian;
            return $this->view->render('views/my-contacts/overview.php', $data);
        }

        public function create() {
            
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->guardian = $this->guardian;
            return $this->view->render('views/my-contacts/create.php', $data);
        }

        /**
         * 
         * @Deprecated
         */
        public function createContact($postData) {
            if (!$this->model->emailExistsInSystem($postData->email)) {
                $contact = $this->model->createContact($postData);
                if ($contact) {

                    $this->mailer->setRecipients($postData->email); // $postData->email)
                    $this->mailer->setSender('contact@power-ed.nl');
                    $this->mailer->setSubject('Power-ED: er is een account voor u aangemaakt');
                    $this->mailer->setMessage('test'); // file_get_contents('resources/templates/contact-created.html')
                    $this->mailer->setHeaders();

                    //$this->mailer->send();
                    //exit;
                    $mail = $this->mailer->sendMessage();
                    if ($mail){
                        return [
                            "response" => "Er is iets fout gelopen, probeer opnieuw.",
                            "success" => false
                        ];
                        exit;
                        // Redirect::location('my-contacts/overview');
                    }
                } else {
                    return [
                        "response" => "Er is iets fout gelopen, probeer opnieuw.",
                        "success" => false
                    ];
                }
            } else {
                return [
                    "response" => "Het opgegeven e-mail adres is reeds in gebruik.",
                    "success" => false
                ];
            }
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }