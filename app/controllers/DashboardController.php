<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\Permission_model as Permission;
    use \App\models\User_model as User;
    use \App\models\Survey_model as Survey;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;
    use \App\handlers\Guardian;
    use \App\controllers\SessionController;

    class DashboardController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->permissionModel = new Permission();
            $this->view = new View();
            $this->surveyModel = new Survey();
            $this->guardian = new Guardian();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function index() {
            SessionController::session_required();

            if (isset($this->request[3])) {
                $userId = $this->request[3];
            } else {
                $userId = $_SESSION['id'];
            }

            $navigationData = new \StdClass();
            $navigationData->modules = $this->surveyModel->fetchModules();

            foreach ($navigationData->modules as $module) {
                $module->submodules = $this->surveyModel->fetchSubmodule($module->id);
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;
            $data->sidenavigation = $navigationData;

            return $this->view->render('views/dashboard/dashboard.php', $data);
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }