<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\models\Permission_model as Permission;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;
    use \App\handlers\Guardian;

    use \PHPMailer\PHPMailer\PHPMailer as PHPMailer;

    class MyaccountController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->permissionModel = new Permission();
            $this->view = new View();
            $this->guardian = new Guardian();
            $this->mailer = new PHPMailer();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function my_details() {
            $this->personal_details();
        }

        public function company_details() {
            SessionController::session_required();

            if (isset($this->request[3])) {
                $userId = $this->request[3];
            } else {
                $userId = $_SESSION['id'];
            }
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->companyDetails = $this->model->getCompanyDetails($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;

            return $this->view->render('views/my-account/professional-details.php', $data);
        }

        public function personal_details() {
            SessionController::session_required();

            if (isset($this->request[3])) {
                $userId = $this->request[3];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;

            return $this->view->render('views/my-account/personal-details.php', $data);
        }

        public function my_contacts() {
            if (isset($this->request[2])) {
                switch ($this->request[2]) {
                    case 'create':
                        $this->my_contactsCreate();
                        break;
                    
                    default:
                        $this->my_contactsOverview();
                        break;
                }
            } else {
                $this->my_contactsOverview();
            }
        }

        public function listUsers() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->accountInfo = $this->user_fields;
            $data->users = $this->model->getAllUsers();
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/users/list.php', $data);
        }

        public function update($target, $data) {
            switch ($target) {
                case 'role':
                    $this->model->updatePermissionSetId($data);
                    Redirect::location('admin/users/edit/' . $data->userId . '#role');
                    break;

                case 'personal':

                    // Format birthdate
                    $dateTime = new \DateTime($data->birthdate);
                    $data->birthdate = $dateTime->format('U');

                    $this->model->updatePersonalDetails($data);
                    Redirect::location('my-account/personal-details');
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        public function my_contactsOverview() {
            
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->contacts = $this->model->getAllContactsForOwner();
            $data->guardian = $this->guardian;
            return $this->view->render('views/my-contacts/overview.php', $data);
        }

        public function my_contactsCreate() {
            
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->guardian = $this->guardian;
            return $this->view->render('views/my-contacts/create.php', $data);
        }

        public function my_contactsCreateContact($postData) {

            // Generate token for newly created account
            $token = bin2hex(random_bytes(78));
            $postData->token = $token;

            if (!$this->model->emailExistsInSystem($postData->email)) {
                $contact = $this->model->createContact($postData);
                if ($contact) {

                    $contactInfo = $this->model->getUserInfo($contact);
                    $name = $contactInfo->personal->firstname . ' ' . $contactInfo->personal->insertion . ' ' . $contactInfo->personal->lastname;

                    $template = file_get_contents('resources/templates/contact-created.html');
				    $message = str_replace(array("[name]", "[context-id]", "[token]"), array($name, $contact, $token), $template);
                    
                    $this->mailer->setFrom('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addReplyTo('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addAddress($postData->email);
                    $this->mailer->addBCC("dave@power-ed.nl");

                    $this->mailer->Subject = 'Welkom op Power-ED!';
                    $this->mailer->msgHTML($message);

                    if ($this->mailer->send()) {
                        Redirect::location('my-contacts/overview');
                    } else {
                        return [
                            "response" => "Er is iets fout gelopen, contacteer een administrator.",
                            "success" => false
                        ];
                    }
                } else {
                    return [
                        "response" => "Er is iets fout gelopen, probeer opnieuw.",
                        "success" => false
                    ];
                }
            } else {
                return [
                    "response" => "Het opgegeven e-mail adres is reeds in gebruik.",
                    "success" => false
                ];
            }
        }

        public function saveCompanyDetails($postData) {
            return $this->model->saveCompanyDetails($postData);
        }

        public function saveCompanyDetailsField($input, $field, $companyId) {
            return $this->model->saveCompanyDetailsField($input, $field, $companyId);
        }

        public function createSubCompany($postData) {
            return $this->model->createSubCompany($postData);
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }