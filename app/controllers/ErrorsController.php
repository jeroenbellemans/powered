<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;

    use \PHPMailer\PHPMailer\PHPMailer as PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    class ErrorsController {

        private $mailer;
        private $view;

        public function __construct() {
            $this->view = new View();
            $this->mailer = new PHPMailer();
        }

        public function logError($exception, $statusCode) {
            $template = file_get_contents('resources/templates/error.html');
            $contextUser = (isset($_SESSION['id'])) ? $_SESSION['id'] : 'N/A';
            $message = str_replace(
                array(
                    "[name]", 
                    "[request]", 
                    "[message]", 
                    "[stacktrace]",
                    "[file]",
                    "[lineNumber]",
                    "[context-id]"
                ), 
                array(
                    'Administrator', 
                    $_SERVER['REQUEST_URI'], 
                    $exception->getMessage(), 
                    $exception->getTraceAsString(),
                    $exception->getFile(),
                    $exception->getLine(),
                    $contextUser
                ),
                $template
            );

            $this->mailer->setFrom('welkom@power-ed.nl', 'Power ED');
            //$this->mailer->addReplyTo('welkom@power-ed.nl', 'Power ED');
            $this->mailer->addAddress("jeroenbellemans@hotmail.com");
            //$this->mailer->addBCC("dave@power-ed.nl");

            $this->mailer->Subject = 'Power-ED: Er is een fout (' . $statusCode . ') opgetreden';
            $this->mailer->msgHTML($message);

            if ($this->mailer->send()) {
                return true;
            } else {
                return [
                    "response" => "Er is iets fout gelopen, contacteer een administrator.",
                    "success" => false
                ];
            }
        }

        public function internal() {
            return $this->view->render('views/errors/500.php');
        }

    }