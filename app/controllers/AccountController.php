<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;

    use \PHPMailer\PHPMailer\PHPMailer as PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    class AccountController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->view = new View();
            $this->mailer = new PHPMailer();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function settings() {
            $data = new \StdClass();
            $data->accountInfo = $this->user_fields;
            return $this->view->render('views/account/settings.php', $data, $this->request);
        }

        public function sign_out() {
            if (isset($this->id)) {
                session_destroy();
                Redirect::location('login');
            }
        }

        public function sign_in($postData) {
            $response = new \StdClass();
            $data = $this->model->combination_exists($postData->email, $postData->password);

            if ($data->success == true) {
                // Set a cookie if user wants a continued session
                /*if ($postData->remember) {
                    Cookie::setCookie($postData);
                }*/

                Redirect::location('dashboard');
            }

            return $data;
        }

        public function submitResetPassword($postData) {
            if ($this->model->validToken($postData->id, $postData->token)) {
                $this->setPassword($postData, 'support/confirm-reset');
            }
        }

        public function setPassword($postData, $location) {
            if ($this->model->setPassword($postData)) {
                Redirect::location($location);
            }
            return false;
        }

        public function sendPasswordReset($postData) {
            if ($this->model->emailExistsInSystem($postData->email)) {
                
                // Generate token for newly created account
                $token = bin2hex(random_bytes(78));
                $postData->token = $token;

                $userInfo = $this->model->fetchUserInfoByEmail($postData->email);
                if ($this->model->updateUserWithToken($postData->token, $userInfo->id)) {
                    
                    $name = $userInfo->firstname . ' ' . $userInfo->insertion . ' ' . $userInfo->lastname;

                    $template = file_get_contents('resources/templates/forgot-password.html');
                    $message = str_replace(array("[name]", "[context-id]", "[token]"), array($name, $userInfo->id, $token), $template);

                    $this->mailer->setFrom('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addReplyTo('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addAddress($postData->email);
                    $this->mailer->addBCC("dave@power-ed.nl");

                    $this->mailer->Subject = 'Herstel uw wachtwoord!';
                    $this->mailer->msgHTML($message);

                    if ($this->mailer->send()) {
                        Redirect::location('support/recovery-confirmation');
                    } else {
                        return false;
                    }
                }
                return false;
            }
            return false;
        }

        public function validateCookie($cookie) {
            Cookie::validateCookie($cookie);
        }

        public function getAccountInformation() {
            return $this->model->getAccountInformation();
        }
        
        public function deleteUser($postData) {
            return $this->model->deleteUser($postData);
        }
        
        public function deleteClient($postData) {
            return $this->model->deleteClient($postData);
        }

        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }