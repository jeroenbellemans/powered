<?php

    namespace App\controllers;
    
    use \App\models\User_model as User;
    use \App\models\Permission_model as Permission;
    use \App\models\Survey_model as Survey;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Views as View;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;
    use \App\handlers\Guardian;
    use \App\Controllers\MyaccountController as MyAccount;

    class SurveyController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->permissionModel = new Permission();
            $this->view = new View();
            $this->guardian = new Guardian();
            $this->mailer = new Mailer();
            $this->surveyModel = new Survey();
            $this->myAccount = new MyAccount();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }
        
        public function sdg() {

            SessionController::session_required();

            $userId = $_SESSION['id'];

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->sdgSurveys = new \StdClass();
            $data->sdgSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->sdgSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->sdgScan = new \StdClass();
            $data->sdgScan->goals = $this->surveyModel->getSustainableDevelopmentGoals();
            $data->guardian = $this->guardian;

            if (isset($this->request[2])) {

                switch ($this->request[2]) {
                    case 'results':
                        $this->view_sdg($data->sdgScan->goals);
                    break;
                    
                    default:
            
                        if (isset($this->request[2])) {
                            $this->start_sdg($data->sdgScan->goals);
                        } else {
                            return $this->view->render('views/survey/introduction-sdg.php', $data);
                        }
                    break;
                }
            } else {
                return $this->view->render('views/survey/introduction-sdg.php', $data);
            }
            
        }

        public function footprint() {

            SessionController::session_required();

            $userId = $_SESSION['id'];

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->coSurveys = new \StdClass();
            $data->coSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->coSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->surveyHasYear = $this->surveyModel->getSurveyYearContext($this->request[1], 0);
            $data->establishment = $this->model->getCompanyDetails($userId, '1');
            $data->guardian = $this->guardian;

            if (isset($this->request[2])) {

                switch ($this->request[2]) {
                    case 'results':
                        $this->view_footprint();
                    break;
                    
                    default:
            
                        if (isset($this->request[2])) {
                            if (isset($this->request[3]) && $data->surveyHasYear) {
                                $this->start_footprint_scan();
                            } else {
                                $this->start_footprint();
                            }
                        } else {
                            return $this->view->render('views/survey/introduction-footprint.php', $data); // Here
                        }
                    break;
                }
            } else {
                return $this->view->render('views/survey/introduction-footprint.php', $data);
            }
            
        }

        public function mvo() {

            SessionController::session_required();

            $userId = $_SESSION['id'];

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->mvoSurveys = new \StdClass();
            $data->mvoSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->mvoSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->guardian = $this->guardian;

            if (isset($this->request[2])) {

                switch ($this->request[2]) {
                    case 'results':
                        $this->view_mvo();
                    break;
                    
                    default:
            
                        if (isset($this->request[2]) && isset($this->request[3])) {
                            $this->start_mvo(true);
                        } else
                        if (isset($this->request[2])) {
                            $this->start_mvo();
                        } else {
                            return $this->view->render('views/survey/introduction-mvo.php', $data);
                        }
                    break;
                }
            } else {
                return $this->view->render('views/survey/introduction-mvo.php', $data);
            }
            
        }

        public function view_sdg($sdgGoals) {
            SessionController::session_required();

            if (!$this->surveyModel->incomplete_survey($this->request[1])) {
                $this->surveyModel->create_survey($this->request[1]);
            }

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->context = $this->request[3];
            $data->guardian = $this->guardian;
            $data->sdgSurveys = new \StdClass();
            $data->sdgSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->sdgSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->answeredSdg = $this->surveyModel->getSdgSelection($this->request[4], $_SESSION['id']);
            $data->selectedSdg = array();

            foreach ($data->answeredSdg as $selected) {
                $data->selectedSdg[] = $selected->goal_id;
            }

            $data->sdgScan = new \StdClass();
            $data->sdgScan->goals = $sdgGoals;

            return $this->view->render('views/survey/sdg-view-results.php', $data);
        }

        public function start_sdg($sdgGoals) {
            SessionController::session_required();

            if (!$this->surveyModel->incomplete_survey($this->request[1])) {
                $this->surveyModel->create_survey($this->request[1]);
            }

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            $data->sdgSurveys = new \StdClass();
            $data->sdgSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->sdgSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->sdgScan = new \StdClass();
            $data->sdgScan->goals = $sdgGoals;

            return $this->view->render('views/survey/sdg-start.php', $data);
        }

        public function start_footprint() {
            SessionController::session_required();

            if (!$this->surveyModel->incomplete_survey($this->request[1])) {
                $this->surveyModel->create_survey($this->request[1]);
            }

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->guardian = $this->guardian;
            $data->establishment = $this->model->getCompanyDetails($userId, '1');
            $data->subEstablishment = $this->model->getCompanyDetails($userId, '0');
            $data->coSurveys = new \StdClass();
            $data->coSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->coSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->incompleteSurvey = $this->surveyModel->incomplete_survey($this->request[1]);

            return $this->view->render('views/survey/footprint-start.php', $data);
        }

        public function view_as_admin_footprint() {
            SessionController::session_required();

            if (isset($this->request[6])) {
                $userId = $this->request[6];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[4];
            $data->guardian = $this->guardian;
            $data->establishment = $this->model->getCompanyDetails($userId, '1');
            $data->subEstablishment = $this->model->getCompanyDetails($userId, '0');
            $data->getAllEstablishments = $this->model->getAllCompanyDetails($_SESSION['id']);
            $surveyData = new \StdClass();
            $surveyData->module = $this->surveyModel->fetchModule($this->request[3]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);
            $surveyData->answers = $this->surveyModel->fetchAnswersForQuestions();
            $surveyData->answersForQuestion = $this->surveyModel->getAllSurveyAnswers($this->request[5], 'admin', $userId);
            $surveyData->getQuestionByQuestion = $this->surveyModel->getAllQuestions();

            $surveyData->getQuestionsByQuestion = [];
            foreach ($surveyData->getQuestionByQuestion as $question) {
                $surveyData->getQuestionsByQuestion[$question->id] = $question;
            }

            $allSubmodulesForFootprint = array();
            $parentQuestions = array();

            foreach ($surveyData->submodules as $submodule) {
                $allSubmodulesForFootprint[$submodule->id] = $this->surveyModel->fetchCategories($submodule->id);

                foreach ($allSubmodulesForFootprint[$submodule->id] as $category) {
                    $parentQuestions[$category->id] = $this->surveyModel->fetchParentQuestionsForCategory($category->id);
                }
            }

            $answersPerQuestion = array();

            foreach ($surveyData->answers as $answer) {
                $answersPerQuestion[$answer->question_id][] = $answer;
            }

            $surveyData->categories = $allSubmodulesForFootprint;
            $surveyData->parentQuestions = $parentQuestions;
            $surveyData->answersPerQuestion = $answersPerQuestion;
            $surveyData->allQuestions = $this->surveyModel->fetchAllQuestions();
            
            $data->survey = $surveyData;
            $data->coSurveys = new \StdClass();
            $data->coSurveys->completed = $this->surveyModel->getSurveys($this->request[3], 1);
            $data->coSurveys->incompleted = $this->surveyModel->getSurveys($this->request[3], 0);
            $data->incompleteSurvey = $this->surveyModel->incomplete_survey($this->request[3]);
            $data->view_mode = 'admin';
            $data->request = $this->request;
            $data->surveyId = $this->request[5];

            return $this->view->render('views/survey/footprint-view-results-admin.php', $data);
        }
        
        public function view_footprint() {
            SessionController::session_required();

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->guardian = $this->guardian;
            $data->establishment = $this->model->getCompanyDetails($userId, '1');
            $data->subEstablishment = $this->model->getCompanyDetails($userId, '0');
            $data->getAllEstablishments = $this->model->getAllCompanyDetails($_SESSION['id']);
            $surveyData = new \StdClass();
            $surveyData->module = $this->surveyModel->fetchModule($this->request[1]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);
            $surveyData->answers = $this->surveyModel->fetchAnswersForQuestions();
            $surveyData->answersForQuestion = $this->surveyModel->getAllSurveyAnswers($this->request[4], 'original', $_SESSION['id']);
            $surveyData->getQuestionByQuestion = $this->surveyModel->getAllQuestions();

            $surveyData->getQuestionsByQuestion = [];
            foreach ($surveyData->getQuestionByQuestion as $question) {
                $surveyData->getQuestionsByQuestion[$question->id] = $question;
            }

            $allSubmodulesForFootprint = array();
            $parentQuestions = array();

            foreach ($surveyData->submodules as $submodule) {
                $allSubmodulesForFootprint[$submodule->id] = $this->surveyModel->fetchCategories($submodule->id);

                foreach ($allSubmodulesForFootprint[$submodule->id] as $category) {
                    $parentQuestions[$category->id] = $this->surveyModel->fetchParentQuestionsForCategory($category->id);
                }
            }

            $answersPerQuestion = array();

            foreach ($surveyData->answers as $answer) {
                $answersPerQuestion[$answer->question_id][] = $answer;
            }

            $surveyData->categories = $allSubmodulesForFootprint;
            $surveyData->parentQuestions = $parentQuestions;
            $surveyData->answersPerQuestion = $answersPerQuestion;
            $surveyData->allQuestions = $this->surveyModel->fetchAllQuestions();
            
            $data->survey = $surveyData;
            $data->coSurveys = new \StdClass();
            $data->coSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->coSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->incompleteSurvey = $this->surveyModel->incomplete_survey($this->request[1]);

            return $this->view->render('views/survey/footprint-view-results.php', $data);
        }

        public function start_footprint_scan() {
            SessionController::session_required();

            if (!$this->surveyModel->incomplete_survey($this->request[1])) {
                $this->surveyModel->create_survey($this->request[1]);
            }

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->guardian = $this->guardian;
            $data->establishment = $this->model->getCompanyDetails($userId, '1');
            $data->subEstablishment = $this->model->getCompanyDetails($userId, '0');
            $data->getAllEstablishments = $this->model->getAllCompanyDetails($_SESSION['id']);
            $surveyData = new \StdClass();
            $surveyData->module = $this->surveyModel->fetchModule($this->request[1]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);
            $surveyData->answers = $this->surveyModel->fetchAnswersForQuestions();

            $allSubmodulesForFootprint = array();
            $parentQuestions = array();

            foreach ($surveyData->submodules as $submodule) {
                $allSubmodulesForFootprint[$submodule->id] = $this->surveyModel->fetchCategories($submodule->id);

                foreach ($allSubmodulesForFootprint[$submodule->id] as $category) {
                    $parentQuestions[$category->id] = $this->surveyModel->fetchParentQuestionsForCategory($category->id);
                }
            }

            $answersPerQuestion = array();

            foreach ($surveyData->answers as $answer) {
                $answersPerQuestion[$answer->question_id][] = $answer;
            }

            $surveyData->categories = $allSubmodulesForFootprint;
            $surveyData->parentQuestions = $parentQuestions;
            $surveyData->answersPerQuestion = $answersPerQuestion;
            $surveyData->allQuestions = $this->surveyModel->fetchAllQuestions();
            
            $data->survey = $surveyData;
            $data->coSurveys = new \StdClass();
            $data->coSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->coSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->incompleteSurvey = $this->surveyModel->incomplete_survey($this->request[1]);

            return $this->view->render('views/survey/footprint-start-scan.php', $data);
        }

        public function start_mvo($start_scan = false) {
            SessionController::session_required();

            if (!$this->surveyModel->incomplete_survey($this->request[1])) {
                $this->surveyModel->create_survey($this->request[1]);
            }

            if (isset($this->request[4])) {
                $userId = $this->request[4];
            } else {
                $userId = $_SESSION['id'];
            }
            $surveyData = new \StdClass();
            $surveyData->module = $this->surveyModel->fetchModule($this->request[1]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);

            $navigationData = new \StdClass();
            $navigationData->modules = $this->surveyModel->fetchModules();

            foreach ($navigationData->modules as $module) {
                $module->submodules = $this->surveyModel->fetchSubmodule($module->id);
            }

            foreach ($surveyData->submodules as $submodule) {
                $submodule->open_question = $this->surveyModel->fetchOpenQuestionForSubmodule($submodule->id);
                $submodule->questionIntro = $this->surveyModel->fetchQuestionIntroForSubmodule($submodule->id);

                if ($submodule->questionIntro) {
                    foreach ($submodule->questionIntro as $questionIntro) {
                        $questionIntro->questions = $this->surveyModel->fetchQuestionsForQuestionIntro($questionIntro->id);
                        if ($questionIntro->questions) {
                            foreach ($questionIntro->questions as $question) {
                                if (in_array($question->question_type, array("single", "dropdown", "multiple")) && ($question->has_answer_set_id)) {
                                    $question->possibleAnswers = $this->surveyModel->fetchAnswersForQuestion($question->answer_set_id);
                                }
                            }
                        }
                    }
                }
            }

            $data = new \StdClass();
            $data->mvoSurveys = new \StdClass();
            $data->mvoSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->mvoSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[4])) ? $this->request[4] : $userId;
            $data->guardian = $this->guardian;
            $data->survey = $surveyData;
            $data->sidenavigation = $navigationData;
            $data->incompleteSurvey = $this->surveyModel->incomplete_survey($this->request[1]);

            $view = ($start_scan) ? 'views/survey/mvo-start-scan.php' : 'views/survey/mvo-start.php';

            return  $this->view->render($view, $data);
        }

        public function view_mvo() {
            SessionController::session_required();
            
            $userId = $_SESSION['id'];

            $surveyData = new \StdClass();
            $surveyData->isReviewed = $this->surveyModel->getIsSurveyReviewed($this->request[4]);
            $surveyData->module = $this->surveyModel->fetchModule($this->request[1]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);

            $navigationData = new \StdClass();
            $navigationData->modules = $this->surveyModel->fetchModules();

            foreach ($navigationData->modules as $module) {
                $module->submodules = $this->surveyModel->fetchSubmodule($module->id);
            }

            foreach ($surveyData->submodules as $submodule) {
                $submodule->open_question = $this->surveyModel->fetchOpenQuestionForSubmodule($submodule->id);
                $submodule->questionIntro = $this->surveyModel->fetchQuestionIntroForSubmodule($submodule->id);

                if ($submodule->questionIntro) {
                    foreach ($submodule->questionIntro as $questionIntro) {
                        $questionIntro->questions = $this->surveyModel->fetchAnsweredQuestionsForQuestionIntro($questionIntro->id, $this->request[3], $this->request[4]);
                        if ($questionIntro->questions) {
                            foreach ($questionIntro->questions as $question) {
                                if (in_array($question->question_type, array("single", "dropdown", "multiple")) && ($question->has_answer_set_id)) {
                                    $question->possibleAnswers = $this->surveyModel->fetchAnswersForQuestion($question->answer_set_id);
                                }
                            }
                        }
                    }
                }
            }

            $surveyData->answers = $this->surveyModel->fetchAnswersForModuleAndUser($this->request[3], $userId);
            $surveyData->openQuestionAnswers = $this->surveyModel->fetchOpenQuestionAnswersForSurveyAndUser($this->request[4], $userId);

            if ($surveyData->answers) {
                $questionAnswers = array();
                foreach ($surveyData->answers as $answer) {
                    $questionAnswers[$answer->question_description_id] = $answer->answer_set_id;
                }
            }

            $openQuestionAnswers = false;
            if ($surveyData->openQuestionAnswers) {
                $openQuestionAnswers = array();
                foreach ($surveyData->openQuestionAnswers as $answer) {
                    $openQuestionAnswers[$answer->open_question_id] = $answer->answer;
                }
            }

            $surveyData->answers = $questionAnswers;
            $surveyData->openAnswers = $openQuestionAnswers;

            $data = new \StdClass();
            $data->mvoSurveys = new \StdClass();
            $data->mvoSurveys->completed = $this->surveyModel->getSurveys($this->request[1], 1);
            $data->mvoSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;
            $data->survey = $surveyData;
            $data->sidenavigation = $navigationData;

            return $this->view->render('views/survey/mvo-view-results.php', $data);
        }

        public function createAnswersForQuestions($postData) {

            $data = new \StdClass();
            $data->mvoSurveys = new \StdClass();
            $data->mvoSurveys->incompleted = $this->surveyModel->getSurveys($this->request[1], 0);

            if ($this->surveyModel->createAnswersForQuestions($postData, $data->mvoSurveys->incompleted[0]->survey_id)) {
                Redirect::location('survey/mvo');
            }
        }

        public function createAdminAnswersForQuestions($postData) {

            $data = new \StdClass();

            if ($this->surveyModel->createAdminAnswersForQuestions($postData, $this->request[5])) {
                Redirect::location('admin/clients/overview');
            }
        }

        public function saveSdgSelection($postData, $returnParam, $admin = false) {
            $reformattedPostData = new \StdClass();

            $reformattedPostData->sdg = explode("/", $postData->sdg);
            $removedValue = array_pop($reformattedPostData->sdg);
            $reformattedPostData->sdg = $reformattedPostData->sdg;

            if (isset($postData->originalSdg)) {
                $reformattedPostData->originalSdg = explode("/", $postData->originalSdg);
                $removedValue = array_pop($reformattedPostData->originalSdg);
                $reformattedPostData->originalSdg = $reformattedPostData->originalSdg;
            }

            $reformattedPostData->moduleId = $postData->moduleId;
            $reformattedPostData->module = isset($postData->module) ? $postData->module : false;
            $reformattedPostData->surveyId = $postData->surveyId;

            if ($this->surveyModel->saveSdgSelection($reformattedPostData, $returnParam, $admin)) {
                if ($returnParam == 'survey') {
                    Redirect::location('survey/sdg');
                } else {
                    Redirect::location('admin/clients/contact/' . $returnParam);
                }
            }
        }

        public function saveFootprintAnswers($postData) {
            $postData['module'] = $this->request[1];
            if ($this->surveyModel->saveFootprintAnswers($postData)) {
                Redirect::location('survey/footprint');
            }
        }

        public function updateFootprintAnswers($postData) {
            $postData['surveyId'] = $this->request[5];
            if ($this->surveyModel->updateFootprintAnswers($postData)) {
                Redirect::location('survey/footprint');
            }
        }

        public function createSubCompany($postData) {
            if ($this->myAccount->createSubCompany($postData)) {
                Redirect::location('survey/footprint/start');
            }

            return $postData;
        }

        public function fetchQuestionForAnswer($questionId) {
            return $this->surveyModel->fetchQuestionForAnswer($questionId);
        }

        public function fetchPossibleAnswersForQuestionId($questionId) {
            return $this->surveyModel->fetchPossibleAnswersForQuestionId($questionId, $this->request[4]);
        }

        public function fetchAnswersForSurveyId($questionId, $userId) {
            $requestPosition = isset($this->request[5]) ? $this->request[5] : $this->request[4];
            return $this->surveyModel->fetchAnswersForSurveyId($requestPosition, $userId);
        }

        public function setYearForScan($postData, $location) {

            $survey = $this->surveyModel->incomplete_survey($this->request[1]);

            if ($this->surveyModel->setYearForScan($postData, $survey[0]->id)) {
                Redirect::location($location);
            }
        }

        public function fetchModule($module) {
            return $this->surveyModel->fetchModule($module);
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

        public function view_as_admin_mvo() {
            SessionController::session_required();
            
            $userId = $this->request[6];

            $surveyData = new \StdClass();
            $surveyData->module = $this->surveyModel->fetchModule($this->request[3]);
            $surveyData->submodules = $this->surveyModel->fetchSubmodule($surveyData->module[0]->id);

            $navigationData = new \StdClass();
            $navigationData->modules = $this->surveyModel->fetchModules();

            foreach ($navigationData->modules as $module) {
                $module->submodules = $this->surveyModel->fetchSubmodule($module->id);
            }

            foreach ($surveyData->submodules as $submodule) {
                $submodule->open_question = $this->surveyModel->fetchOpenQuestionForSubmodule($submodule->id);
                $submodule->questionIntro = $this->surveyModel->fetchQuestionIntroForSubmodule($submodule->id);

                if ($submodule->questionIntro) {
                    foreach ($submodule->questionIntro as $questionIntro) {
                        $questionIntro->questions = $this->surveyModel->fetchAnsweredQuestionsForQuestionIntro($questionIntro->id, $this->request[4], $this->request[5]);
                        if ($questionIntro->questions) {
                            foreach ($questionIntro->questions as $question) {
                                if (in_array($question->question_type, array("single", "dropdown", "multiple")) && ($question->has_answer_set_id)) {
                                    $question->possibleAnswers = $this->surveyModel->fetchAnswersForQuestion($question->answer_set_id);
                                }
                            }
                        }
                    }
                }
            }

            $surveyData->answers = $this->surveyModel->fetchAnswersForModuleAndUser($this->request[4], $userId);
            $surveyData->openQuestionAnswers = $this->surveyModel->fetchOpenQuestionAnswersForSurveyAndUser($this->request[5], $userId);

            $questionAnswers = false;
            $reviewedQuestionAnswers = false;
            if ($surveyData->answers) {
                $questionAnswers = array();
                $reviewedQuestionAnswers = array();
                foreach ($surveyData->answers as $answer) {
                    $questionAnswers[$answer->question_description_id] = $answer->answer_set_id;
                    $reviewedQuestionAnswers[$answer->question_description_id] = $answer->review_answer_set_id;
                }
            }

            $questionAnswerIds = false;
            if ($surveyData->answers) {
                $questionAnswerIds = array();
                foreach ($surveyData->answers as $answer) {
                    $questionAnswerIds[$answer->question_description_id] = $answer->id;
                }
            }

            $openQuestionAnswers = false;
            $reviewedOpenQuestionAnswers = false;
            if ($surveyData->openQuestionAnswers) {
                $openQuestionAnswers = array();
                $reviewedOpenQuestionAnswers = array();
                foreach ($surveyData->openQuestionAnswers as $answer) {
                    $openQuestionAnswers[$answer->open_question_id] = $answer->answer;
                    $reviewedOpenQuestionAnswers[$answer->open_question_id] = $answer->review_answer;
                }
            }

            $surveyData->answers = $questionAnswers;
            $surveyData->reviewedAnswers = $reviewedQuestionAnswers;
            $surveyData->openAnswers = $openQuestionAnswers;
            $surveyData->reviewedOpenAnswers = $reviewedOpenQuestionAnswers;
            $surveyData->answerIds = $questionAnswerIds;

            $data = new \StdClass();
            $data->mvoSurveys = new \StdClass();
            $data->mvoSurveys->completed = $this->surveyModel->getSurveys($this->request[3], 1);
            $data->mvoSurveys->incompleted = $this->surveyModel->getSurveys($this->request[3], 0);
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;
            $data->survey = $surveyData;
            $data->sidenavigation = $navigationData;
            $data->test = $this->surveyModel->fetchAnswersForModuleAndUser($this->request[4], $userId);

            return $this->view->render('views/survey/mvo-view-results-admin.php', $data);
        }

        public function view_as_admin_sdg() {
            SessionController::session_required();

            $userId = $this->request[6];

            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->moduleId = $this->request[2];
            $data->context = $this->request[5];
            $data->guardian = $this->guardian;
            $data->sdgSurveys = new \StdClass();
            $data->sdgSurveys->completed = $this->surveyModel->getSurveys($this->request[3], 1);
            $data->sdgSurveys->incompleted = $this->surveyModel->getSurveys($this->request[3], 0);
            $data->sdgGoals = $this->surveyModel->getSustainableDevelopmentGoals();
            $data->answeredSdg = $this->surveyModel->getSdgSelection($this->request[5], $this->request[6]);
            $data->answeredReviewedSdg = $this->surveyModel->getReviewedSdgSelection($this->request[5], $this->request[6]);
            $data->selectedSdg = array();

            if ($data->answeredReviewedSdg) {
                foreach ($data->answeredReviewedSdg as $selected) {
                    $data->selectedSdg[] = $selected->review_goal_id;
                }
            } else {
                foreach ($data->answeredSdg as $selected) {
                    $data->selectedSdg[] = $selected->goal_id;
                }
            }

            $data->sdgScan = new \StdClass();
            $data->sdgScan->selection = $data->selectedSdg;
            $data->sdgScan->goals = $data->sdgGoals;

            return $this->view->render('views/survey/sdg-view-results-admin.php', $data);
        }

    }