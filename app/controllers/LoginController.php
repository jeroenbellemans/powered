<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;

    class LoginController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->view = new View();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function index() {
            return $this->view->render('views/login.php');
        }

        public function is_activated() {
           return ($this->user_fields->activated == 1);
        }

        public function sign_in($postData) {
            $response = new \StdClass();

            if (!$this->model->combination_exists($postData->email, $postData->password)) {
                $response->success = false;
                $response->message = 'Invalid e-mail address or password';
                return $response;
            } else {
                $response->success = true;
                $response->data = 'Redirect';
                $response->message = 'Login successfull';
            }

            return $response;
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }