<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\models\Survey_model as Survey;
    use \App\models\Permission_model as Permission;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;
    use \App\handlers\Guardian;
    use \App\controllers\MyaccountController as myaccountController;
    use \App\controllers\SurveyController as SurveyController;

    use \PHPMailer\PHPMailer\PHPMailer as PHPMailer;

    class AdminController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->surveyModel = new Survey();
            $this->permissionModel = new Permission();
            $this->view = new View();
            $this->guardian = new Guardian();
            $this->mailer = new PHPMailer();

            $this->myaccountController = new myaccountController();
            $this->surveyController = new SurveyController($_SESSION['id'], $this->request);
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function users() {
            switch ($this->request[2]) {
                case 'overview':
                    $this->listUsers();
                    break;

                case 'edit':
                    $this->editUser();
                    break;

                case 'view':
                    $this->viewUser();
                    break;

                case 'create':
                    $this->createUser();
                    break;
                
                default:
                    // Target page not found controller & create method to load the view (more oo)
                    $this->pageNotFound();
                    break;
            }
        }

        public function permissions() {
            switch ($this->request[2]) {
                case 'overview':
                    $this->overviewPermissions();
                    break;

                case 'view':
                    $this->viewPermissions();
                    break;

                case 'edit':
                    $this->editPermissions();
                    break;
                
                default:
                    // Target page not found controller & create method to load the view (more oo)
                    $this->pageNotFound();
                    break;
            }
        }

        public function index() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->accountInfo = $this->user_fields;
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/index.php', $data);
        }

        public function listUsers() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->accountInfo = $this->user_fields;
            $data->users = $this->model->getAllUsers();
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/users/overview.php', $data);
        }

        public function editUser() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($this->request[3]);
            $data->companyDetails = $this->model->getCompanyDetails($this->request[3]);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = $this->request[3];
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/users/edit.php', $data);
        }

        public function createUser() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/users/create.php', $data);
        }

        public function viewUser() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($this->request[3]);
            $data->companyDetails = $this->model->getCompanyDetails($this->request[3]);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = $this->request[3];
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/users/view.php', $data);
        }

        public function overviewPermissions() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/permissions/overview.php', $data);
        }

        public function viewPermissions() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->permission = $this->permissionModel->getPermissionsById($this->request[3]);
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/permissions/view.php', $data);
        }

        public function editPermissions() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->permission = $this->permissionModel->getPermissionsById($this->request[3]);
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            return $this->view->render('views/admin/permissions/edit.php', $data);
        }

        public function clients() {
            switch ($this->request[2]) {
                case 'overview':
                    $this->listClients();
                    break;

                case 'view':
                    $this->viewClient();
                    break;

                case 'contact':
                    $this->viewContact();
                    break;

                case 'create':
                    $this->createClient();
                    break;

                case 'survey':
                    
                    if ($this->request[3] == 'mvo') {
                        $this->surveyController->view_as_admin_mvo();
                    } else
                    if ($this->request[3] == 'footprint') {
                        $mode = (isset($this->request[6])) ? $this->request[6] : 'normal';
                        $this->surveyController->view_as_admin_footprint($mode);
                    } else
                    if ($this->request[3] == 'sdg') {
                        $this->surveyController->view_as_admin_sdg();
                    }

                    break;
                
                default:
                    // Target page not found controller & create method to load the view (more oo)
                    $this->pageNotFound();
                    break;
            }
        }

        public function listClients() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            $data->clients = $this->model->getAllClients();
            return $this->view->render('views/admin/clients/overview.php', $data);
        }

        public function viewClient() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            $data->currentClient = $this->model->getClient($this->request[3])[0];
            $data->contacts = $this->model->getAllUsersForOwner($data->currentClient->user_id);
            return $this->view->render('views/admin/clients/view.php', $data);
        }

        public function viewContact() {
            SessionController::session_required();
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : false;
            $data->guardian = $this->guardian;
            $data->currentContact = $this->model->getUserInfo($this->request[3]);
            $data->contacts = $this->model->getAllUsersForOwner($data->currentContact->personal->id);
            $data->surveys = $this->surveyModel->getSurveysForUser($data->currentContact->personal->id);
            return $this->view->render('views/admin/clients/view-contact.php', $data);
        }

        public function createClient() {
            SessionController::session_required();

            if (isset($this->request[3])) {
                $userId = $this->request[3];
            } else {
                $userId = $_SESSION['id'];
            }
            $data = new \StdClass();
            $data->instance = $this;
            $data->accountInfo = $this->user_fields;
            $data->userInfo = $this->model->getUserInfo($userId);
            $data->companyDetails = $this->model->getCompanyDetails($userId);
            $data->permissions = $this->permissionModel->getAllPermissions();
            $data->context = (isset($this->request[3])) ? $this->request[3] : $userId;
            $data->guardian = $this->guardian;

            return $this->view->render('views/admin/clients/create.php', $data);
        }

        public function createAccountAndCompanyDetails($postData) {
            $contact = $this->createAccount($postData);
            if ($contact && !is_array($contact)) {
                $postData->userId = $contact;
                if ($this->myaccountController->saveCompanyDetails($postData)) {
                    Redirect::location('admin/clients/overview');
                }
            } else {
                return $contact;
            }
        }
        
        public function updatePermissionsForRole($postData) {

            $actions = [
                "view",
                "create",
                "update",
                "delete"
            ];
            
            $permission_object = new \StdClass();

            foreach ($postData->data as $key => $value) {
                if ($key == $value) {
                    $permission_object->$key = new \StdClass();

                    foreach ($actions as $action) {
                        $property = $action . '_' . $key;
                        $permission_object->$key->$property = isset($postData->data[$property]) ? true : false;
                    }
                }
            }

            $permissionString = json_encode($permission_object);

            $this->permissionModel->updatePermissionsForRole($permissionString, $postData->permissionId);

            Redirect::location('admin/permissions/view/' . $postData->permissionId);

        }

        public function update($target, $data) {
            switch ($target) {
                case 'role':
                    $this->model->updatePermissionSet($data);
                    Redirect::location('admin/users/edit/' . $data->userId . '#role');
                    break;

                case 'personal':

                    // Format birthdate
                    $dateTime = new \DateTime($data->birthdate);
                    $data->birthdate = $dateTime->format('U');

                    $this->model->updatePersonalDetails($data);
                    Redirect::location('admin/users/edit/' . $data->userId . '#personal');
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        public function createAccount($postData) {

            // Generate token for newly created account
            $token = bin2hex(random_bytes(78));
            $postData->token = $token;

            if (!$this->model->emailExistsInSystem($postData->email)) {
                $contact = $this->model->createAccount($postData);
                if ($contact) {

                    $contactInfo = $this->model->getUserInfo($contact);
                    $name = $contactInfo->personal->firstname . ' ' . $contactInfo->personal->insertion . ' ' . $contactInfo->personal->lastname;

                    $template = file_get_contents('resources/templates/account-created.html');
				    $message = str_replace(array("[name]", "[context-id]", "[token]"), array($name, $contact, $token), $template);

                    $this->mailer->setFrom('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addReplyTo('welkom@power-ed.nl', 'Power ED');
                    $this->mailer->addAddress($postData->email);
                    $this->mailer->addBCC("dave@power-ed.nl");

                    $this->mailer->Subject = 'Welkom op Power-ED!';
                    $this->mailer->msgHTML($message);

                    if ($this->mailer->send()) {
                        return $contact;
                    } else {
                        return [
                            "response" => "Er is iets fout gelopen, contacteer een administrator.",
                            "success" => false
                        ];
                    }
                } else {
                    return [
                        "response" => "Er is iets fout gelopen, probeer opnieuw.",
                        "success" => false
                    ];
                }
            } else {
                return [
                    "response" => "Het opgegeven e-mail adres is reeds in gebruik.",
                    "success" => false
                ];
            }
        }

        public function updatePermission($postData) {
            if ($this->model->updatePermissionSetId($postData)) {
                if ($_SESSION['id'] == $postData->userId) {
                    $_SESSION['permission'];
                }
                Redirect::location('admin/users/view/' . $postData->userId);
            }
        }

        public function updateUser($postData) {
            if ($this->model->updatePersonalDetails($postData)) {
                Redirect::location('admin/users/view/' . $postData->userId);
            }
        }

        public function setCompanyDetails($postData) {
            if ($postData->companyId) {
                // Update existing record
                if ($this->model->updateCompanyDetails($postData)) {
                    Redirect::location('admin/users/view/' . $postData->userId);
                }
            } else {
                // Create new record
                if ($this->model->saveCompanyDetails($postData)) {
                    Redirect::location('admin/users/view/' . $postData->userId);
                }
            }
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }