<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;
    use \App\handlers\Validation as Validate;
    use \App\handlers\Mailer;
    use \App\handlers\Redirect;
    use \App\handlers\Cookie;

    class SupportController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $id;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->id = isset($user_id) ? $user_id : $_SESSION['id'];
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->view = new View();
            
            $this->init_with_id();
        }

        public function init_with_id() {
            $this->user_fields = $this->model->get_user_fields($this->id);
        }

        public function confirm_reset() {
            return $this->view->render('views/support/confirm-reset-password.php');
        }
        
        public function forgot_password() {
            return $this->view->render('views/support/forgot-password.php');
        }

        public function recovery_confirmation() {
            return $this->view->render('views/support/recovery-confirmation.php');
        }

        public function reset_password() {
            return $this->view->render('views/support/reset-password.php');
        }

        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }