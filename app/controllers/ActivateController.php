<?php

    namespace App\controllers;
    
    use \App\handlers\Views as View;
    use \App\models\User_model as User;

    class ActivateController {

        private $model;
        private $view;
        private $request;
        
        public $error;
        public $success;
        public $user_fields;

        public function __construct($user_id = false, $request = null) {
            $this->request = isset($request) ? $request : false;
            $this->model = new User();
            $this->view = new View();
        }

        public function account() {

            $data = new \StdClass();

            $userId = $this->request[2];
            $token = $this->request[3];

            $data->userId = $userId;

            if (!isset($this->request[2]) || !isset($this->request[3])) {
                $data->message = 'Er is een fout opgetreden, probeer later opnieuw';
                $data->success = false;
            } else {
                $activated = $this->model->activateUser($userId, $token);

                if ($activated) {
                    $data->message = 'Uw account is geactiveerd. <br /> Gelieve een wachtwoord te kiezen.';
                    $data->success = true;
                    $data->retry = true;
                } else {
                    $data->message = 'Deze request is ongeldig. <br /> Contacteer jouw administrator';
                    $data->success = false;
                }
            }

            $this->view->render('views/support/activated.php', $data);
        }
        
        public function get_error_message() {
            return $this->error;
        }

        public function get_success_message() {
            return $this->success;
        }

    }