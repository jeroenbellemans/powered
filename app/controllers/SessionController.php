<?php

    namespace  App\controllers;
    
    use \App\handlers\Database as DB;
    use \App\handlers\Views as View;
    use \App\handlers\Redirect as Redirect;
    use \App\models\Home_model as Home;

    class SessionController {

        private $db;
        private $model;
        private $view;
        private $validation;

        public $id;
        public $error;
        public $success;

        public function __construct($user_id = null, $request = null) {
            $this->db = new DB();
            $this->view = new View();
            $this->model = new Home();
        }

        public static function session_required() {
            if (!isset($_SESSION['id'])) {
                Redirect::location('');
            }
        }

        public static function sign_out() {
            if (isset($_COOKIE['power_ed'])) {
                setcookie('power_ed', false); 
            }
            if (isset($_SESSION['id'])) {
                session_destroy();
                Redirect::location('');
            }
        }

    }