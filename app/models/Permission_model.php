<?php

    namespace App\models;

    use \App\handlers\Database as Database;
    use \App\handlers\Cookie;

    class Permission_model {

        private $table = 'permission_sets';
        public $db;

        public function __construct() {
            $db = new Database();
            $this->db = $db;
        }

        public function getAllPermissions() {
            $query = "SELECT * FROM ". $this->table;
            $result = $this->db->get_results($query);

            return ($result) ? $result : false;
        }

        public function getPermissionsById($permissionId) {
            $query = "SELECT * FROM ". $this->table . " WHERE `id` = :id";
            $result = $this->db->get_results($query, array(
                ":id" => $permissionId
            ));

            return ($result) ? $result : false;
        }

        public function updatePermissionsForRole($permissionString, $permissionSetId) {
            $query = "UPDATE " . $this->table . " SET `permissions` = :permissionString WHERE `id` = :permissionSetId";
            $result = $this->db->update($query, array(
                ":permissionString" => $permissionString,
                ":permissionSetId" => $permissionSetId
            ));

            return ($result) ? $result : false;
        }
    }