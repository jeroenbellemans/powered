<?php

    namespace App\models;

    use \App\handlers\Database as Database;
    use \App\handlers\Cookie;

    class User_model {

        private $table = 'users';
        private $permissions_table = 'permission_sets';
        private $company_table = 'companies';
        public $db;

        public function __construct() {
            $db = new Database();
            $this->db = $db;
        }

        public function get_user_fields($user_id) {
            $query = "SELECT * FROM " . $this->table . " WHERE `id` = :user_id";
            $results = $this->db->get_results($query, array(
                ":user_id"  =>  $user_id
            ));

            if ($results) {

                $user_fields = new \StdClass();
                
                foreach ($results as $result) {

                    $query = "SELECT * FROM " . $this->permissions_table . " WHERE `id` = :permission_set_id";
                    $results2 = $this->db->get_results($query, array(
                        ":permission_set_id"    =>  $result->permission_set_id
                    ));

                    $user_fields->email = $result->email;
                    $user_fields->firstname = $result->firstname;
                    $user_fields->insertion = $result->insertion;
                    $user_fields->lastname = $result->lastname;

                    foreach ($results2 as $result2) {
                        $user_fields->permission = new \StdClass();
                        $user_fields->permission->id = $result2->id;
                        $user_fields->permission->collection = $result2->permissions;
                        $user_fields->permission->label = $result2->label;
                    }
                }

                return $user_fields;
            }
        }

        public function fetchUserInfoByEmail($email) {
            $query = "SELECT * FROM " . $this->table . " WHERE `email` = :email";
            $results = $this->db->get_results($query, array(
                ":email"  =>  $email
            ));

            if ($results) {

                $user_fields = new \StdClass();
                
                foreach ($results as $result) {

                    $user_fields->id = $result->id;
                    $user_fields->email = $result->email;
                    $user_fields->firstname = $result->firstname;
                    $user_fields->insertion = $result->insertion;
                    $user_fields->lastname = $result->lastname;
                }

                return $user_fields;
            }
        }

        public function combination_exists($email, $password) {
            $response = new \StdClass();

            $query = "SELECT * FROM " . $this->table . " WHERE `email` = :email AND `is_deleted` = :deleted";
            $results = $this->db->get_results($query, array(
                ":email"    =>  $email,
                ":deleted"  => '0'
            ));

            if ($results) {
                foreach ($results as $result) {
                    if (password_verify($password, $result->password)) {
                        $_SESSION['id'] = $result->id;
                        $_SESSION['firstname'] = $result->firstname;
                        $_SESSION['lastname'] = $result->lastname;
                        
                        $response->id = $_SESSION['id'];
                        $response->email = $result->email;
                        $response->success = true;

                    } else {
                        $response->success = false;
                    }
                }
            }

            return $response;
        }

        public function checkSecuredHash($hash) {

            $performAutomaticLogin = false;

            $query = "SELECT * FROM " . $this->table . " WHERE `auth_hash` = :hash";
            $results = $this->db->get_results($query, array(
                ":hash"    =>  $hash
            ));
            
            if ($results) {
                foreach ($results as $result) {
                    if ($hash == $result->auth_hash) {
                        $_SESSION['id'] = $result->id;
                        $_SESSION['firstname'] = $result->firstname;
                        $_SESSION['lastname'] = $result->lastname;

                        $postData = new \StdClass();
                        $postData->password = $result->password;
                        $postData->email = $result->email;
                        

                        // Cookie::refreshCookie($postData);

                        $performAutomaticLogin = true;
                    }
                }
            }
            
            return $performAutomaticLogin;
        }

        public function emailExistsInSystem($email) {
            $query = "SELECT `email` FROM ". $this->table . " WHERE `email` = :email";
            $result = $this->db->get_results($query, array(
                ":email"    =>  $email
            ));

            return ($result) ? $result : false;
        }

        public function tokenCombinationExists($userId, $token) {
            $query = "SELECT `id` FROM ". $this->table . " WHERE `id` = :id AND `token` = :token";
            $result = $this->db->get_results($query, array(
                ":id" => $userId,
                ":token" => $token
            ));

            return ($result) ? $result : false;
        }

        public function getAccountInformation($userId = null) {
            $query = "SELECT * FROM ". $this->table . " WHERE `id` = :userId";
            $result = $this->db->get_results($query, array(
                ":userId"    =>  ($userId == null) ? $_SESSION['id'] : $userId
            ));

            return ($result) ? $result : '5';
        }

        public function getUserInfo($userId) {
            $query = "SELECT * FROM ". $this->table . " WHERE `id` = :userId";
            $result = $this->db->get_results($query, array(
                ":userId"    =>  $userId
            ));

            if ($result) {

                $user_fields = new \StdClass();
                
                foreach ($result as $result) {

                    $query = "SELECT * FROM " . $this->permissions_table . " WHERE `id` = :permission_set_id";
                    $results2 = $this->db->get_results($query, array(
                        ":permission_set_id"    =>  $result->permission_set_id
                    ));

                    $user_fields->personal = new \StdClass();
                    $user_fields->personal->id = $result->id;
                    $user_fields->personal->email = $result->email;
                    $user_fields->personal->firstname = $result->firstname;
                    $user_fields->personal->insertion = $result->insertion;
                    $user_fields->personal->lastname = $result->lastname;
                    $user_fields->personal->phone = $result->phone;
                    $user_fields->personal->street = $result->street;
                    $user_fields->personal->number = $result->number;
                    $user_fields->personal->city = $result->city;
                    $user_fields->personal->zipcode = $result->zipcode;
                    $user_fields->personal->country = $result->country;
                    $user_fields->personal->token = $result->token;

                    foreach ($results2 as $result2) {
                        $user_fields->permission = new \StdClass();
                        $user_fields->permission->id = $result2->id;
                        $user_fields->permission->collection = $result2->permissions;
                        $user_fields->permission->label = $result2->label;
                    }
                }

                return $user_fields;
            }
        }

        public function createContact($postData) {
            $query = "INSERT INTO " . $this->table . " (`firstname`, `lastname`, `insertion`, `email`, `parent_id`, `permission_set_id`, `token`) VALUES (:firstname, :lastname, :insertion, :email, :parentId, :permissionSetId, :token)";
            $result = $this->db->insert($query, array(
                ":firstname" => $postData->firstname,
                ":lastname" => $postData->lastname,
                ":insertion" => $postData->insertion,
                ":email" => $postData->email,
                ":parentId" => $postData->parentId,
                ":permissionSetId" => '4',
                ":token" => $postData->token
            ));

            return $result;
        }

        public function createAccount($postData) {
            $query = "INSERT INTO " . $this->table . " (`firstname`, `lastname`, `insertion`, `email`, `permission_set_id`, `token`) VALUES (:firstname, :lastname, :insertion, :email, :permissionSetId, :token)";
            $result = $this->db->insert($query, array(
                ":firstname" => $postData->firstname,
                ":lastname" => $postData->lastname,
                ":insertion" => $postData->insertion,
                ":email" => $postData->email,
                ":permissionSetId" => '3',
                ":token" => $postData->token
            ));

            return $result;
        }

        public function updateUser($hash, $postData) {
            $query = "UPDATE " . $this->table . " SET `auth_hash` = :hash WHERE `email` = :email";
            $result = $this->db->update($query, array(
                ":hash"     => $hash,
                ":email"    => $postData->email
            ));

            return $result;
        }

        public function updateUserDetails($postData) {
            $query = "UPDATE " . $this->table . " SET `firstname` = :firstname, `insertion` = :insertion, `lastname` = :lastname, `email` = :email, `street` = :street, `number` = :number, `city` = :city, `zipcode` = :zipcode, `country` = :country WHERE `id` = :userId";
            $result = $this->db->update($query, array(
                ":firstname"    => $postData->firstname,
                ":insertion"    => $postData->insertion,
                ":lastname"     => $postData->lastname,
                ":email"        => $postData->email,
                ":street"       => $postData->street,
                ":number"       => $postData->number,
                ":city"         => $postData->city,
                ":zipcode"      => $postData->zipcode,
                ":country"      => $postData->country,
                ":userId"       => $postData->id
            ));

            return $result;
        }

        public function activateUser($userId, $token) {

            if ($this->tokenCombinationExists($userId, $token)) {
                $query = "UPDATE " . $this->table . " SET `status` = :status WHERE `id` = :id AND `token` = :token";
                $result = $this->db->update($query, array(
                    ":status" => '1',
                    ":id" => $userId,
                    ":token" => $token
                ));
                return $result;
            } else {
                return false;
            }
        }

        public function setPassword($postData) {
            $query = "UPDATE " . $this->table . " SET `password` = :password WHERE `id` = :id";
            $result = $this->db->update($query, array(
                ":password" => password_hash($postData->password, PASSWORD_BCRYPT),
                ":id" => $postData->id,
            ));

            return $result;
        }

        public function validToken($userId, $token) {
            $query = "SELECT `id` FROM " . $this->table . " WHERE `id` = :userId AND `token` = :token";
            $results = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":token" => $token
            ));

            return ($results) ? true : false;
        }

        public function updateUserWithToken($token, $userId) {
            $query = "UPDATE " . $this->table . " SET `token` = :token WHERE `id` = :id";
            $result = $this->db->update($query, array(
                ":token" => $token,
                ":id" => $userId,
            ));

            return $result;
        }

        public function updatePermissionSetId($postData) {
            $query = "UPDATE " . $this->table . " SET `permission_set_id` = :permission WHERE `id` = :userId";
            $result = $this->db->update($query, array(
                ":permission" => $postData->role,
                ":userId" => $postData->userId
            ));

            return $result;
        }

        public function updatePersonalDetails($postData) {
            $query = "UPDATE " . $this->table . " SET `email` = :email, `firstname` = :firstname, `insertion` = :insertion, `lastname` = :lastname, `phone` = :phone, `street` = :street, `number` = :number, `city` = :city, `zipcode` = :zipcode, `country` = :country WHERE `id` = :userId";
            $result = $this->db->update($query, array(
                ":email" => $postData->email,
                ":firstname" => $postData->firstname,
                ":insertion" => $postData->insertion,
                ":lastname" => $postData->lastname,
                ":phone" => $postData->phone,
                ":street" => $postData->street,
                ":number" => $postData->number,
                ":city" => $postData->city,
                ":zipcode" => $postData->zipcode,
                ":country" => $postData->country,
                ":userId" => $postData->userId
            ));

            return $result;
        }

        public function saveCompanyDetails($postData) {
            $query = "INSERT INTO " . $this->company_table . " (`user_id`, `company_name`, `kvk_number`, `sbi_code`, `sbi_code_description`, `establishment_pc`, `establishment_street`, `establishment_number`, `establishment_city`, `establishment_country`, `establishment_code`, `main_branch`) VALUES (:userId, :companyName, :kvk, :sbi, :sbidescription, :postalCode, :street, :number, :city, :country, :code, :mainBranch)";
            $result = $this->db->insert($query, array(
                ":userId" => (isset($postData->userId)) ? $postData->userId : $_SESSION['id'],
                ":companyName" => $postData->businessName,
                ":kvk" => $postData->kvkNumber,
                ":sbi" => $postData->sbiCode,
                ":sbidescription" => $postData->sbiCodeDescription,
                ":postalCode" => $postData->postalCode,
                ":street" => $postData->street,
                ":number" => $postData->number,
                ":city" => $postData->city,
                ":country" => $postData->country,
                ":code" => $postData->code,
                ":mainBranch" => ($postData->isMainBranch) ? '1' : '0'
            ));

            return $result;

        }

        public function updateCompanyDetails($postData) {
            $query = "UPDATE " . $this->company_table . " SET `user_id` = :userId, `company_name` = :companyName, `kvk_number` = :kvk, `sbi_code` = :sbi, `sbi_code_description` = :sbidescription, `establishment_pc` = :postalCode, `establishment_street` = :street, `establishment_number` = :number, `establishment_city` = :city, `establishment_country` = :country, `establishment_code` = :code, `main_branch` = :mainBranch WHERE `id` = :companyId";
            $result = $this->db->update($query, array(
                ":userId" => (isset($postData->userId)) ? $postData->userId : $_SESSION['id'],
                ":companyName" => $postData->businessName,
                ":kvk" => $postData->kvkNumber,
                ":sbi" => $postData->sbiCode,
                ":sbidescription" => $postData->sbiCodeDescription,
                ":postalCode" => $postData->postalCode,
                ":street" => $postData->street,
                ":number" => $postData->number,
                ":city" => $postData->city,
                ":country" => $postData->country,
                ":code" => $postData->code,
                ":mainBranch" => ($postData->isMainBranch) ? '1' : '0',
                ":companyId"    => $postData->companyId
            ));

            return $result;
        }

        public function getCompanyDetails($userId, $mainBranch = '1') {
            $query = "SELECT * FROM " . $this->company_table . " WHERE `user_id` = :userId AND `main_branch` = :true";
            $result = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":true" => $mainBranch
            ));

            return ($result) ? $result : false;
        }

        public function getAllCompanyDetails($userId) {
            $query = "SELECT * FROM " . $this->company_table . " WHERE `user_id` = :userId";
            $result = $this->db->get_results($query, array(
                ":userId" => $userId
            ));

            return ($result) ? $result : false;
        }

        public function saveCompanyDetailsField($input, $field, $companyId) {
            
            $query = "UPDATE " . $this->company_table . " SET `" . $field . "` = :value WHERE `id` = :companyId";
            $result = $this->db->update($query, array(
                ":value" => $input,
                ":companyId" => $companyId
            ));

            return $result;
        }

        public function createSubCompany($postData) {
            $query = "INSERT INTO " . $this->company_table . " (`user_id`, `company_name`, `establishment_pc`, `establishment_street`, `establishment_number`, `establishment_city`, `establishment_country`, `main_branch`, `fte`, `sqft`) VALUES (:userId, :companyName, :postalCode, :street, :number, :city, :country, :mainBranch, :fte, :sqft)";
            $result = $this->db->insert($query, array(
                ":userId" => $_SESSION['id'],
                ":companyName" => $postData->companyName,
                ":postalCode" => $postData->postalCode,
                ":street" => $postData->street,
                ":number" => $postData->number,
                ":city" => $postData->city,
                ":country" => $postData->country,
                ":mainBranch" => $postData->mainBranch,
                ":fte" => $postData->fte,
                ":sqft" => $postData->sqft
            ));

            return $result;
        }

        public function getAllContactsForOwner($userId = null) {
            $query = "SELECT * FROM ". $this->table . " WHERE `parent_id` = :userId";
            $result = $this->db->get_results($query, array(
                ":userId"    =>  ($userId == null) ? $_SESSION['id'] : $userId
            ));

            return ($result) ? $result : false;
        }

        public function getAllUsers() {
            $query = "SELECT * FROM ". $this->table . " WHERE `is_deleted` = :deleted";
            $result = $this->db->get_results($query, array(
                "deleted"   =>  '0'
            ));

            return ($result) ? $result : false;
        }

        public function getAllClients() {
            $query = "SELECT
                        `client`.`id` as client_id,
                        `client`.`company_name`,
                        `client`.`kvk_number`,
                        `client`.`is_deleted` as `client_is_deleted`,
                        `user`.id as `user_id`,
                        `user`.`firstname`,
                        `user`.`lastname`,
                        `user`.`insertion`,
                        `user`.`is_deleted` as `user_is_deleted`
                      FROM " . $this->company_table . " as `client`
                      LEFT JOIN " . $this->table . " as `user` ON `user`.`id` = `client`.`user_id`
                      WHERE `client`.`is_deleted` = :deleted";
            $result = $this->db->get_results($query, array(
                ":deleted"  =>  '0'
            ));

            return ($result) ? $result : false;
        }

        public function getClient($clientId) {
            $query = "SELECT
                        `client`.`id` as client_id,
                        `client`.`company_name`,
                        `client`.`kvk_number`,
                        `client`.`establishment_pc`,
                        `client`.`establishment_street`,
                        `client`.`establishment_city`,
                        `client`.`establishment_country`,
                        `client`.`establishment_number`,
                        `client`.`sbi_code_description`,
                        `user`.id as `user_id`,
                        `user`.`firstname`,
                        `user`.`lastname`,
                        `user`.`insertion`
                      FROM " . $this->company_table . " as `client`
                        LEFT JOIN " . $this->table . " as `user` ON `user`.`id` = `client`.`user_id`
                      WHERE `client`.`id` = :clientId";
            $result = $this->db->get_results($query, array(
                ":clientId" => $clientId
            ));

            return ($result) ? $result : false;
        }

        public function getAllUsersForOwner($user_id) {
            $query = "SELECT * FROM " . $this->table . " WHERE `id` = :userId OR `parent_id` = :userIdParent";
            $result = $this->db->get_results($query, array(
                ":userId" => $user_id,
                ":userIdParent" => $user_id
            ));

            return ($result) ? $result : false;
        }

        public function getDefaultCompanyId($user_id) {
            $query = "SELECT `id` FROM `companies` WHERE `user_id` = :userId AND `main_branch` = :mainBranch";
            $result = $this->db->get_results($query, array(
                ":userId" => $user_id,
                ":mainBranch" => '1'
            ));

            return ($result) ? $result[0]->id : false;
        }

        public function deleteUser($postData) {
            $query = "UPDATE " . $this->table . " SET `is_deleted` = :deleted WHERE `id` = :userId";
            $result = $this->db->update($query, array(
                ":deleted"    =>  '1',
                ":userId"     => $postData
            ));

            return $result;
        }

        public function deleteClient($postData) {
            $query = "UPDATE `companies` SET `is_deleted` = :deleted WHERE `id` = :clientId";
            $result = $this->db->update($query, array(
                ":deleted"    =>  '1',
                ":clientId"     => $postData
            ));

            return $result;
        }
    }