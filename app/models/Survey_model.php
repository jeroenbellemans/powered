<?php

    namespace App\models;

    use \App\handlers\Database as Database;
    use \App\models\User_model as User;
    use \App\handlers\Cookie;

    class Survey_model {

        private $table = 'module';
        private $sdg_goals = 'goal';
        public $db;
        private $user_model;

        public function __construct() {
            $db = new Database();
            $this->user_model = new User();
            $this->db = $db;
        }

        public function fetchModule($module) {
            $query = "SELECT * FROM " . $this->table . " WHERE `alias` = :module";
            $results = $this->db->get_results($query, array(
                ":module"  =>  $module
            ));

            return $results ? $results : false;
        }

        public function fetchModules() {
            $query = "SELECT * FROM " . $this->table;
            $results = $this->db->get_results($query);

            return $results ? $results : false;
        }

        public function fetchSubmodule($moduleId) {
            $query = "SELECT * FROM `submodule` WHERE `module_id` = :moduleId";
            $results = $this->db->get_results($query, array(
                ":moduleId"  =>  $moduleId
            ));

            return $results ? $results : false;
        }

        public function fetchCategories($moduleId) {
            $query = "SELECT * FROM `category` WHERE `module_id` = :moduleId";
            $results = $this->db->get_results($query, array(
                ":moduleId"  =>  $moduleId
            ));

            return $results ? $results : false;
        }

        public function fetchParentQuestionsForCategory($categoryId) {
            $query = "SELECT * FROM `category_question` WHERE `category_id` = :categoryId AND `is_parent` = :true";
            $results = $this->db->get_results($query, array(
                ":categoryId"  =>  $categoryId,
                ":true" => '1'
            ));

            return $results ? $results : false;
        }

        public function fetchAllQuestions() {
            $query = "SELECT * FROM `category_question`";
            $results = $this->db->get_results($query);

            return $results ? $results : false;
        }

        public function fetchAnswersForQuestions() {
            $query = "SELECT * FROM `category_answer`";
            $results = $this->db->get_results($query);

            return $results ? $results : false;
        }

        public function fetchQuestionIntroForSubmodule($submoduleId) {
            $query = "SELECT `id`, `excerpt` FROM `question_intro` WHERE submodule_id = :submoduleId ORDER BY `sequence` ASC";
            $results = $this->db->get_results($query, array(
                ":submoduleId"  =>  $submoduleId
            ));

            return $results ? $results : false;
        }

        public function fetchOpenQuestionForSubmodule($submoduleId) {
            $query = "SELECT * FROM `open_question_description` WHERE submodule_id = :submoduleId 
                        AND 
                        `version` IN (SELECT MAX(qd.`version`) FROM `open_question_description` AS qd GROUP BY `submodule_id`)";
            $results = $this->db->get_results($query, array(
                ":submoduleId"  =>  $submoduleId
            ));

            return $results ? $results : false;
        }

        // The following query will fetch the latest version of a question
        public function fetchQuestionsForQuestionIntro($questionIntroId) {
            $query =   "SELECT  
                            qd.original_id, 
                            qd.id as question_description_id,
                            qd.description AS question_description,
                            q.id AS id,
                            q.type AS question_type,
                            q.has_answer_set_id AS has_answer_set_id,
                            q.answer_set_id AS answer_set_id,
                            q.question_intro_id,
                            qd.`version` as `version`
                        FROM question_description AS qd
                            LEFT JOIN `question` q
                                ON q.question_description_id = qd.id
                        WHERE
                            q.question_intro_id = :questionIntroId
                        AND 
                            qd.`version` IN (SELECT MAX(qdd.`version`) FROM `question_description` AS qdd GROUP BY `question_intro_id`)";
            $results = $this->db->get_results($query, array(
                ":questionIntroId"  =>  $questionIntroId
            ));

            return $results ? $results : false;
        }

        // The following query will fetch the latest version of a question
        public function fetchAnsweredQuestionsForQuestionIntro($questionIntroId, $moduleId, $surveyId) {
            $query =   "SELECT
                            sa.id as answered_id,
                            qd.original_id, 
                            qd.id as question_description_id,
                            qd.description AS question_description,
                            q.id AS id,
                            q.type AS question_type,
                            q.has_answer_set_id AS has_answer_set_id,
                            q.answer_set_id AS answer_set_id,
                            q.question_intro_id,
                            qd.`version` as `version`
                        FROM survey_attendees as satt
                            LEFT JOIN survey_answers as sa
                                ON sa.survey_id = satt.id
                            LEFT JOIN question_description as qd
                                ON qd.id = sa.question_description_id
                            LEFT JOIN `question` q
                                ON q.question_description_id = qd.id
                        WHERE
                            q.question_intro_id = :questionIntroId
                        AND
                            satt.module_id = :moduleId
                        AND
                            sa.survey_id = :surveyId";
            $results = $this->db->get_results($query, array(
                ":questionIntroId" => $questionIntroId,
                ":moduleId" => $moduleId,
                ":surveyId" => $surveyId
            ));

            return $results ? $results : false;
        }

        // The following query will fetch the latest version of an answer (part)
        public function fetchAnswersForQuestion($answerSetId) {
            $query = "SELECT *
                        FROM answer_sets
                        WHERE
                            set_id = :answerSetId
                        AND 
                            `version` IN (SELECT MAX(`version`) FROM `answer_sets`)";
            $results = $this->db->get_results($query, array(
                ":answerSetId"  =>  $answerSetId
            ));

            return $results ? $results : false;
        }

        public function fetchAnswersForModuleAndUser($moduleId, $userId) {
            $query = "SELECT
                            sa.id,
                            sa.answer_set_id,
                            sa.review_answer_set_id,
                            sa.question_description_id
                        FROM question_description AS qd
                            LEFT JOIN `question` q
                                ON q.question_description_id = qd.id
                            LEFT JOIN `question_intro` as qi
                                    ON qi.id = qd.question_intro_id
                        LEFT JOIN `submodule` as sm
                            ON sm.id = qi.submodule_id
                            LEFT JOIN `module` as m
                                ON m.id = sm.module_id
                            LEFT JOIN `survey_answers` as sa
                                ON sa.question_description_id = qd.id
                        WHERE
                            m.id = :moduleId
                        AND
                            sa.user_id = :userId";
        
            $results = $this->db->get_results($query, array(
                ":moduleId" => $moduleId,
                ":userId" => $userId
            ));

            return $results ? $results : false;
        }

        public function fetchOpenQuestionAnswersForSurveyAndUser($surveyId, $userId) {
            $query = "SELECT * FROM `open_question_answers` WHERE `user_id` = :userId AND `survey_id` = :surveyId";
            $results = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":surveyId" => $surveyId
            ));

            return $results ? $results : false;
        }

        public function incomplete_survey($module) {

            $modules = $this->fetchModule($module);

            foreach ($modules as $module) {

                $query = "SELECT * FROM `survey_attendees` WHERE `user_id` = :userId AND `module_id` = :moduleId AND `completed` = :incomplete";
                $results = $this->db->get_results($query, array(
                    ":userId" => $_SESSION['id'],
                    ":moduleId" => $module->id,
                    ":incomplete" => 0
                ));

                return ($results) ? $results : false;

            }

        }

        public function getSurveys($module, $completed) {

            $modules = $this->fetchModule($module);

            foreach ($modules as $module) {

                $query = "SELECT
                            `module`.`name` as `module_name`,
                            `module`.`id` as `module_id`,
                            `survey_attendees`.`id` as `survey_id`,
                            `survey_attendees`.`completion_date` as `module_completion_date`,
                            `survey_attendees`.`creation_date` as `module_creation_date`
                            FROM `survey_attendees`
                            LEFT JOIN `module` ON `module`.`id` = `survey_attendees`.`module_id`
                            WHERE `user_id` = :userId AND `module_id` = :moduleId AND `completed` = :incomplete";
                $results = $this->db->get_results($query, array(
                    ":userId" => $_SESSION['id'],
                    ":moduleId" => $module->id,
                    ":incomplete" => $completed
                ));

                return $results;

            }

        }

        public function getSurveyYearContext($module, $completed) {

            $modules = $this->fetchModule($module);

            foreach ($modules as $module) {

                $query = "SELECT * FROM `survey_attendees` LEFT JOIN `module` ON `module`.`id` = `survey_attendees`.`module_id` WHERE `user_id` = :userId AND `module_id` = :moduleId AND `completed` = :incomplete";
                $results = $this->db->get_results($query, array(
                    ":userId" => $_SESSION['id'],
                    ":moduleId" => $module->id,
                    ":incomplete" => $completed
                ));

                return !(empty($results[0]->subject_year));
            }
        }

        public function create_survey($module) {
            $modules = $this->fetchModule($module);

            foreach ($modules as $module) {
                $query = "INSERT INTO `survey_attendees` (`module_id`, `user_id`, `creation_date`, `completion_date`, `completed`) VALUES (:moduleId, :userId, :created, :completion_date, :completed)";
                $results = $this->db->insert($query, array(
                    ":moduleId" => $module->id,
                    ":userId" => $_SESSION['id'],
                    ":created" => time(),
                    ":completion_date" => 0,
                    ":completed" => '0'
                ));
                return ($results) ? true : false;
            }
        }

        public function createAnswersForQuestions($postData, $incompleteSurveyId) {

            $clearProcedure = true;
            $result = true;

            foreach ($postData as $question => $value) {
                if ($question != 'module') {

                    $question_description_id = explode("_", $question);

                    if ($question_description_id[0] == 'question') {
                        $question_description_id = $question_description_id[1];
                        $answer_set_id = $value;

                        $query = "INSERT INTO `survey_answers` (`question_description_id`, `answer_set_id`, `user_id`, `answer_date`, `survey_id`) VALUES (:questionDescriptionId, :answerSetId, :userId, :created, :surveyId)";
                        $result = $this->db->insert($query, array(
                            ":questionDescriptionId" => $question_description_id,
                            ":answerSetId" => ($answer_set_id == '-') ? null : $answer_set_id,
                            ":userId" => $_SESSION['id'],
                            ":created" => time(),
                            ":surveyId" => $incompleteSurveyId
                        ));
                    }
                    if ($question_description_id[0] == 'open-question') {
                        $open_question_id = $question_description_id[1];
                        $answer = $value;

                        $query2 = "INSERT INTO `open_question_answers` (`open_question_id`, `answer`, `user_id`, `answer_date`, `survey_id`) VALUES (:openQuestionId, :answer, :userId, :created, :surveyId)";
                        $result2 = $this->db->insert($query2, array(
                            ":openQuestionId" => $open_question_id,
                            ":answer" => $answer,
                            ":userId" => $_SESSION['id'],
                            ":created" => time(),
                            ":surveyId" => $incompleteSurveyId
                        ));
                    }

                    if (!$result || !$result2) {
                        $clearProcedure = false;
                    }
                }
            }

            if ($clearProcedure) {
                $query = "UPDATE `survey_attendees` SET `completed` = :completed, `completion_date` = :completedAt WHERE `module_id` = :moduleId AND `user_id` = :userId AND `completed` = '0'";
                $result = $this->db->update($query, array(
                    ":completed" => '1',
                    ":completedAt" => time(),
                    ":moduleId" => $_POST["module"],
                    ":userId" => $_SESSION["id"]
                ));

                return true;
            } else {
                return false;
            }
            

        }

        public function createAdminAnswersForQuestions($postData, $surveyId) {

            foreach ($postData as $question => $value) {
                if ($question != 'module') {

                    $question_description_id = explode("_", $question);

                    if ($question_description_id[0] == 'question') {
                        $answerId = $question_description_id[1];
                        $answer_set_id = $value;

                        $query = "UPDATE `survey_answers` SET `review_answer_set_id` = :answerSetId, `reviewer_id` = :userId, `review_answer_date` = :created WHERE `id` = :answerId";
                        $result = $this->db->update($query, array(
                            ":answerSetId" => ($answer_set_id == '-') ? null : $answer_set_id,
                            ":userId" => $_SESSION['id'],
                            ":created" => time(),
                            ":answerId" => $answerId
                        ));
                    }
                    if ($question_description_id[0] == 'open-question') {
                        $open_question_answer_id = $question_description_id[1];
                        $answer = $value;

                        $query2 = "UPDATE `open_question_answers` SET `reviewer_id` = :userId, `review_answer_date` = :created, `review_answer` = :answer WHERE `open_question_id` = :answerId AND `survey_id` = :surveyId";
                        $result2 = $this->db->update($query2, array(
                            ":userId" => $_SESSION['id'],
                            ":created" => time(),
                            ":answer" => $answer,
                            ":answerId" => $open_question_answer_id,
                            ":surveyId" => $surveyId
                        ));
                    }
                }
            }

            $query2 = "UPDATE `survey_attendees` SET `is_reviewed` = :reviewed WHERE `id` = :surveyId";
            $result2 = $this->db->update($query2, array(
                ":reviewed" => '1',
                ":surveyId" => $surveyId
            ));

            return true;

        }

        public function createContact($postData) {
            $query = "INSERT INTO " . $this->table . " (`firstname`, `lastname`, `insertion`, `email`, `parent_id`, `permission_set_id`, `token`) VALUES (:firstname, :lastname, :insertion, :email, :parentId, :permissionSetId, :token)";
            $result = $this->db->insert($query, array(
                ":firstname" => $postData->firstname,
                ":lastname" => $postData->lastname,
                ":insertion" => $postData->insertion,
                ":email" => $postData->email,
                ":parentId" => $postData->parentId,
                ":permissionSetId" => '4',
                ":token" => $postData->token
            ));

            return $result;
        }

        

        public function updatePersonalDetails($postData) {
            $query = "UPDATE " . $this->table . " SET `email` = :email, `firstname` = :firstname, `insertion` = :insertion, `lastname` = :lastname, `birthdate` = :birthdate, `street` = :street, `number` = :number, `city` = :city, `zipcode` = :zipcode, `country` = :country WHERE `id` = :userId";
            $result = $this->db->update($query, array(
                ":email" => $postData->email,
                ":firstname" => $postData->firstname,
                ":insertion" => $postData->insertion,
                ":lastname" => $postData->lastname,
                ":birthdate" => $postData->birthdate,
                ":street" => $postData->street,
                ":number" => $postData->number,
                ":city" => $postData->city,
                ":zipcode" => $postData->zipcode,
                ":country" => $postData->country,
                ":userId" => $postData->userId
            ));

            return $result;
        }

        public function saveSdgSelection($postData, $returnParam = false, $admin = false) {
            
            $errorWhileInserting = false;

            if (!$admin) {
                $currentIncompleteSurveyId = $this->incomplete_survey($postData->module);
                $currentIncompleteSurveyId = $currentIncompleteSurveyId[0]->id;

                foreach ($postData->sdg as $selected_value) {
                    $query = "INSERT INTO `selected_goals` (`user_id`, `survey_id`, `goal_id`) VALUES (:userId, :surveyId, :goalId)";
                    $result = $this->db->insert($query, array(
                        ":userId" => $_SESSION['id'],
                        ":surveyId" => $currentIncompleteSurveyId,
                        ":goalId" => $selected_value
                    ));

                    if (!$result) {
                        $errorWhileInserting = true;
                    }
                }

                if (!$errorWhileInserting) {
                    $query = "UPDATE `survey_attendees` SET `completed` = :completed, `completion_date` = :completedAt WHERE `module_id` = :moduleId AND `user_id` = :userId AND `completed` = '0'";
                    $result = $this->db->update($query, array(
                        ":completed" => '1',
                        ":completedAt" => time(),
                        ":moduleId" => $postData->moduleId,
                        ":userId" => $_SESSION["id"]
                    ));
    
                    return ($result) ? true : false;
                } else {
                    return false;
                }
            } else {
                if ($returnParam) {
                    $query = "DELETE FROM `selected_goals_reviewed` WHERE `survey_id` = :surveyId AND `reviewer_id` = :userId";
                    $this->db->delete($query, array(
                        ":userId" => $returnParam,
                        ":surveyId" => $postData->surveyId
                    ));

                    foreach ($postData->sdg as $selected_value) {
                        $query = "INSERT INTO `selected_goals_reviewed` (`reviewer_id`, `survey_id`, `review_goal_id`) VALUES (:userId, :surveyId, :goalId)";
                        $result = $this->db->insert($query, array(
                            ":userId" => $returnParam,
                            ":surveyId" => $postData->surveyId,
                            ":goalId" => $selected_value
                        ));

                    }

                    return ($result) ? true : false;
                }
            }

            return (!$errorWhileInserting) ? true : false;
        }

        public function getSdgSelection($surveyId, $userId) {
            $query = "SELECT * FROM `selected_goals` WHERE `user_id` = :userId AND `survey_id` = :survey_id";
            $results = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":survey_id" => $surveyId
            ));

            return $results;
        }

        public function getReviewedSdgSelection($surveyId, $userId) {
            $query = "SELECT * FROM `selected_goals_reviewed` WHERE `reviewer_id` = :userId AND `survey_id` = :survey_id";
            $results = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":survey_id" => $surveyId
            ));

            return $results;
        }

        public function fetchQuestionForAnswer($questionId) {
            $query = "SELECT * FROM `category_answer` WHERE `id` = :question_id";
            $results = $this->db->get_results($query, array(
                ":question_id" => $questionId
            ));

            return $results;
        }

        public function getAllQuestions() {
            $query = "SELECT * FROM `category_question`";
            $results = $this->db->get_results($query);

            return $results;
        }

        /******************************************************************************************
         * @description     This method returns the survey answers for a specific survey, based
         *                  upon the mode the user (normal or admin) is viewing the survey. This
         *                  can be in the overruling context or the normal view context. Based upon
         *                  the context, the query will vary.
         * @param           String surveyId
         *                  This is the ID of the survey
         * @param           Mixed mode ("original" or boolean "false")
         *                  This is the mode on which the survey is being called
         * @param           String userId
         *                  This is the ID of the user
         *****************************************************************************************/
        public function getAllSurveyAnswers($surveyId, $mode = false, $userId) {

            // First check for a review
            $reviewApplied = "SELECT is_reviewed FROM `survey_attendees` WHERE `id` = :survey_id AND `is_reviewed` = :reviewed";
            $reviewResults = $this->db->get_results($reviewApplied, array(
                ":survey_id" => $surveyId,
                ":reviewed"  => '1'
            ));

            // If no review is applied for a survey, select the normal fields
            if ($mode == 'original' || !$reviewResults) {
                $query = "SELECT 
                                `id` as `id`,
                                `survey_id` as `survey_id`,
                                `user_id` as `user_id`,
                                `parent_question_id` as `parent_question_id`,
                                `question_id` as `question_id`,
                                `input_value` as `input_value`,
                                `company_id` as `company_id`
                            FROM `footprint_answers` WHERE `user_id` = :userId AND `survey_id` = :survey_id";
            // Otherwise, select the reviewed fields for updated values
            } else {
                $query = "SELECT 
                            `id` as `id`,
                            `survey_id` as `survey_id`,
                            `user_id` as `user_id`,
                            `review_parent_question_id` as `parent_question_id`,
                            `review_question_id` as `question_id`,
                            `review_input_value` as `input_value`,
                            `company_id` as `company_id`,
                            `reviewer_id`
                        FROM `footprint_answers` WHERE `user_id` = :userId AND `survey_id` = :survey_id";
            }
            $results = $this->db->get_results($query, array(
                ":userId" => $userId,
                ":survey_id" => $surveyId
            ));

            return $results;
        }

        public function setYearForScan($postData, $surveyId) {
            $query = "UPDATE `survey_attendees` SET `subject_year` = :subjectYear WHERE `id` = :surveyId";
            $result = $this->db->update($query, array(
                ":subjectYear" => $postData->year,
                ":surveyId" => $surveyId
            ));

            return $result;
        }

        public function saveFootprintAnswers($postData) {
            $errorWhileInserting = false;

            $currentIncompleteSurveyId = $this->incomplete_survey($postData['module']);
            $currentIncompleteSurveyId = $currentIncompleteSurveyId[0]->id;

            foreach ($postData as $answer_path => $value) {
                $split_path = explode("_", $answer_path);


                if (isset($split_path[1])) {
                    $establishment = explode("@", $split_path[1]);
                    if (isset($establishment[1])) {
                        $submodule = explode("+", $establishment[1]);

                        $parentQuestionId = $establishment[0];
                        $establishmentId = (isset($submodule[0]) && $submodule[0] != '') ? $submodule[0] : $this->user_model->getDefaultCompanyId($_SESSION['id']);
                        $questionEquivalent = $this->getEquivalentForAnswer($parentQuestionId);

                        $submoduleId = $submodule[1];

                    } else {
                        $submodule = explode("+", $split_path[1]);

                        $parentQuestionId = $submodule[0];
                        $establishmentId = $this->user_model->getDefaultCompanyId($_SESSION['id']);
                        $questionEquivalent = $this->getEquivalentForAnswer($parentQuestionId);
                        $submoduleId = $submodule[1];
                    }

                    if (is_array($value)) {
                        foreach ($value as $id) {
                            $query = "INSERT INTO `footprint_answers` (`survey_id`, `user_id`, `parent_question_id`, `question_id`, `input_value`, `company_id`) VALUES (:surveyId, :userId, :parentQuestionId, :questionId, :input, :companyId)";
                            $result = $this->db->insert($query, array(
                                ":surveyId" => $currentIncompleteSurveyId,
                                ":userId" => $_SESSION['id'],
                                ":parentQuestionId" => $parentQuestionId,
                                ":questionId" => $id,
                                ":input" => null,
                                ":companyId" => $establishmentId
                            ));
            
                            if (!$result) {
                                $errorWhileInserting = true;
                            }
                        }
                    } else {
                        $query = "INSERT INTO `footprint_answers` (`survey_id`, `user_id`, `parent_question_id`, `question_id`, `input_value`, `company_id`) VALUES (:surveyId, :userId, :parentQuestionId, :questionId, :input, :companyId)";
                        $result = $this->db->insert($query, array(
                            ":surveyId" => $currentIncompleteSurveyId,
                            ":userId" => $_SESSION['id'],
                            ":parentQuestionId" => $parentQuestionId,
                            ":questionId" => null,
                            ":input" => $value,
                            ":companyId" => $establishmentId
                        ));

                        $query = "INSERT INTO `footprint_collection` (`user_id`, `company_id`, `survey_id`, `submodule_id`, `input_value`, `question_id`, `equivalent_result`) VALUES (:userId, :companyId, :surveyId, :submoduleId, :input, :questionId, :equivalent_result)";
                        $result = $this->db->insert($query, array(
                            ":userId" => $_SESSION['id'],
                            ":companyId" => $establishmentId,
                            ":surveyId" => $currentIncompleteSurveyId,
                            ":submoduleId" => $submoduleId,
                            ":input" => $value,
                            ":questionId" => $parentQuestionId,
                            ":equivalent_result" => ($value * $questionEquivalent->equivalent)
                        ));
        
                        if (!$result) {
                            $errorWhileInserting = true;
                        }
                    }

                }
            }
            
            if (!$errorWhileInserting) {
                $query = "UPDATE `survey_attendees` SET `completed` = :completed, `completion_date` = :completedAt WHERE `id` = :surveyId";
                $result = $this->db->update($query, array(
                    ":completed" => '1',
                    ":completedAt" => time(),
                    ":surveyId" => $currentIncompleteSurveyId
                ));

                return ($result) ? true : false;
            } else {
                return false;
            }

            return (!$errorWhileInserting) ? true : false;
        }

        public function updateFootprintAnswers($postData) {
            $errorWhileInserting = false;

            $originalOwner = $this->getOwnerOfSurvey($postData['surveyId']);
            $surveyId = isset($postData['surveyId']) ? $postData['surveyId'] : false;

            // Delete results from collection as we want this to be the unique for reporting
            $query = "DELETE FROM `footprint_collection` WHERE `survey_id` = :surveyId";
            $result = $this->db->delete($query, array(
                ":surveyId" => $surveyId
            ));

            foreach ($postData as $answer_path => $value) {
                $split_path = explode("_", $answer_path);

                if (isset($split_path[1])) {

                    $targetId = explode("/", $split_path[0])[0];
                    $establishment = explode("@", $split_path[1]);

                    if (isset($establishment[1])) {
                        $submodule = explode("+", $establishment[1]);

                        $parentQuestionId = $establishment[0];
                        $establishmentId = (isset($submodule[0]) && $submodule[0] != '') ? $submodule[0] : $this->user_model->getDefaultCompanyId($originalOwner);
                        $questionEquivalent = $this->getEquivalentForAnswer($parentQuestionId);

                        $submoduleId = $submodule[1];

                    } else {
                        $submodule = explode("+", $split_path[1]);

                        $parentQuestionId = $submodule[0];
                        $questionEquivalent = $this->getEquivalentForAnswer($parentQuestionId);
                        $establishmentId = $this->user_model->getDefaultCompanyId($originalOwner);
                        $submoduleId = $submodule[1];
                    }

                    if (is_array($value)) {
                        if (isset($targetId)) {
                            foreach ($value as $id) {
                                $query = "UPDATE `footprint_answers` SET `reviewer_id` = :reviewer, `review_parent_question_id` = :parentQuestionId, `review_question_id` = :questionId, `review_input_value` = :input WHERE `id` = :targetId";
                                $result = $this->db->insert($query, array(
                                    ":reviewer" => $_SESSION['id'],
                                    ":parentQuestionId" => $parentQuestionId,
                                    ":questionId" => $id,
                                    ":input" => null,
                                    ":targetId" => $targetId
                                ));
                            }
                        } else {
                            foreach ($value as $id) {
                                $query = "INSERT INTO `footprint_answers` (`survey_id`, `user_id`, `parent_question_id`, `question_id`, `input_value`, `company_id`) VALUES (:surveyId, :userId, :parentQuestionId, :questionId, :input, :companyId)";
                                $result = $this->db->insert($query, array(
                                    ":surveyId" => $surveyId,
                                    ":userId" => $_SESSION['id'],
                                    ":parentQuestionId" => $parentQuestionId,
                                    ":questionId" => $id,
                                    ":input" => null,
                                    ":companyId" => $establishmentId
                                ));
                
                                if (!$result) {
                                    $errorWhileInserting = true;
                                }
                            }
                        }
                    } else {

                        if (isset($targetId)) {
                            $query = "UPDATE `footprint_answers` SET `reviewer_id` = :userId, `review_parent_question_id` = :parentQuestionId, `review_question_id` = :questionId, `review_input_value` = :input WHERE `id` = :targetId";
                            $result = $this->db->insert($query, array(
                                ":userId" => $_SESSION['id'],
                                ":parentQuestionId" => $parentQuestionId,
                                ":questionId" => null,
                                ":input" => $value,
                                ":targetId" => $targetId
                            ));
                        } else {
                            $query = "INSERT INTO `footprint_answers` (`survey_id`, `user_id`, `parent_question_id`, `question_id`, `input_value`, `company_id`) VALUES (:surveyId, :userId, :parentQuestionId, :questionId, :input, :companyId)";
                            $result = $this->db->insert($query, array(
                                ":surveyId" => $surveyId,
                                ":userId" => $_SESSION['id'],
                                ":parentQuestionId" => $parentQuestionId,
                                ":questionId" => null,
                                ":input" => $value,
                                ":companyId" => $establishmentId
                            ));
                        }

                        $query = "INSERT INTO `footprint_collection` (`user_id`, `company_id`, `survey_id`, `submodule_id`, `input_value`, `question_id`, `equivalent_result`) VALUES (:userId, :companyId, :surveyId, :submoduleId, :input, :questionId, :equivalent_result)";
                        $result = $this->db->insert($query, array(
                            ":userId" => $_SESSION['id'],
                            ":companyId" => $establishmentId,
                            ":surveyId" => $surveyId,
                            ":submoduleId" => $submoduleId,
                            ":input" => $value,
                            ":questionId" => $parentQuestionId,
                            ":equivalent_result" => ($value * $questionEquivalent->equivalent)
                        ));
                    }
                }
            }
            
            if (!$errorWhileInserting) {
                $query = "UPDATE `survey_attendees` SET `is_reviewed` = :reviewed WHERE `id` = :surveyId";
                $result = $this->db->update($query, array(
                    ":reviewed" => '1',
                    ":surveyId" => $surveyId
                ));

                return ($result) ? true : false;

            } else {
                return false;
            }

            return (!$errorWhileInserting) ? true : false;
        }

        // @todo: use latest version
        protected function getEquivalentForAnswer($answer) {

            $query = "SELECT `equivalent` FROM `category_answer_equivalents` WHERE `answer_id` = :answer";
            $results = $this->db->get_results($query, array(
                ":answer" => $answer
            ));

            return ($results) ? $results[0] : false;
        }

        public function getSustainableDevelopmentGoals() {

            $query = "SELECT * FROM " . $this->sdg_goals;
            $results = $this->db->get_results($query);

            return ($results) ? $results : false;
        }

        public function fetchPossibleAnswersForQuestionId($questionId) {
            $query = "SELECT * FROM `category_answer` WHERE `question_id` = :questionId";
            $results = $this->db->get_results($query, array(
                ":questionId" => $questionId,
            ));

            return $results;
        }
        
        public function fetchAnswersForSurveyId($surveyId, $userId = false) {
            $query = "SELECT * FROM `footprint_answers` WHERE `user_id` = :userId AND `survey_id` = :survey_id";
            $results = $this->db->get_results($query, array(
                ":userId" => ($userId) ? $userId : $_SESSION['id'],
                ":survey_id" => $surveyId
            ));

            return $results;
        }

        public function getSurveysForUser($userId) {
            $query = "SELECT 
                        `survey_attendees`.`id` as `survey_id`,
                        `survey_attendees`.`creation_date`,
                        `survey_attendees`.`completion_date`,
                        `survey_attendees`.`completed`,
                        `survey_attendees`.`subject_year`,
                        `survey_attendees`.`is_reviewed`,
                        `module`.`id` as `module_id`,
                        `module`.`name` as `module_name`,
                        `module`.`alias` as `module_alias`
                      FROM `survey_attendees`
                        LEFT JOIN `module` ON `module`.`id` = `survey_attendees`.`module_id`
                      WHERE `user_id` = :userId";
            $results = $this->db->get_results($query, array(
                ":userId" => $userId
            ));

            return $results;
        }

        public function getIsSurveyReviewed($surveyId) {
            $query = "SELECT is_reviewed FROM `survey_attendees` WHERE `id` = :surveyId";
            $results = $this->db->get_results($query, array(
                ":surveyId" => $surveyId
            ));

            return ($results[0]->is_reviewed == '1') ? true : false;
        }

        public function getOwnerOfSurvey($surveyId) {
            $query = "SELECT `user_id` FROM `survey_attendees` WHERE `id` = :surveyId";
            $results = $this->db->get_results($query, array(
                ":surveyId" => $surveyId
            ));

            return ($results) ? $results[0]->user_id : false;
        }
    }