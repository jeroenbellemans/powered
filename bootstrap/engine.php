<?php

    /// Enable/disable error logging
        //ini_set('display_errors', true);
        //error_reporting(E_ALL);

    /// Start the session
        session_start();

    /// Set timezone
        date_default_timezone_set('Europe/Brussels');

    /// Set locale
        setlocale(LC_ALL, 'nl_NL');

    /// Include the controller autoloader
        require 'autoloader.php';

    /// Include the configuration file
        require 'config.php';

    /// Include helper functions
        require 'form.php';

    /// Handle the request
        $request = new \App\handlers\Request($_SERVER['REQUEST_URI']);

    /// Handle the request to load the views
        $request->handle();