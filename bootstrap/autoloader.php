<?php

    /// Autoload classes
        function my_autoloader($class_name) {

            $directories = [
                'app/handlers/',
                'app/controllers/',
                'app/models/',
                'vendor/psr/log/Psr/Log/',
                'vendor/psr/log/Psr/Log/Test/',
                'vendor/pusher/pusher-php-server/src/',
                'vendor/pusher/pusher-php-server/tests/acceptance',
                'vendor/phpmailer/phpmailer/src/',
                'vender/phpmailer/phpmailer/language/'
            ];

            $class_name = explode('\\', $class_name);
            $class_name = end($class_name);

            foreach ($directories as $directory) {

                if (file_exists($directory . $class_name . '.php')) {
                    require_once $directory . $class_name . '.php';
                }

            }

        }

        spl_autoload_register('my_autoloader');