<?php

    function form_posted() {
        if (isset($_POST) && !empty($_POST)) {
            return true;
        }

        return false;
    }

    function post_field($field_name) {
        return isset($_POST[$field_name]) ? $_POST[$field_name] : '';
    }