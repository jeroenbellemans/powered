<?php

    function config_get_root() {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/';
    }

    function config_get_front_title() {
        return 'Power ED';
    }

    function config_get_title() {
        return 'Power ED';
    }

    function debug_print($data) {
        echo '<code><pre>';
        (is_array($data)) ? print_r($data) : var_dump($data);
        echo '</pre></code>';
    }